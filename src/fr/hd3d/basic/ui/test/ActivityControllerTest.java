package fr.hd3d.basic.ui.test;

import java.util.Date;

import org.junit.Test;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.basic.ui.client.portlet.activity.ActivityController;
import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.basic.ui.client.portlet.activity.model.ActivityModel;
import fr.hd3d.basic.ui.client.portlet.activity.model.reader.MonthTotalReaderSingleton;
import fr.hd3d.basic.ui.client.portlet.activity.model.reader.WeekTotalReaderSingleton;
import fr.hd3d.basic.ui.test.mock.ActivityMock;
import fr.hd3d.basic.ui.test.mock.MonthTotalReaderMock;
import fr.hd3d.basic.ui.test.mock.WeekTotalReaderMock;
import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUtils;


public class ActivityControllerTest extends UITestCase
{
    ActivityModel model;
    ActivityMock view;
    ActivityController controller;

    ProjectModelData project;
    PersonDayModelData currentDay;

    @Override
    public void setUp()
    {
        super.setUp();

        MainModel.currentUser = TestUtils.getDefaultPerson();
        project = TestUtils.getDefaultProject();
        currentDay = TestUtils.getDefaultDay();

        model = new ActivityModel();
        view = new ActivityMock();
        controller = new ActivityController(view, model);

        WeekTotalReaderSingleton.setInstance(new WeekTotalReaderMock());
        MonthTotalReaderSingleton.setInstance(new MonthTotalReaderMock());

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    @Test
    public void testOnInit()
    {
        EventDispatcher.forwardEvent(ActivityEvents.INIT);

        assertEquals(true, this.view.getInitilizedWidgets());
    }

    // @Test
    // public void testPreviousClicked()
    // {
    // BasicConfig.availableDays = 1;
    // this.model.setCurrentDay(currentDay);
    // EventDispatcher.forwardEvent(ActivityEvents.PREVIOUS_CLICKED);
    //
    // this.view.enablePreviousButton();
    // this.view.setEditable(true);
    // assertEquals(new DateWrapper(this.currentDay.getDate()).addDays(-1).asDate().getTime(), this.model
    // .getCurrentDay().getDate().getTime());
    // assertTrue(this.view.isEnabledNextButton());
    //
    // EventDispatcher.forwardEvent(ActivityEvents.PREVIOUS_CLICKED);
    // assertTrue(this.view.isEditable());
    // assertTrue(this.view.isEnabledPreviousButton());
    //
    // EventDispatcher.forwardEvent(ActivityEvents.PREVIOUS_CLICKED);
    // EventDispatcher.forwardEvent(ActivityEvents.PREVIOUS_CLICKED);
    // assertTrue(this.view.isEditable());
    // assertTrue(this.view.isEnabledPreviousButton());
    // }
    //
    // @Test
    // public void testNextClicked()
    // {
    // this.model.setCurrentDay(currentDay);
    // EventDispatcher.forwardEvent(ActivityEvents.PREVIOUS_CLICKED);
    // assertEquals(new DateWrapper(this.currentDay.getDate()).addDays(-1).asDate().getTime(), this.model
    // .getCurrentDay().getDate().getTime());
    //
    // EventDispatcher.forwardEvent(ActivityEvents.NEXT_CLICKED);
    // assertEquals(new DateWrapper(this.currentDay.getDate()).asDate().getTime(), this.model.getCurrentDay()
    // .getDate().getTime());
    // assertEquals(false, this.view.isEnabledNextButton());
    // }

    // @Test
    // public void testRefreshActivities()
    // {
    // ConstituentModelData constituent = TestUtils.getDefaultConstituent();
    // TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
    // TaskModelData task = TestUtils.getTask(MainModel.currentUser, taskType, constituent, ETaskStatus.STAND_BY);
    // SimpleActivityModelData simpleActivity = TestUtils.getSimpleActivity(currentDay, project,
    // ESimpleActivityType.MEETING);
    // TaskActivityModelData taskActivity = TestUtils.getTaskActivity(task, currentDay);
    //
    // this.model.initStoreParameters();
    //
    // EventDispatcher.forwardEvent(ActivityEvents.REFRESH_ACTIVITIES);
    // assertNotNull(currentDay);
    // assertNotNull(currentDay.getId());
    // assertNotNull(this.model.getDayConstraintValue());
    // assertEquals(currentDay.getId().longValue(), this.model.getDayConstraintValue().longValue());
    // Date startDate = new DateWrapper(currentDay.getDate()).addDays(1).asDate();
    // assertEquals(2, this.model.getTaskActivityStore().getCount());
    // assertEquals(2, this.model.getSimpleActivityStore().getCount()); // 2 Because of the default one + the new
    // // activity
    // assertEquals(startDate.getTime(), this.model.getTaskStartDateConstraintValue().getTime());
    // assertEquals(currentDay.getDate().getTime(), this.model.getTaskEndDateConstraintValue().getTime());
    // assertEquals(this.model.getTaskActivityStore().getAt(0).getDuration()
    // + this.model.getTaskActivityStore().getAt(1).getDuration() + simpleActivity.getDuration(), this.model
    // .getWeekTotal().longValue());
    // assertEquals(taskActivity.getDuration() + simpleActivity.getDuration(), this.model.getMonthTotal().longValue());
    //
    // assertTrue(this.view.isSelectionReseted());
    // }

    @Test
    public void testDayLoaded()
    {
        PersonDayModelData currentDay = TestUtils.getDefaultDay();
        this.model.setCurrentDay(currentDay);
        this.model.initStoreParameters();

        ConstituentModelData constituent = TestUtils.getDefaultConstituent();
        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
        TaskModelData task = TestUtils.getTask(MainModel.currentUser, taskType, constituent, ETaskStatus.STAND_BY);
        SimpleActivityModelData simpleActivity = TestUtils.getSimpleActivity(currentDay, project,
                ESimpleActivityType.MEETING);
        TaskActivityModelData taskActivity = TestUtils.getTaskActivity(task, currentDay);

        EventDispatcher.forwardEvent(ActivityEvents.DAY_LOADED);

        assertEquals(currentDay.getId().longValue(), this.model.getDayConstraintValue().longValue());
        Date startDate = new DateWrapper(currentDay.getDate()).addDays(1).asDate();
        assertEquals(2, this.model.getTaskActivityStore().getCount());
        assertEquals(2, this.model.getSimpleActivityStore().getCount()); // 2 Because of the default one + the new
                                                                         // activity
        assertEquals(startDate.getTime(), this.model.getTaskStartDateConstraintValue().getTime());
        assertEquals(currentDay.getDate().getTime(), this.model.getTaskEndDateConstraintValue().getTime());
        assertEquals(taskActivity.getDuration() + simpleActivity.getDuration(), this.model.getWeekTotal().longValue());
        assertEquals(taskActivity.getDuration() + simpleActivity.getDuration(), this.model.getMonthTotal().longValue());
    }

    //
    // @Test
    // public void testActivityWeekTotalRetrieved()
    // {
    // Long duration = 3600L;
    // this.view.setTitleUpdated(false);
    // EventDispatcher.forwardEvent(ActivityEvents.ACTIVITY_WEEK_TOTAL_RETRIEVED, duration);
    // assertEquals(duration.longValue(), this.model.getWeekTotal().longValue());
    // assertEquals(0, this.model.getMonthTotal().longValue());
    // assertTrue(this.view.isTitleUpdated());
    // }
    //
    // @Test
    // public void testActivityMonthTotalRetrieved()
    // {
    // Long duration = 3600L;
    // this.view.setTitleUpdated(false);
    // EventDispatcher.forwardEvent(ActivityEvents.ACTIVITY_MONTH_TOTAL_RETRIEVED, duration);
    // assertEquals(0, this.model.getWeekTotal().longValue());
    // assertEquals(duration.longValue(), this.model.getMonthTotal().longValue());
    // assertTrue(this.view.isTitleUpdated());
    // }
    //
    // @Test
    // public void testTasksLoaded()
    // {
    // PersonDayModelData currentDay = TestUtils.getDefaultDay();
    // this.model.setCurrentDay(currentDay);
    // this.model.initStoreParameters();
    //
    // ConstituentModelData constituent = TestUtils.getDefaultConstituent();
    // TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
    //
    // TestUtils.deleteTasksForTaskTypes(Arrays.asList(taskType));
    //
    // TaskModelData task = TestUtils.getTask(MainModel.getCurrentUser(), taskType, constituent, ETaskStatus.STAND_BY);
    // TaskActivityModelData taskActivity = TestUtils.getTaskActivity(task, currentDay);
    // taskActivity = TestUtils.getTaskActivity(task, currentDay);
    //
    // this.model.getTaskStore().add(task);
    // this.model.updateDayConstraints();
    // EventDispatcher.forwardEvent(ActivityEvents.TASKS_LOADED);
    // assertEquals(1, this.model.getTaskMapById().values().size());
    // assertNotNull(this.model.getTaskMapById().get(task.getId().toString()));
    //
    // assertEquals(1, this.model.getTaskActivityStore().getCount());
    // assertEquals(taskActivity.getTaskId().longValue(), this.model.getTaskActivityStore().getAt(0).getTaskId()
    // .longValue());
    // assertEquals(taskActivity.getDayId().longValue(), this.model.getTaskActivityStore().getAt(0).getDayId()
    // .longValue());
    // }
    //
    // @Test
    // public void testTaskActivitiesLoaded()
    // {
    // EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITIES_LOADED);
    // // TODO
    // }
    //
    // @Test
    // public void testSimpleActivitiesLoaded()
    // {
    // PersonDayModelData currentDay = TestUtils.getDefaultDay();
    // this.model.setCurrentDay(currentDay);
    // this.view.setTitleUpdated(false);
    // EventDispatcher.forwardEvent(ActivityEvents.SIMPLE_ACTIVITIES_LOADED);
    // assertTrue(this.view.isTitleUpdated());
    //
    // assertEquals(1, this.model.getSimpleActivityStore().getCount());
    // SimpleActivityModelData activity = this.model.getSimpleActivityStore().getAt(0);
    //
    // assertEquals(this.model.getCurrentDay().getId(), activity.getDayId());
    // assertEquals(DatetimeUtil.today().getTime(), activity.getFilledDate().getTime());
    // assertEquals(MainModel.currentUser.getId().longValue(), activity.getFilledById().longValue());
    // assertEquals(MainModel.currentUser.getId().longValue(), activity.getWorkerId().longValue());
    // }
    //
    // @Test
    // public void testTaskActivityEdited()
    // {
    // EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITY_EDITED);
    // // TODO
    // }
    //
    // @Test
    // public void testSimpleActivityEdited()
    // {
    // ProjectModelData myProject = new ProjectModelData("ProjectName", "open", "ProjectHook");
    // SimpleActivityModelData eventDataActivity = new SimpleActivityModelData();
    // eventDataActivity.setDuration((long) 340);
    // eventDataActivity.setProjectId(myProject.getId());
    //
    // AppEvent myEvent = new AppEvent(ActivityEvents.SIMPLE_ACTIVITY_EDITED, eventDataActivity);
    //
    // EventDispatcher.forwardEvent(myEvent);
    //
    // // assertEquals(eventDataActivity, this.model.getSimpleActivityStore().getModels());
    // }
    //
    // @Test
    // public void testSimpleProjectChanged()
    // {
    // EventDispatcher.forwardEvent(ActivityEvents.SIMPLE_PROJECT_CHANGED);
    // // TODO
    // }
    //
    // @Test
    // public void testSimpleActivitySaved()
    // {
    // this.model.setCurrentDay(currentDay);
    // EventDispatcher.forwardEvent(ActivityEvents.SIMPLE_ACTIVITY_SAVED);
    // // TODO
    // }
    //
    // @Test
    // public void testTaskActivitySaved()
    // {
    // this.model.setCurrentDay(currentDay);
    // EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITY_SAVED);
    // // TODO
    // }
    //
    // @Test
    // public void testTaskAddClicked()
    // {
    // EventDispatcher.forwardEvent(ActivityEvents.ADD_TASK_CLICKED);
    // // TODO
    // }
    //
    // @Test
    // public void testSelectorTaskDoubleClicked()
    // {
    // this.model.setCurrentDay(currentDay);
    // // TaskModelData task = TestUtils.getDefaultTask();
    // // EventDispatcher.forwardEvent(ActivityEvents.SELECTOR_TASK_DOUBLE_CLICKED, task);
    // // TODO
    // }
    //
    // @Test
    // public void testTaskAllOrPlannedClicked()
    // {
    // this.model.setCurrentDay(currentDay);
    // EventDispatcher.forwardEvent(ActivityEvents.ALL_OR_PLANNED_CLICKED, Boolean.TRUE);
    // // TODO
    // }
    //
    // @Test
    // public void testRefreshTimeReached()
    // {
    // this.model.setCurrentDay(currentDay);
    // EventDispatcher.forwardEvent(BasicEvents.REFRESH_TIME_REACHED);
    // // TODO
    // }
    //
    // @Test
    // public void testHeadersToggleClicked()
    // {
    // EventDispatcher.forwardEvent(BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED, Boolean.TRUE);
    // // TODO
    // }
    //
    // @Test
    // public void testRefreshButtonClicked()
    // {
    // this.model.setCurrentDay(currentDay);
    // EventDispatcher.forwardEvent(BasicEvents.REFRESH_BUTTON_CLICKED);
    // // TODO
    // }
    //
    // @Test
    // public void testTaskActivitySelected()
    // {
    // EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITY_SELECTED);
    // // TODO
    // }
    //
    // @Test
    // public void testStatusChangedWaitApp()
    // {
    // EventDispatcher.forwardEvent(ActivityEvents.STATUS_CHANGED);
    // // TODO
    // }
    //
    // public void testAddActivityToTaskGrid()
    // {
    // // TODO
    // }
    //
    // public void testAddTaskActivityForTask()
    // {
    // // TODO
    // }
    //
    // public void testUpdateStatusTaskForActivity()
    // {
    // // TODO
    // }
}
