package fr.hd3d.basic.ui.test;

import java.util.Arrays;

import org.junit.Test;

import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.basic.ui.client.portlet.activity.widget.TaskSimpleExplorerController;
import fr.hd3d.basic.ui.client.portlet.activity.widget.TaskSimpleExplorerModel;
import fr.hd3d.basic.ui.test.mock.TaskSimpleExplorerMock;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUtils;


/**
 * Test suite that tests simple explorer displayed in task selector dialog.
 * 
 * @author HD3D
 */
public class TaskSimpleExplorerControllerTest extends UITestCase
{
    protected TaskSimpleExplorerMock view;
    protected TaskSimpleExplorerModel model;
    protected TaskSimpleExplorerController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        MainModel.currentUser = TestUtils.getDefaultPerson();

        model = new TaskSimpleExplorerModel();
        view = new TaskSimpleExplorerMock(model);
        controller = new TaskSimpleExplorerController(model, view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    @Test
    public void testProjectChanged()
    {
        ProjectModelData project = TestUtils.getDefaultProject();
        this.model.setTaskTypeStore(new ServiceStore<TaskTypeModelData>(ReaderFactory.getTaskTypeReader()));

        EventDispatcher.forwardEvent(ActivityEvents.SELECTOR_PROJECT_CHANGED);
        assertEquals(0, this.model.getModelStore().getCount());
        assertTrue(this.view.getIsNonProjectFieldsCleared());
        assertFalse(this.view.getIsProjectFieldsEnabled());

        EventDispatcher.forwardEvent(ActivityEvents.SELECTOR_PROJECT_CHANGED, project);
        assertTrue(this.view.getIsNonProjectFieldsCleared());
        assertTrue(this.view.getIsProjectFieldsEnabled());

        assertEquals(0, this.model.getModelStore().getCount());

        assertEquals(ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.TASKTYPES, this.model
                .getTaskTypeStore().getPath());
        assertEquals(ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.TASKS, this.model.getModelStore()
                .getPath());
    }

    @Test
    public void testTaskTypeChanged()
    {
        ProjectModelData project = TestUtils.getDefaultProject();
        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
        TaskTypeModelData taskType2 = TestUtils.getTaskType("Test-Texturing");

        TestUtils.deleteTasksForTaskTypes(Arrays.asList(taskType, taskType2));

        ConstituentModelData constituent = TestUtils.getDefaultConstituent();
        ConstituentModelData constituent2 = TestUtils.getDefaultConstituent2();
        TaskModelData task1 = TestUtils.getTask(null, taskType, constituent, ETaskStatus.STAND_BY);
        TaskModelData task2 = TestUtils.getTask(null, taskType, constituent2, ETaskStatus.STAND_BY);
        TaskModelData task3 = TestUtils.getTask(null, taskType2, constituent, ETaskStatus.STAND_BY);
        TaskModelData task4 = TestUtils.getTask(MainModel.currentUser, taskType, constituent, ETaskStatus.STAND_BY);
        TaskModelData task5 = TestUtils.getTask(null, taskType, constituent, ETaskStatus.OK);

        EventDispatcher.forwardEvent(ActivityEvents.SELECTOR_TASK_TYPE_CHANGED);
        assertEquals(0, this.model.getModelStore().getCount());

        EventDispatcher.forwardEvent(ActivityEvents.SELECTOR_TASK_TYPE_CHANGED, taskType);
        assertEquals(0, this.model.getModelStore().getCount());

        this.model.setProjectConstraintValue(project.getId());
        EventDispatcher.forwardEvent(ActivityEvents.SELECTOR_TASK_TYPE_CHANGED, taskType2);
        assertEquals(1, this.model.getModelStore().getCount());
        assertNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task1.getId()));
        assertNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task2.getId()));
        assertNotNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task3.getId()));
        assertNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task4.getId()));
        assertNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task5.getId()));
    }

    @Test
    public void testWorkObjectKeyUp()
    {
        ProjectModelData project = TestUtils.getDefaultProject();
        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
        TaskTypeModelData taskType2 = TestUtils.getTaskType("Test-Texturing");

        TestUtils.deleteTasksForTaskTypes(Arrays.asList(taskType, taskType2));

        ConstituentModelData constituent = TestUtils.getDefaultConstituent();
        ConstituentModelData constituent2 = TestUtils.getDefaultConstituent2();
        TaskModelData task1 = TestUtils.getTask(null, taskType, constituent, ETaskStatus.STAND_BY);
        TaskModelData task2 = TestUtils.getTask(null, taskType, constituent2, ETaskStatus.STAND_BY);
        TaskModelData task3 = TestUtils.getTask(null, taskType2, constituent, ETaskStatus.STAND_BY);
        TaskModelData task4 = TestUtils.getTask(MainModel.currentUser, taskType, constituent, ETaskStatus.STAND_BY);
        TaskModelData task5 = TestUtils.getTask(null, taskType, constituent, ETaskStatus.OK);

        EventDispatcher.forwardEvent(ActivityEvents.WORKOBJECT_FILTER_KEY_UP);
        assertEquals(0, this.model.getModelStore().getCount());

        this.model.setProjectConstraintValue(project.getId());
        EventDispatcher.forwardEvent(ActivityEvents.WORKOBJECT_FILTER_KEY_UP);
        assertEquals(0, this.model.getModelStore().getCount());

        this.model.setProjectConstraintValue(null);
        this.model.setTaskTypeConstraintValue(taskType.getId());
        EventDispatcher.forwardEvent(ActivityEvents.WORKOBJECT_FILTER_KEY_UP);
        assertEquals(0, this.model.getModelStore().getCount());

        this.model.setProjectConstraintValue(project.getId());
        EventDispatcher.forwardEvent(ActivityEvents.WORKOBJECT_FILTER_KEY_UP, "Sa");
        assertEquals(1, this.model.getModelStore().getCount());
        assertNotNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task1.getId()));
        assertNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task2.getId()));
        assertNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task3.getId()));
        assertNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task4.getId()));
        assertNull(this.model.getModelStore().findModel(TaskModelData.ID_FIELD, task5.getId()));
    }
}
