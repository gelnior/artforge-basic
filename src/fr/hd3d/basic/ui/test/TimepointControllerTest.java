package fr.hd3d.basic.ui.test;

import java.util.Date;

import org.junit.Test;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.form.Time;

import fr.hd3d.basic.ui.client.error.BasicErrors;
import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.basic.ui.client.portlet.timepoint.TimepointController;
import fr.hd3d.basic.ui.client.portlet.timepoint.TimepointEvents;
import fr.hd3d.basic.ui.client.portlet.timepoint.TimepointModel;
import fr.hd3d.basic.ui.test.mock.TimepointMock;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUtils;


/**
 * Tests for time point portlet : test behavior of controller for each event then test utility methods like checkTime
 * method.
 * 
 * @author HD3D
 */
public class TimepointControllerTest extends UITestCase
{
    private TimepointModel model;
    private TimepointMock view;
    private TimepointController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        model = new TimepointModel();
        view = new TimepointMock(model);
        controller = new TimepointController(view, model);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
        dispatcher.addController(errorController);
    }

    @Test
    public void testInit()
    {
        EventDispatcher.forwardEvent(TimepointEvents.INIT);
        assertTrue(this.view.isWidgetInitialized());
    }

    @Test
    public void testDayLoaded()
    {
        PersonDayModelData personDay = new PersonDayModelData();
        AppEvent event = new AppEvent(ActivityEvents.DAY_LOADED);

        personDay.setPersonId(123L);
        personDay.setDate(new Date());
        Date startDate1 = new Date();
        Date startDate2 = new Date();
        Date endDate1 = new Date();
        Date endDate2 = new Date();
        personDay.setStartDate1(startDate1);
        personDay.setStartDate2(startDate2);
        personDay.setEndDate1(endDate1);
        personDay.setEndDate2(endDate2);
        event.setData(CommonConfig.MODEL_EVENT_VAR_NAME, personDay);

        EventDispatcher.forwardEvent(event);
        assertEquals(personDay, this.model.getCurrentDay());
        assertEquals(startDate1, view.getArrivalTime());
        assertEquals(startDate2, view.getBeginningBreak());
        assertEquals(endDate1, view.getEndBreak());
        assertEquals(endDate2, view.getDepartureTime());
        assertEquals(null, view.getFieldTotal());

        event.setData(CommonConfig.MODEL_EVENT_VAR_NAME, TestUtils.getDefaultDay());
        EventDispatcher.forwardEvent(event);
        assertEquals("08h", view.getFieldTotal());
    }

    @Test
    public void testTaskActivitiesLoaded()
    {
        this.model.setCurrentDay(TestUtils.getDefaultDay());
        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 8);
        EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITIES_LOADED);
        assertNotSame(BasicErrors.ACTIVITIES_BIGGER_PRESENCE, errorController.getLastError());
        assertEquals(CSSUtils.COLOR_GREEN, this.view.getTotalcolor());

        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 13);
        EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITIES_LOADED);
        assertEquals(BasicErrors.ACTIVITIES_BIGGER_PRESENCE, errorController.getLastError());
        assertEquals(CSSUtils.COLOR_RED, this.view.getTotalcolor());

        this.errorController.reset();
        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 3);
        EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITIES_LOADED);
        assertNotSame(BasicErrors.ACTIVITIES_BIGGER_PRESENCE, errorController.getLastError());
        assertEquals(CSSUtils.COLOR_GREEN, this.view.getTotalcolor());
    }

    @Test
    public void testTimePointSaved()
    {
        this.model.setCurrentDay(TestUtils.getDefaultDay());
        EventDispatcher.forwardEvent(TimepointEvents.TIME_POINT_SAVED);
        assertEquals("08h", this.view.getFieldTotal());
    }

    @Test
    public void testArrivalChanged()
    {
        this.timeChanged(PersonDayModelData.PERSON_WORKING_TIME_START1_FIELD, TimepointEvents.ARRIVAL_CHANGED, 8, 30);
    }

    @Test
    public void testLeavingChanged()
    {
        this.timeChanged(PersonDayModelData.PERSON_WORKING_TIME_END2_FIELD, TimepointEvents.LEAVING_CHANGED, 20, 40);
    }

    @Test
    public void testBeginningBreakChanged()
    {
        this.timeChanged(PersonDayModelData.PERSON_WORKING_TIME_END1_FIELD, TimepointEvents.BEGINNING_BREAK_CHANGED,
                11, 50);
    }

    @Test
    public void testEndBreakChanged()
    {
        this.timeChanged(PersonDayModelData.PERSON_WORKING_TIME_START2_FIELD, TimepointEvents.END_BREAK_CHANGED, 15, 05);
    }

    public void timeChanged(String field, EventType eventType, int hours, int minutes)
    {
        PersonDayModelData day = TestUtils.getDefaultDay();
        this.model.setCurrentDay(day);

        PersonModelData personTest = TestUtils.getDefaultPerson();
        assertNotNull(personTest.getId());
        day.setPersonId(personTest.getId());
        day.refreshOrCreate(null);

        Time time = new Time();
        time.setHour(hours);
        time.setMinutes(minutes);
        EventDispatcher.forwardEvent(eventType, time);

        PersonDayModelData dayFromServer = TestUtils.getDefaultDay();
        dayFromServer.setPersonId(personTest.getId());
        dayFromServer.setDate(day.getDate());
        dayFromServer.refreshOrCreate(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);

        assertEquals(hours, new DateWrapper(((Date) day.get(field))).getHours());
        assertEquals(hours, new DateWrapper(((Date) dayFromServer.get(field))).getHours());
        assertEquals(minutes, new DateWrapper(((Date) dayFromServer.get(field))).getMinutes());
    }

    @Test
    public void testCheckTime()
    {
        PersonDayModelData day = TestUtils.getDefaultDay();
        this.model.setCurrentDay(day);

        assertTrue(this.controller.checkTime(day.getStartDate1(), day.getEndDate1(), day.getStartDate2(),
                day.getEndDate2()));
        assertEquals(-1, errorController.getLastError());
        assertFalse(this.controller.checkTime(day.getEndDate1(), day.getStartDate2(), day.getStartDate1(),
                day.getEndDate2()));
        assertEquals(BasicErrors.TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING, errorController.getLastError());
        assertFalse(this.controller.checkTime(day.getStartDate2(), day.getEndDate1(), day.getEndDate2(),
                day.getStartDate1()));
        assertEquals(BasicErrors.TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING, errorController.getLastError());
        assertFalse(this.controller.checkTime(day.getEndDate2(), day.getEndDate1(), day.getStartDate1(),
                day.getStartDate2()));
        assertEquals(BasicErrors.TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING, errorController.getLastError());
        assertFalse(this.controller.checkTime(day.getEndDate2(), null, null, day.getStartDate2()));
        assertEquals(BasicErrors.TIME_DEPARTURE_BEFORE_ARRIVAL, errorController.getLastError());
    }

    @Test
    public void testActivitiesSaved()
    {
        this.model.setCurrentDay(TestUtils.getDefaultDay());

        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 8);
        EventDispatcher.forwardEvent(ActivityEvents.ACTIVITIES_SAVED);
        assertNotSame(BasicErrors.ACTIVITIES_BIGGER_PRESENCE, errorController.getLastError());
        assertEquals(CSSUtils.COLOR_GREEN, this.view.getTotalcolor());

        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 13);
        EventDispatcher.forwardEvent(ActivityEvents.ACTIVITIES_SAVED);
        assertEquals(BasicErrors.ACTIVITIES_BIGGER_PRESENCE, errorController.getLastError());
        assertEquals(CSSUtils.COLOR_RED, this.view.getTotalcolor());

        errorController.reset();
        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 3);
        EventDispatcher.forwardEvent(ActivityEvents.ACTIVITIES_SAVED);
        assertNotSame(BasicErrors.ACTIVITIES_BIGGER_PRESENCE, errorController.getLastError());
        assertEquals(CSSUtils.COLOR_GREEN, this.view.getTotalcolor());
    }

    @Test
    public void testOnError()
    {
        this.model.setCurrentDay(TestUtils.getDefaultDay());
        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 8);
        ErrorDispatcher.sendError(CommonErrors.NO_PERMISSION);
        assertEquals(CSSUtils.COLOR_GREEN, this.view.getTotalcolor());

        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 13);
        ErrorDispatcher.sendError(CommonErrors.NO_PERMISSION);
        assertEquals(CSSUtils.COLOR_RED, this.view.getTotalcolor());

        this.view.setActivitiesTotal(DatetimeUtil.HOUR_SECONDS * 3);
        ErrorDispatcher.sendError(CommonErrors.NO_PERMISSION);
        assertEquals(CSSUtils.COLOR_GREEN, this.view.getTotalcolor());
    }
}
