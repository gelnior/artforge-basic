package fr.hd3d.basic.ui.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.hd3d.basic.ui.client.BasicModel;
import fr.hd3d.basic.ui.client.config.BasicConfig;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.test.UITestCase;


/**
 * Test the BasicModel
 */
public class BasicModelTest extends UITestCase
{
    BasicModel model;

    @Override
    public void setUp()
    {
        super.setUp();

        model = new BasicModel();
    }

    @Test
    public void testSavePortlet()
    {
        List<List<String>> myListList = new ArrayList<List<String>>();
        List<String> Col0 = new ArrayList<String>();
        List<String> Col1 = new ArrayList<String>();

        Col0.add("Col0_Line0");
        Col0.add("Col0_Line1");
        Col0.add("Col0_Line2");
        Col1.add("Col1_Line0");
        Col1.add("Col1_Line1");
        Col1.add("Col1_Line2");

        myListList.add(Col0);
        myListList.add(Col1);

        String strTest = Col0.get(0) + "-" + Col0.get(1) + "-" + Col0.get(2) + ";" + Col1.get(0) + "-" + Col1.get(1)
                + "-" + Col1.get(2);

        this.model.setNbColumns(2);
        this.model.setPortletMatrix(myListList);

        this.model.savePortletMatrix();

        assertEquals(strTest, UserSettings.getSetting(BasicConfig.POSITION_PORTLET_SETTING + this.model.getNbColumns()));
    }
}
