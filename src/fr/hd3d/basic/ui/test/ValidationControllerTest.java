package fr.hd3d.basic.ui.test;

import org.junit.Test;

import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.basic.ui.client.StoreFactory;
import fr.hd3d.basic.ui.client.event.BasicEvents;
import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.basic.ui.client.portlet.validation.ValidationController;
import fr.hd3d.basic.ui.client.portlet.validation.ValidationEvents;
import fr.hd3d.basic.ui.client.portlet.validation.model.ValidationModel;
import fr.hd3d.basic.ui.test.mock.ValidationMock;
import fr.hd3d.basic.ui.test.utils.BasicTestUtils;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.BasePair;
import fr.hd3d.common.ui.client.util.HtmlUtils;
import fr.hd3d.common.ui.test.UITestCase;


/**
 * Tests for validation portlet : test behavior of validation portlet controller for each event then test utility
 * methods.
 * 
 * @author HD3D
 */
public class ValidationControllerTest extends UITestCase
{
    private ValidationModel model;
    private ValidationMock view;
    private ValidationController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        model = new ValidationModel();
        view = new ValidationMock(model);
        controller = new ValidationController(view, model);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
        dispatcher.addController(errorController);
    }

    @Test
    public void testInit()
    {
        EventDispatcher.forwardEvent(ValidationEvents.INIT);
        assertTrue(this.view.isWidgetInitialized());
    }

    @Test
    public void testHeadersToggleButtonClicked()
    {
        assertFalse(this.view.isGridHeadersDisplayed());
        EventDispatcher.forwardEvent(BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED, true);
        assertTrue(this.view.isGridHeadersDisplayed());
        EventDispatcher.forwardEvent(BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED, false);
        assertFalse(this.view.isGridHeadersDisplayed());
    }

    @Test
    public void testTaskSelected()
    {
        BasePair<TaskModelData, ApprovalNoteModelData> taskApprovalPair = BasicTestUtils.getDefaultTaskAndApproval();
        TaskModelData task = taskApprovalPair.getA();
        ApprovalNoteModelData approval = taskApprovalPair.getB();

        EventDispatcher.forwardEvent(BasicEvents.TASK_SELECTED, task);
        assertTrue(controller.isExpandTreeGridAfterLoading());
        assertEquals(task, this.model.getCurrentTask());
        assertTrue(0 < this.model.getApprovalStore().getCount());
        assertNotNull(this.model.getApprovalStore().findModel(Hd3dModelData.ID_FIELD, approval.getId()));
    }

    @Test
    public void testOnTaskLoaded()
    {
        BasePair<TaskModelData, ApprovalNoteModelData> taskApprovalPair = BasicTestUtils.getDefaultTaskAndApproval();
        TaskModelData task = taskApprovalPair.getA();
        ApprovalNoteModelData approval = taskApprovalPair.getB();

        this.model.setTaskStore(StoreFactory.newTaskStore());
        this.model.getTaskStore().add(task);

        EventDispatcher.forwardEvent(ActivityEvents.TASKS_LOADED, this.model.getTaskStore());
        TreeStore<ApprovalNoteModelData> treeStore = this.model.getApprovalTreeStore();
        assertNotNull(treeStore.findModel(Hd3dModelData.ID_FIELD, approval.getId()));
        assertNotNull(treeStore.findModel(ApprovalNoteModelData.APPROVAL_TYPE_NAME, getExpectedDisplayName(task)));
        assertNotNull(treeStore.findModel(ApprovalNoteModelData.APPROVAL_TYPE_NAME,
                HtmlUtils.BLANK_CHAR + approval.getApprovalTypeName() + HtmlUtils.BLANK_CHAR));
    }

    // public void testAddTaskToTreeStore()
    // {
    // ApprovalNoteModelData approval1 = new ApprovalNoteModelData();
    // approval1.setStatus(EApprovalNoteStatus.OK.toString());
    // approval1.setDate(DatetimeUtil.today());
    // approval1.set(ApprovalNoteModelData.TASK_TYPE_ID, 1L);
    // approval1.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 20L);
    // ApprovalNoteModelData approval2 = new ApprovalNoteModelData();
    // approval2.setStatus(EApprovalNoteStatus.Done.toString());
    // approval2.setDate(DatetimeUtil.daysBeforeToday(1));
    // approval2.set(ApprovalNoteModelData.TASK_TYPE_ID, 2L);
    // approval2.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 20L);
    // ApprovalNoteModelData approval3 = new ApprovalNoteModelData();
    // approval3.setStatus(EApprovalNoteStatus.NOK.toString());
    // approval3.setDate(DatetimeUtil.daysBeforeToday(2));
    // approval3.set(ApprovalNoteModelData.TASK_TYPE_ID, 2L);
    // approval3.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 20L);
    //
    // List<ApprovalNoteModelData> approvals = new ArrayList<ApprovalNoteModelData>();
    // approvals.add(approval1);
    // approvals.add(approval2);
    // approvals.add(approval3);
    //
    // TaskModelData task = new TaskModelData();
    // task.setProjectName("UI Test Project");
    // task.setWorkObjectParentsName("Persos");
    // task.setWorkObjectName("Sam");
    // task.setTaskTypeName("Modeling");
    //
    // this.contro
    // }

    @Test
    public void testOnApprovalLoaded()
    {
        BasePair<TaskModelData, ApprovalNoteModelData> taskApprovalPair = BasicTestUtils.getDefaultTaskAndApproval();
        TaskModelData task = taskApprovalPair.getA();
        ApprovalNoteModelData approval = taskApprovalPair.getB();

        this.model.setTaskStore(new ServiceStore<TaskModelData>(ReaderFactory.getTaskReader()));
        this.model.getTaskStore().add(task);
        this.model.getApprovalStore().add(approval);
        EventDispatcher.forwardEvent(ValidationEvents.APPROVAL_LOADED);
        TreeStore<ApprovalNoteModelData> treeStore = this.model.getApprovalTreeStore();
        assertNotNull(treeStore.findModel(Hd3dModelData.ID_FIELD, approval.getId()));
        assertNotNull(treeStore.findModel(ApprovalNoteModelData.APPROVAL_TYPE_NAME, getExpectedDisplayName(task)));
        assertNotNull(treeStore.findModel(ApprovalNoteModelData.APPROVAL_TYPE_NAME,
                HtmlUtils.BLANK_CHAR + approval.getApprovalTypeName() + HtmlUtils.BLANK_CHAR));
    }

    @Test
    public void testShowAllApprovalNotesClicked()
    {
        BasePair<TaskModelData, ApprovalNoteModelData> taskApprovalPair = BasicTestUtils.getDefaultTaskAndApproval();
        TaskModelData task = taskApprovalPair.getA();
        ApprovalNoteModelData approval = taskApprovalPair.getB();

        this.model.setTaskStore(new ServiceStore<TaskModelData>(ReaderFactory.getTaskReader()));
        this.model.getTaskStore().add(task);
        EventDispatcher.forwardEvent(ValidationEvents.SHOW_ALL_APPROVAL_NOTES_CLICKED);
        TreeStore<ApprovalNoteModelData> treeStore = this.model.getApprovalTreeStore();
        assertNotNull(treeStore.findModel(Hd3dModelData.ID_FIELD, approval.getId()));
        assertNotNull(treeStore.findModel(ApprovalNoteModelData.APPROVAL_TYPE_NAME, getExpectedDisplayName(task)));
        assertNotNull(treeStore.findModel(ApprovalNoteModelData.APPROVAL_TYPE_NAME,
                HtmlUtils.BLANK_CHAR + approval.getApprovalTypeName() + HtmlUtils.BLANK_CHAR));
    }

    private String getExpectedDisplayName(TaskModelData task)
    {
        return HtmlUtils.BLANK_CHAR + task.getWorkObjectName() + " " + HtmlUtils.BLANK_CHAR + HtmlUtils.BLANK_CHAR
                + " (" + task.getTaskTypeName() + ") " + HtmlUtils.BLANK_CHAR;
    }

}
