package fr.hd3d.basic.ui.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.basic.ui.client.BasicController;
import fr.hd3d.basic.ui.client.BasicModel;
import fr.hd3d.basic.ui.client.config.BasicConfig;
import fr.hd3d.basic.ui.client.event.BasicEvents;
import fr.hd3d.basic.ui.test.mock.BasicMock;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.test.UITestCase;


/**
 * Test application main controller.
 * 
 * @author HD3D
 */
public class BasicControllerTest extends UITestCase
{
    BasicModel model;
    BasicMock view;
    BasicController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        model = new BasicModel();
        view = new BasicMock();
        controller = new BasicController(model, view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    /**
     * Test that a message is displayed when an error occurs.
     */
    @Test
    public void testOnError()
    {
        int error = CommonErrors.NO_CONFIG_FILE;
        String msg = "StringMsgError";
        String stack = "StringStackError";

        ErrorDispatcher.sendError(error, msg, stack);

        assertEquals(error, this.view.getDisplayedError());
        assertEquals(msg, this.view.getDisplayedUserMsg());
        assertEquals(stack, this.view.getDisplayedStack());
        assertEquals(false, this.view.getDisplayedStartPanel());
    }

    @Test
    public void testSettingsInitialized()
    {
        Integer nbColumnsTest = UserSettings.getSettingAsInteger(BasicConfig.COLUMN_PORTAL_SETTING);
        if (nbColumnsTest == null)
        {
            nbColumnsTest = BasicModel.DEFAULT_COLUMN_NUMBER;
        }

        EventDispatcher.forwardEvent(CommonEvents.SETTINGS_INITIALIZED);

        assertEquals(true, this.view.getDisplayedWidget());
        assertEquals(false, this.view.getDisplayedStartPanel());
        assertEquals(String.valueOf(nbColumnsTest), String.valueOf(this.model.getNbColumns()));
    }

    @Test
    public void testPortletSettingsNull()
    {
        int i = 0;
        int nbrPortlets = 56;
        String strTest = "";
        UserSettings.setSetting(BasicConfig.COLUMN_PORTAL_SETTING, null);

        for (i = 0; i < nbrPortlets; i++)
        {
            this.view.addPortletTitles("portletTitle" + i);
        }

        i = 0;
        for (String portletTest : this.view.getPortletTitles())
        {
            strTest += portletTest.toLowerCase();
            if (i < nbrPortlets - 1)
            {
                strTest += "-";
            }
            else
            {
                strTest += ";";
            }
            i++;
        }

        UserSettings.setSetting(BasicConfig.POSITION_PORTLET_SETTING + model.getNbColumns(), strTest);

        EventDispatcher.forwardEvent(CommonEvents.SETTINGS_INITIALIZED);
        assertEquals(BasicModel.DEFAULT_COLUMN_NUMBER, (Integer) this.model.getNbColumns());
        assertEquals(strTest, UserSettings.getSetting(BasicConfig.POSITION_PORTLET_SETTING + model.getNbColumns()));
    }

    @Test
    public void testPortletChecked()
    {
        String eventData = "myPortlet";
        assertEquals(false, this.view.isPortletItemChecked(eventData));
        this.view.checkPortlet(eventData);
        assertEquals(true, this.view.isPortletItemChecked(eventData));
        EventDispatcher.forwardEvent(BasicEvents.PORTLET_CHECKED, eventData);

        assertEquals(true, this.view.isPortletItemChecked(eventData));
        assertEquals(this.view.getDisplayedPortlets(), this.view.getCheckedPortlets());
    }

    @Test
    public void testColumnsChanged()
    {
        int nbColumns = 1;
        EventDispatcher.forwardEvent(BasicEvents.COLUMNS_CHANGED, nbColumns);

        assertEquals(nbColumns, this.model.getNbColumns());
        assertEquals(nbColumns, this.view.getNbColumns());
        assertEquals(String.valueOf(nbColumns), UserSettings.getSetting(BasicConfig.COLUMN_PORTAL_SETTING));
    }

    /**
     * Test when nbColumns >> MAX_COLUMNS : In this case, nbColumns should be equals to MAX_COLUMNS
     */
    @Test
    public void testColumnsChangedMax()
    {
        int nbColumns = 480;
        this.model.setNbColumns(1);
        EventDispatcher.forwardEvent(BasicEvents.COLUMNS_CHANGED, nbColumns);

        assertEquals(BasicModel.MAX_COLUMNS, this.model.getNbColumns());
        assertEquals(BasicModel.MAX_COLUMNS, this.view.getNbColumns());
        assertEquals(String.valueOf(this.view.getNbColumns()),
                UserSettings.getSetting(BasicConfig.COLUMN_PORTAL_SETTING));
    }

    @Test
    public void testPortletPositionChanged()
    {
        List<List<String>> myListList = new ArrayList<List<String>>();
        List<String> Col0 = new ArrayList<String>();
        List<String> Col1 = new ArrayList<String>();

        Col0.add("Col0_Line0".toLowerCase());
        Col0.add("Col0_Line1".toLowerCase());
        Col0.add("Col0_Line2".toLowerCase());
        Col1.add("Col1_Line0".toLowerCase());
        Col1.add("Col1_Line1".toLowerCase());
        Col1.add("Col1_Line2".toLowerCase());

        myListList.add(Col0);
        myListList.add(Col1);

        this.model.setNbColumns(myListList.size());
        this.model.setPortletMatrix(myListList);

        AppEvent myEvent = new AppEvent(BasicEvents.PORTLET_POSITION_CHANGED);

        myEvent.setData(BasicConfig.ROW_EVENT_VAR_NAME, 2);
        myEvent.setData(BasicConfig.COLUMN_EVENT_VAR_NAME, 1);
        myEvent.setData(BasicConfig.START_ROW_EVENT_VAR_NAME, 0);
        myEvent.setData(BasicConfig.START_COLUMN_EVENT_VAR_NAME, 0);
        myEvent.setData(BasicConfig.TITLE_EVENT_VAR_NAME, "Col0_Line0");

        String strTest = Col0.get(1) + "-" + Col0.get(2) + ";" + Col1.get(0) + "-" + Col1.get(1) + "-" + Col0.get(0)
                + "-" + Col1.get(2);

        EventDispatcher.forwardEvent(myEvent);

        assertEquals(myListList.size(), this.model.getNbColumns());
        String settingString = UserSettings
                .getSetting(BasicConfig.POSITION_PORTLET_SETTING + this.model.getNbColumns());
        assertEquals(strTest, settingString);
        assertEquals(Col0.get(1), this.model.getPortletMatrix().get(0).get(1));
    }
}
