package fr.hd3d.basic.ui.test.utils;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.util.BasePair;
import fr.hd3d.common.ui.test.util.TestUtils;


public class BasicTestUtils
{

    /**
     * @return A pair constituted of a default task : Modeling of constituent Sam and an OK approval note about the
     *         modeling of constituent Sam.
     */
    public static BasePair<TaskModelData, ApprovalNoteModelData> getDefaultTaskAndApproval()
    {
        ProjectModelData project = TestUtils.getDefaultProject();
        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
        CategoryModelData category = TestUtils.getCategory("Persos", project.getId(), null);
        ConstituentModelData constituent = TestUtils.getConstituent("Sam", category);

        TaskModelData task = new TaskModelData();
        task.setWorkObjectId(constituent.getId());
        task.setProjectID(project.getId());
        task.setTaskTypeId(taskType.getId());
        task.setProjectName(project.getName());
        task.setWorkObjectName(constituent.getLabel());
        task.setStatus(ETaskStatus.TO_DO.toString());

        ApprovalNoteTypeModelData approvalType = TestUtils.getApprovalType("Test-Modeling", project.getId(),
                taskType.getId());
        ApprovalNoteModelData note = TestUtils.getApprovalNote(DatetimeUtil.today(), constituent, approvalType);
        note.setApprovalTypeName(approvalType.getName());
        note.setTaskTypeId(taskType.getId());

        BasePair<TaskModelData, ApprovalNoteModelData> taskApprovalPair = new BasePair<TaskModelData, ApprovalNoteModelData>(
                task, note);
        return taskApprovalPair;
    }

    public static void getApprovalList()
    {
        ApprovalNoteModelData approval1 = new ApprovalNoteModelData();
        approval1.setId(1L);
        approval1.set(ApprovalNoteModelData.TASK_TYPE_ID, 1L);
        approval1.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 10L);
        ApprovalNoteModelData approval2 = new ApprovalNoteModelData();
        approval2.setId(2L);
        approval2.set(ApprovalNoteModelData.TASK_TYPE_ID, 2L);
        approval2.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 20L);
        ApprovalNoteModelData approval3 = new ApprovalNoteModelData();
        approval3.setId(3L);
        approval3.set(ApprovalNoteModelData.TASK_TYPE_ID, 2L);
        approval3.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 20L);
    }

    public static ApprovalNoteModelData getDoneApproval(Long boundEntityId, Long approvalNoteTypeId)
    {
        ApprovalNoteModelData approval = new ApprovalNoteModelData(DatetimeUtil.today(), boundEntityId,
                approvalNoteTypeId);

        Constraints constraints = new Constraints();
        constraints.add(new EqConstraint(ApprovalNoteModelData.STATUS_FIELD, EApprovalNoteStatus.WAIT_APP.toString()));
        constraints.add(new EqConstraint(ApprovalNoteModelData.BOUND_ENTITY_ID, boundEntityId));
        constraints.add(new EqConstraint(ApprovalNoteModelData.APPROVAL_TYPE_ID, approvalNoteTypeId));
        approval.refreshWithParams(null, constraints);

        return approval;
    }

}
