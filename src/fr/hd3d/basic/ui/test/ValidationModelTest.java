package fr.hd3d.basic.ui.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.basic.ui.client.portlet.validation.model.ValidationModel;
import fr.hd3d.basic.ui.client.portlet.validation.model.ValidationSorter;
import fr.hd3d.basic.ui.test.utils.BasicTestUtils;
import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.BasePair;
import fr.hd3d.common.ui.client.util.HtmlUtils;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUtils;
import fr.hd3d.common.ui.test.util.modeldata.reader.BaseModelReaderMock;


/**
 * Test that data are rightly handled by approval portlet.
 * 
 * @author HD3D
 */
public class ValidationModelTest extends UITestCase
{
    protected ValidationModel model;

    @Override
    public void setUp()
    {
        super.setUp();
        Hd3dModelDataReaderSingleton.setInstanceAsMock(new BaseModelReaderMock());
        model = new ValidationModel();
    }

    @Test
    public void testValidationSorter()
    {
        ValidationSorter sorter = new ValidationSorter();

        ApprovalNoteModelData approval1 = new ApprovalNoteModelData();
        ApprovalNoteModelData approval2 = new ApprovalNoteModelData();

        assertTrue(0 == sorter.compare(null, approval1, approval2, null));

        approval1.setDate(DatetimeUtil.today());
        approval2.setDate(new DateWrapper(DatetimeUtil.today()).addDays(1).asDate());
        assertTrue(0 < sorter.compare(null, approval1, approval2, null));

        approval1.setStatus(EApprovalNoteStatus.RETAKE.toString());
        approval2.setStatus(EApprovalNoteStatus.OK.toString());
        approval1.set(ApprovalNoteModelData.WORK_OBJECT_PATH, "test");
        approval2.set(ApprovalNoteModelData.WORK_OBJECT_PATH, "test");

        approval1.setStatus(EApprovalNoteStatus.RETAKE.toString());
        approval2.setStatus(EApprovalNoteStatus.OK.toString());
        assertTrue(0 > sorter.compare(null, approval1, approval2, null));

        approval1.setStatus(EApprovalNoteStatus.WAIT_APP.toString());
        assertTrue(0 > sorter.compare(null, approval1, approval2, null));

        approval1.setStatus(EApprovalNoteStatus.OK.toString());
        assertTrue(0 < sorter.compare(null, approval1, approval2, null));
    }

    @Test
    public void testReloadValidations()
    {
        BasePair<TaskModelData, ApprovalNoteModelData> taskApprovalPair = BasicTestUtils.getDefaultTaskAndApproval();
        TaskModelData task = taskApprovalPair.getA();
        ApprovalNoteModelData note = taskApprovalPair.getB();
        task.setDefaultPath(TestUtils.getDefaultProject().getPath() + "/" + ServicesPath.TASKS);
        task.save();
        note.save();

        this.model.reloadValidations(task);
        assertEquals(task, this.model.getCurrentTask());
        assertTrue(0 < this.model.getApprovalStore().getCount());
        assertNotNull(this.model.getApprovalStore().findModel(Hd3dModelData.ID_FIELD, note.getId()));
        task.delete();
        note.delete();
    }

    @Test
    public void testLoadApprovalNoteType()
    {
        ProjectModelData project = TestUtils.getDefaultProject();
        TaskTypeModelData taskType = TestUtils.getTaskType("Modeling");

        ApprovalNoteTypeModelData type1 = TestUtils.getApprovalType("Modeling", project.getId(), taskType.getId());
        ApprovalNoteTypeModelData type2 = TestUtils.getApprovalType("Modeling all projects", null, taskType.getId());
        ApprovalNoteTypeModelData type3 = TestUtils.getApprovalType("Full approbation", project.getId(), null);

        TaskModelData task = new TaskModelData();
        task.setProjectID(project.getId());
        task.setTaskTypeId(taskType.getId());

        // this.model.loadApprovalNoteType(task);
        // assertTrue(0 < this.model.getApprovalTypeStore().getCount());
        // assertNotNull(this.model.getApprovalTypeStore().findModel(Hd3dModelData.ID_FIELD, type1.getId()));
        // assertNull(this.model.getApprovalTypeStore().findModel(Hd3dModelData.ID_FIELD, type2.getId()));
        // assertNull(this.model.getApprovalTypeStore().findModel(Hd3dModelData.ID_FIELD, type3.getId()));
    }

    @Test
    public void testReloadTreeStoreForAllTasks()
    {
        BasePair<TaskModelData, ApprovalNoteModelData> taskApprovalPair = BasicTestUtils.getDefaultTaskAndApproval();
        TaskModelData task = taskApprovalPair.getA();
        ApprovalNoteModelData approval = taskApprovalPair.getB();

        this.model.setTaskStore(new ServiceStore<TaskModelData>(ReaderFactory.getTaskReader()));
        this.model.getTaskStore().add(task);
        this.model.reloadTreeStore4AllTask();
        assertNull(this.model.getCurrentTask());
        assertTrue(0 < this.model.getApprovalStore().getCount());
        assertNotNull(this.model.getApprovalStore().findModel(Hd3dModelData.ID_FIELD, approval.getId()));
    }

    @Test
    public void testUpdateApprovalMapToFilter()
    {
        ApprovalNoteModelData approval1 = new ApprovalNoteModelData();
        approval1.setId(1L);
        approval1.set(ApprovalNoteModelData.TASK_TYPE_ID, 1L);
        approval1.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 10L);
        ApprovalNoteModelData approval2 = new ApprovalNoteModelData();
        approval2.setId(2L);
        approval2.set(ApprovalNoteModelData.TASK_TYPE_ID, 2L);
        approval2.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 20L);
        ApprovalNoteModelData approval3 = new ApprovalNoteModelData();
        approval3.setId(3L);
        approval3.set(ApprovalNoteModelData.TASK_TYPE_ID, 2L);
        approval3.set(ApprovalNoteModelData.BOUND_ENTITY_ID, 20L);

        this.model.getApprovalStore().add(approval1);
        this.model.getApprovalStore().add(approval2);
        this.model.getApprovalStore().add(approval3);

        this.model.updateApprovalMapToFilter();
        assertEquals(approval1.getId().longValue(), this.model.getApprovalListMapByWorkIdAndTaskTypeId().get("10 1")
                .get(0).getId().longValue());
        assertEquals(2, this.model.getApprovalListMapByWorkIdAndTaskTypeId().get("20 2").size());
        assertNull(this.model.getApprovalListMapByWorkIdAndTaskTypeId().get("20 1"));
        assertNull(this.model.getApprovalListMapByWorkIdAndTaskTypeId().get("10 2"));
    }

    public void testNewApprovalTypeNode()
    {
        ApprovalNoteModelData approval = new ApprovalNoteModelData();
        approval.setStatus(EApprovalNoteStatus.OK.toString());
        approval.setApprovalTypeName("Modeling");

        ApprovalNoteModelData newApproval = this.model.getNewApprovalTypeNode(approval);
        assertEquals(HtmlUtils.BLANK_CHAR + approval.getApprovalTypeName() + HtmlUtils.BLANK_CHAR,
                newApproval.getApprovalTypeName());
        assertEquals(approval.getStatus(), newApproval.get(ApprovalNoteModelData.DISPLAY_STATUS_FIELD));
    }

    public void testNewTaskTypeTypeNode()
    {
        ApprovalNoteModelData approval1 = new ApprovalNoteModelData();
        approval1.setStatus(EApprovalNoteStatus.OK.toString());
        approval1.setDate(DatetimeUtil.today());
        ApprovalNoteModelData approval2 = new ApprovalNoteModelData();
        approval2.setStatus(EApprovalNoteStatus.WAIT_APP.toString());
        approval2.setDate(DatetimeUtil.daysBeforeToday(1));
        ApprovalNoteModelData approval3 = new ApprovalNoteModelData();
        approval3.setStatus(EApprovalNoteStatus.RETAKE.toString());
        approval3.setDate(DatetimeUtil.daysBeforeToday(2));

        List<ApprovalNoteModelData> approvals = new ArrayList<ApprovalNoteModelData>();
        approvals.add(approval1);
        approvals.add(approval2);
        approvals.add(approval3);

        TaskModelData task = new TaskModelData();
        task.setProjectName("UI Test Project");
        task.setWorkObjectParentsName("Persos");
        task.setWorkObjectName("Sam");
        task.setTaskTypeName("Modeling");

        ApprovalNoteModelData newApproval = this.model.getNewTaskTypeNode(approvals.iterator(), task);

        assertEquals(
                task.getProjectName().toUpperCase() + " > " + task.getWorkObjectParentsName() + " "
                        + task.getWorkObjectName(), newApproval.get(ApprovalNoteModelData.WORK_OBJECT_PATH));
        assertEquals(HtmlUtils.BLANK_CHAR + task.getWorkObjectName() + " " + HtmlUtils.BLANK_CHAR
                + HtmlUtils.BLANK_CHAR + " (" + task.getTaskTypeName() + ") " + HtmlUtils.BLANK_CHAR,
                newApproval.getApprovalTypeName());

        assertEquals(EApprovalNoteStatus.RETAKE.toString(), newApproval.get(ApprovalNoteModelData.DISPLAY_STATUS_FIELD));
        assertEquals(EApprovalNoteStatus.RETAKE.toString(), newApproval.getStatus());
        assertEquals(approval1.getDate(), newApproval.getDate());
    }
}
