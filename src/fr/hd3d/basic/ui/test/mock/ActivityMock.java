package fr.hd3d.basic.ui.test.mock;

import fr.hd3d.basic.ui.client.portlet.activity.IActivityPortlet;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


public class ActivityMock implements IActivityPortlet
{
    private boolean isInitializedWidgets = false;
    private boolean isSelectionReseted = false;
    private boolean isTitleUpdated = false;
    private boolean isEnabledPreviousButton = true;
    private boolean isEnabledNextButton = true;
    private final boolean isEditable = true;

    public void setInitializedWidgets(boolean value)
    {
        this.isInitializedWidgets = value;
    }

    public boolean getInitilizedWidgets()
    {
        return this.isInitializedWidgets;
    }

    public boolean isSelectionReseted()
    {
        return this.isSelectionReseted;
    }

    public boolean isTitleUpdated()
    {
        return this.isTitleUpdated;
    }

    public void setTitleUpdated(Boolean isTitleUpdated)
    {
        this.isTitleUpdated = isTitleUpdated;
    }

    public void disableNextButton()
    {
        this.isEnabledNextButton = false;
    }

    public boolean isEnabledNextButton()
    {
        return isEnabledNextButton;
    }

    public void enableNextButton()
    {
        this.isEnabledNextButton = true;
    }

    public boolean isEnabledPreviousButton()
    {
        return isEnabledPreviousButton;
    }

    public void disablePreviousButton()
    {
        this.isEnabledPreviousButton = false;
    }

    public void enablePreviousButton()
    {
        this.isEnabledPreviousButton = true;
    }

    public void initWidgets()
    {
        this.isInitializedWidgets = true;
    }

    public void removeSimpleDirty()
    {
        // TODO Auto-generated method stub

    }

    public void removeTaskDirty()
    {
        // TODO Auto-generated method stub

    }

    public void updateDayTitle()
    {
        this.isTitleUpdated = true;
    }

    public void displayTaskSelector()
    {
        // TODO Auto-generated method stub

    }

    public void hideGridHeaders()
    {
        // TODO Auto-generated method stub

    }

    public void showGridHeaders()
    {
        // TODO Auto-generated method stub

    }

    public void openTaskComment(TaskModelData task)
    {
        // TODO Auto-generated method stub

    }

    public void setEditable(boolean b)
    {
        // TODO Auto-generated method stub

    }

    public void resetSelection()
    {
        this.isSelectionReseted = true;
    }

    public void hideTaskSelector()
    {}

    public boolean isEditable()
    {
        return isEditable;
    }

    public TaskActivityModelData getSelectedActivity()
    {
        return null;
    }

    public void displayIdentityDialog(Hd3dModelData workObject, ProjectModelData project)
    {}

    public void displayActivityCommentDialog()
    {}

    public void maskTaskActivityGrid()
    {
        // TODO Auto-generated method stub

    }

}
