package fr.hd3d.basic.ui.test.mock;

import java.util.Date;

import fr.hd3d.basic.ui.client.portlet.timepoint.ITimePointPortlet;
import fr.hd3d.basic.ui.client.portlet.timepoint.TimepointModel;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;


public class TimepointMock implements ITimePointPortlet
{
    private final TimepointModel model;

    private boolean isWidgetInitialized = false;
    private String fieldTotal;
    private Date arrivalTime;
    private Date departureTime;
    private Date beginningBreak;
    private Date endBreak;

    private int activitiesTotal;

    private String totalColor;

    public TimepointMock(TimepointModel model)
    {
        this.model = model;
    }

    /**
     * @return the isWidgetInitialized
     */
    public boolean isWidgetInitialized()
    {
        return isWidgetInitialized;
    }

    /**
     * @param fieldTotal
     *            the fieldTotal to set
     */
    public void setFieldTotal(String fieldTotal)
    {
        this.fieldTotal = fieldTotal;
    }

    /**
     * @return the fieldTotal
     */
    public String getFieldTotal()
    {
        return fieldTotal;
    }

    /**
     * @param arrivalTime
     *            the arrivalTime to set
     */
    public void setArrivalTime(Date arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    /**
     * @return the arrivalTime
     */
    public Date getArrivalTime()
    {
        return arrivalTime;
    }

    /**
     * @param departureTime
     *            the departureTime to set
     */
    public void setDepartureTime(Date departureTime)
    {
        this.departureTime = departureTime;
    }

    /**
     * @return the departureTime
     */
    public Date getDepartureTime()
    {
        return departureTime;
    }

    /**
     * @param beginningBreak
     *            the beginningBreak to set
     */
    public void setBeginningBreak(Date beginningBreak)
    {
        this.beginningBreak = beginningBreak;
    }

    /**
     * @return the beginningBreak
     */
    public Date getBeginningBreak()
    {
        return beginningBreak;
    }

    /**
     * @param endBreak
     *            the endBreak to set
     */
    public void setEndBreak(Date endBreak)
    {
        this.endBreak = endBreak;
    }

    /**
     * @return the endBreak
     */
    public Date getEndBreak()
    {
        return endBreak;
    }

    public void initWidgets()
    {
        this.isWidgetInitialized = true;
    }

    public void updateWidget()
    {
        PersonDayModelData personDay = this.model.getCurrentDay();

        if (personDay != null && personDay.getStartDate1() != null)
        {
            setArrivalTime(personDay.getStartDate1());
        }
        else
        {
            setArrivalTime(null);
        }

        if (personDay != null && personDay.getEndDate2() != null)
        {
            setDepartureTime(personDay.getEndDate2());
        }
        else
        {
            setDepartureTime(null);
        }

        if (personDay != null && personDay.getEndDate1() != null)
        {
            setBeginningBreak(personDay.getEndDate1());
        }
        else
        {
            setBeginningBreak(null);
        }

        if (personDay != null && personDay.getStartDate2() != null)
        {
            setEndBreak(personDay.getStartDate2());
        }
        else
        {
            setEndBreak(null);
        }

        this.updateTotal();
    }

    public void updateTotal()
    {
        PersonDayModelData personDay = this.model.getCurrentDay();
        setFieldTotal(personDay.getPresenceTime());
    }

    public int getActivitiesTotal()
    {
        return this.activitiesTotal;
    }

    public void setActivitiesTotal(int activitiesTotal)
    {
        this.activitiesTotal = activitiesTotal;
    }

    public String getTotalcolor()
    {
        return this.totalColor;
    }

    public void setTotalColor(String cssColor)
    {
        this.totalColor = cssColor;
    }

    public void show()
    {
    // TODO Auto-generated method stub

    }

    public String getTitle()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void hide()
    {
    // TODO Auto-generated method stub

    }

}
