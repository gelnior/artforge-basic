package fr.hd3d.basic.ui.test.mock;

import java.util.List;
import java.util.Map;

import fr.hd3d.basic.ui.client.portlet.activity.model.reader.IMonthTotalReader;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.test.util.modeldata.reader.JSONReaderMock;


public class MonthTotalReaderMock implements IMonthTotalReader
{
    public Long getTotal(String json)
    {
        JSONReaderMock reader = new JSONReaderMock();
        @SuppressWarnings("unchecked")
        Map<String, Object> object = (Map<String, Object>) reader.readString(json);
        @SuppressWarnings("unchecked")
        List<Object> resultList = (List<Object>) object.get(Hd3dModelData.ROOT);
        @SuppressWarnings("unchecked")
        Map<Object, Object> resultMap = (Map<Object, Object>) resultList.get(0);
        String userId = MainModel.currentUser.getId().toString();
        Long duration = (Long) resultMap.get(userId);
        return duration.longValue();
    }
}
