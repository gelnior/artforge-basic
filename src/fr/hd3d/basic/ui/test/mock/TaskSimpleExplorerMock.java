package fr.hd3d.basic.ui.test.mock;

import fr.hd3d.basic.ui.client.portlet.activity.widget.ITaskSimpleExplorer;
import fr.hd3d.basic.ui.client.portlet.activity.widget.TaskSimpleExplorerModel;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.test.widget.simpleexplorer.SimpleExplorerPanelMock;


public class TaskSimpleExplorerMock extends SimpleExplorerPanelMock<TaskModelData> implements ITaskSimpleExplorer
{
    private final Long projectFilterTaskType = -1L;
    private Boolean isProjectFieldsEnabled = Boolean.FALSE;
    private Boolean isNonProjectFieldsCleared = Boolean.FALSE;

    public TaskSimpleExplorerMock(TaskSimpleExplorerModel model)
    {
        super(model);
    }

    public Boolean getIsProjectFieldsEnabled()
    {
        return isProjectFieldsEnabled;
    }

    public void setIsProjectFieldsEnabled(Boolean isProjectFieldsEnabled)
    {
        this.isProjectFieldsEnabled = isProjectFieldsEnabled;
    }

    public Boolean getIsNonProjectFieldsCleared()
    {
        return isNonProjectFieldsCleared;
    }

    public void setIsNonProjectFieldsCleared(Boolean isNonProjectFieldsCleared)
    {
        this.isNonProjectFieldsCleared = isNonProjectFieldsCleared;
    }

    public Long getProjectFilterTaskType()
    {
        return projectFilterTaskType;
    }

    public void clearNonProjectFields()
    {
        this.setIsNonProjectFieldsCleared(Boolean.TRUE);
    }

    public void enableNonProjectFields()
    {
        this.setIsProjectFieldsEnabled(Boolean.TRUE);
    }

    public void disableNonProjectFields()
    {
        this.setIsProjectFieldsEnabled(Boolean.FALSE);
    }

}
