package fr.hd3d.basic.ui.test.mock;

import java.util.Set;

import com.google.gwt.dev.util.collect.HashSet;

import fr.hd3d.basic.ui.client.IBasicView;


public class BasicMock implements IBasicView
{

    private int displayedError = -1;
    private int nbColumns;
    private int addedPortletColumn;

    private String displayedUserMsg;
    private String displayedStack;

    private boolean displayedStartPanel;
    private boolean displayedWidget;

    // Portlets displayed on the view
    private final Set<String> displayedPortlets = new HashSet<String>();
    // Portlets checked
    private final Set<String> checkedPortlets = new HashSet<String>();
    // Portlets added in the model
    private final Set<String> addedPortlets = new HashSet<String>();

    private final Set<String> portletTitles = new HashSet<String>();

    public String getDisplayedUserMsg()
    {
        return displayedUserMsg;
    }

    public String getDisplayedStack()
    {
        return displayedStack;
    }

    public boolean getDisplayedStartPanel()
    {
        return this.displayedStartPanel;
    }

    public int getDisplayedError()
    {
        return this.displayedError;
    }

    public Set<String> getDisplayedPortlets()
    {
        return this.displayedPortlets;
    }

    public boolean getDisplayedWidget()
    {
        return this.displayedWidget;
    }

    public int getNbColumns()
    {
        return this.nbColumns;
    }

    public Set<String> getAddedPortletTitle()
    {
        return this.addedPortlets;
    }

    public int getAddedPortletColumn()
    {
        return this.addedPortletColumn;
    }

    public Set<String> getCheckedPortlets()
    {
        return this.checkedPortlets;
    }

    public void setDisplayedStack(String stack)
    {
        this.displayedStack = stack;
    }

    public void setDisplayedUserMsg(String userMsg)
    {
        this.displayedUserMsg = userMsg;
    }

    public void setDisplayedStartPanel(boolean value)
    {
        this.displayedStartPanel = value;
    }

    public void setDisplayedPortlets(String title)
    {
        this.displayedPortlets.add(title);
    }

    public void setDisplayWidget(boolean value)
    {
        this.displayedWidget = value;
    }

    public void setNbColumns(int nbColumns)
    {
        this.nbColumns = nbColumns;
    }

    public void setAddedPortletTitle(String addedPortletTitle)
    {
        this.addedPortlets.add(addedPortletTitle);
    }

    public void setAddedPortletColumn(int addedPortletColumn)
    {
        this.addedPortletColumn = addedPortletColumn;
    }

    public void setCheckedPortletColumn(String title)
    {
        this.checkedPortlets.add(title);
    }

    public void displayError(Integer error)
    {
        this.displayedError = error;
    }

    public void displayError(Integer error, String stack)
    {
        this.displayedError = error;
        this.displayedStack = stack;
    }

    public void displayError(Integer error, String userMsg, String stack)
    {
        this.displayedError = error;
        this.displayedStack = stack;
        this.displayedUserMsg = userMsg;
    }

    public boolean isPortletItemChecked(String portletTitle)
    {
        boolean result = false;
        for (String checked : this.checkedPortlets)
        {
            if (portletTitle == checked)
            {
                result = true;
            }
        }
        return result;
    }

    public void setPortal(Integer nbColumns)
    {
        this.nbColumns = nbColumns;
    }

    public void showPortlet(String portletTitle)
    {
        this.displayedPortlets.add(portletTitle);
    }

    public void hidePortlet(String portletTitle)
    {
        this.displayedPortlets.remove(portletTitle);
    }

    public void showWidget()
    {
        this.displayedWidget = true;
    }

    public void hideStartPanel()
    {
        this.displayedStartPanel = false;

    }

    public void showStartPanel()
    {
        this.displayedStartPanel = true;
    }

    public void checkPortlet(String title)
    {
        this.checkedPortlets.add(title);
    }

    public void addPortlet(String title, int i)
    {
        this.addedPortlets.add(title);

    }

    public Set<String> getPortletTitles()
    {
        return portletTitles;
    }

    public void addPortletTitles(String title)
    {
        this.portletTitles.add(title);
    }

    public void setTimer(Long refreshTime)
    {}

    public void showScriptRunningPanel()
    {
        // TODO Auto-generated method stub

    }

    public void hideScriptRunningPanel()
    {
        // TODO Auto-generated method stub

    }

}
