package fr.hd3d.basic.ui.test.mock;

import fr.hd3d.basic.ui.client.portlet.validation.IValidationPortlet;
import fr.hd3d.basic.ui.client.portlet.validation.model.ValidationModel;


public class ValidationMock implements IValidationPortlet
{
    // private final ValidationModel model;

    private boolean isWidgetInitialized = false;
    private boolean isGridHeadersDisplayed = false;

    public boolean isWidgetInitialized()
    {
        return isWidgetInitialized;
    }

    public boolean isGridHeadersDisplayed()
    {
        return isGridHeadersDisplayed;
    }

    public ValidationMock(ValidationModel model)
    {
        // this.model = model;
    }

    public void show()
    {
        // TODO Auto-generated method stub

    }

    public void hide()
    {
        // TODO Auto-generated method stub

    }

    public String getTitle()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void initWidgets()
    {
        this.isWidgetInitialized = true;
    }

    public void setContextMenu()
    {
        // TODO Auto-generated method stub

    }

    public void showGridHeaders()
    {
        this.isGridHeadersDisplayed = true;
    }

    public void hideGridHeaders()
    {
        this.isGridHeadersDisplayed = false;
    }

    public void displayTaskType(String taskTypeName)
    {
        // TODO Auto-generated method stub

    }

    public void expandAllTreeGrid()
    {
        // TODO Auto-generated method stub

    }

}
