package fr.hd3d.basic.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by Basic application.
 * 
 * @author HD3D
 */
public class BasicEvents
{
    public static final EventType PERSON_INITIALIZED = new EventType();
    public static final EventType PORTLET_CHECKED = new EventType();
    public static final EventType COLUMNS_CHANGED = new EventType();
    public static final EventType PORTLET_POSITION_CHANGED = new EventType();

    public static final EventType TASK_SELECTED = new EventType();
    public static final EventType REFRESH_TIME_REACHED = new EventType();
    public static final EventType HEADERS_TOGGLE_BUTTON_CLICKED = new EventType();
    public static final EventType REFRESH_BUTTON_CLICKED = new EventType();
}
