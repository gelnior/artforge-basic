package fr.hd3d.basic.ui.client;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.basic.ui.client.config.BasicConfig;
import fr.hd3d.basic.ui.client.event.BasicEvents;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.widget.mainview.MainController;


/**
 * Main controller to handle configuration, error and start events.
 * 
 * @author HD3D
 */
public class BasicController extends MainController
{
    /** Basic view containing main portal. */
    final protected IBasicView view;
    /** Data model */
    final protected BasicModel model;

    /** Default constructor. */
    public BasicController(BasicModel model, IBasicView view)
    {
        super(model, view);

        this.model = model;
        this.view = view;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);

        EventType type = event.getType();

        if (type == BasicEvents.PORTLET_CHECKED)
        {
            this.onPortletChecked(event);
        }
        else if (type == BasicEvents.COLUMNS_CHANGED)
        {
            this.onColumnsChanged(event);
        }
        else if (type == BasicEvents.PORTLET_POSITION_CHANGED)
        {
            this.onPortletPositionChanged(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When settings are initialized, hide start panel then show widget.
     * 
     * @param event
     *            The setting initialized event.
     */
    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        // Retrieve portal column numbers from setting and save it inside model.
        Integer nbColumns = UserSettings.getSettingAsInteger(BasicConfig.COLUMN_PORTAL_SETTING);
        if (nbColumns == null)
        {
            nbColumns = BasicModel.DEFAULT_COLUMN_NUMBER;
        }
        if (nbColumns > BasicModel.MAX_COLUMNS)
        {
            nbColumns = BasicModel.MAX_COLUMNS;
        }
        this.model.setNbColumns(nbColumns);

        // Load widget inside document.
        this.view.hideStartPanel();
        this.view.showWidget();

        this.addPortlets();
        this.showPorltets();

        if (BasicConfig.refreshTime > 0)
            this.view.setTimer(BasicConfig.refreshTime);
    }

    /**
     * Add portlets in the order defined by matrix portlets. If the matrix does not exists all portlet are randomly
     * added to the first column and a new matrix is set when portlet adding is finished.
     */
    private void addPortlets()
    {
        int nbColumns = model.getNbColumns();
        List<List<String>> columns = new ArrayList<List<String>>();
        for (int i = 0; i < nbColumns + 1; i++)
        {
            columns.add(new ArrayList<String>());
        }

        // Retrieve portlet matrix
        String portletPositions = UserSettings.getSetting(BasicConfig.POSITION_PORTLET_SETTING + model.getNbColumns());

        // Case where there is no user setting : all portlets are added to columns alternitavely.
        if (portletPositions == null)
        {
            int col = 0;
            for (String title : this.view.getPortletTitles())
            {
                this.view.addPortlet(title, col);
                columns.get(col).add(title.toLowerCase());
                col++;
                col = col % this.model.getNbColumns();
            }

            this.model.setPortletMatrix(columns);
            this.model.savePortletMatrix();
        }
        // Case where there is user settings
        else
        {
            List<String> addedPortlets = new ArrayList<String>();
            String[] columnsString = portletPositions.split(BasicConfig.PORTLET_MATRIX_COLUMN_SEPARATOR);
            if (columnsString != null)
            {
                for (int i = 0; i < columnsString.length; i++)
                {
                    String columnString = columnsString[i];
                    if (!Util.isEmptyString(columnString))
                    {
                        String[] portletsTitle = columnString.split(BasicConfig.PORTLET_MATRIX_ROW_SEPARATOR);
                        for (String title : portletsTitle)
                        {
                            Boolean isChecked = UserSettings.getSettingAsBoolean(BasicConfig.PORTLET_SUFFIX_SETTING
                                    + title.toLowerCase());

                            this.view.addPortlet(title, i);
                            addedPortlets.add(title);
                            if ((isChecked == null || isChecked))
                            {
                                this.view.checkPortlet(title);
                            }
                            else
                            {
                                this.view.hidePortlet(title);
                            }

                            columns.get(i).add(title);
                        } // End for title
                    } // End if columnString is empty
                } // End for columns
            }

            // Add portlets not defined inside cookies.
            for (String title : this.view.getPortletTitles())
            {
                if (!addedPortlets.contains(title))
                    this.view.addPortlet(title, 0);
            }
            this.model.setPortletMatrix(columns);
        }
    }

    /**
     * Show portlets inside view only if portlet is marked as checked inside user settings.
     */
    private void showPorltets()
    {
        for (String title : view.getPortletTitles())
        {
            Boolean isChecked = UserSettings.getSettingAsBoolean(BasicConfig.PORTLET_SUFFIX_SETTING
                    + title.toLowerCase());
            if ((isChecked == null || isChecked))
            {
                this.view.checkPortlet(title);
            }
        }
    }

    /**
     * When portlet is checked, the corresponding portlet is shown. If it is unchecked, the corresponding portlet is
     * hidden.<br>
     * Shown and hidden status are stored in user settings.
     * 
     * @param event
     *            The portlet checked event.
     */
    private void onPortletChecked(AppEvent event)
    {
        String portletTitle = event.getData();
        if (this.view.isPortletItemChecked(portletTitle))
        {
            UserSettings.setSetting(BasicConfig.PORTLET_SUFFIX_SETTING + portletTitle.toLowerCase(), "true");
            this.view.showPortlet(portletTitle);
        }
        else
        {
            UserSettings.setSetting(BasicConfig.PORTLET_SUFFIX_SETTING + portletTitle.toLowerCase(), "false");
            this.view.hidePortlet(portletTitle);
        }
    }

    /**
     * When number of columns changed, the portal layout is updated with the selected columns number. User settings are
     * updated.
     * 
     * @param event
     */
    private void onColumnsChanged(AppEvent event)
    {
        Integer nbColumns = event.getData();
        if (nbColumns > BasicModel.MAX_COLUMNS)
        {
            nbColumns = BasicModel.MAX_COLUMNS;
        }
        if (nbColumns.intValue() != this.model.getNbColumns())
        {
            this.model.setNbColumns(nbColumns);
            this.view.setPortal(nbColumns);
            this.addPortlets();
            UserSettings.setSetting(BasicConfig.COLUMN_PORTAL_SETTING, nbColumns.toString());
        }
    }

    /**
     * When portlet position changed, the matrix describing portlet positions is updated.
     * 
     * @param event
     */
    private void onPortletPositionChanged(AppEvent event)
    {
        // Event informations.
        Integer row = event.getData(BasicConfig.ROW_EVENT_VAR_NAME);
        Integer column = event.getData(BasicConfig.COLUMN_EVENT_VAR_NAME);
        // Integer startRow = event.getData(BasicConfig.START_ROW_EVENT_VAR_NAME);
        Integer startColumn = event.getData(BasicConfig.START_COLUMN_EVENT_VAR_NAME);
        String title = event.getData(BasicConfig.TITLE_EVENT_VAR_NAME);
        title = title.toLowerCase();

        List<List<String>> columns = this.model.getPortletMatrix();

        // Remove portlet from start position.
        List<String> startCol = columns.get(startColumn);
        startCol.remove(title);

        // Add portlet to arrival position.
        List<String> endCol = columns.get(column);
        if (row < endCol.size())
            endCol.add(row, title);
        else
            endCol.add(title);

        // Save to services new position matrix.
        this.model.savePortletMatrix();
    }

    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(BasicEvents.PERSON_INITIALIZED);
        this.registerEventTypes(BasicEvents.PORTLET_CHECKED);
        this.registerEventTypes(BasicEvents.COLUMNS_CHANGED);
        this.registerEventTypes(BasicEvents.PORTLET_POSITION_CHANGED);
    }
}
