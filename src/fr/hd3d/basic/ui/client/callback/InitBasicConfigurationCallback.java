package fr.hd3d.basic.ui.client.callback;

import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;

import fr.hd3d.basic.ui.client.BasicModel;
import fr.hd3d.basic.ui.client.config.BasicConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.service.callback.InitServicesConfigurationCallback;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Specific configuration callback to handle other Basic parameters : available days (how many days time sheets can
 * navigate on) and forbidden portlets, that list portlets that should not appear inside basic.
 * 
 * @author HD3D
 */
public class InitBasicConfigurationCallback extends InitServicesConfigurationCallback
{

    /**
     * Default constructor.
     * 
     * @param model
     *            Main model of the application.
     */
    public InitBasicConfigurationCallback(MainModel model)
    {
        super(model);
    }

    /**
     * When parsing of common data succeeds, it parses basic specific data from config.xml file.
     */
    @Override
    protected void onParsingSucceed(Document configDocument) throws Exception
    {
        super.onParsingSucceed(configDocument);

        this.parseAvaiableDays(configDocument);
        this.parseExcludedPortlets(configDocument);
        this.parseRefreshTime(configDocument);
        this.parseDayOfWeekBlock(configDocument);
        this.parseDayOfMonthBlock(configDocument);
    }

    /**
     * Extract the day of month when the modification of activity tasks is forbidden for the previous month by
     * application from config.xml file.<br>
     * ex: <dayOfMonthBlock>5</dayOfMonthBlock> => after the 5th of month, the previous month is blocked to
     * modification.
     * 
     * @param configDocument
     *            The document to parse.
     */
    private void parseDayOfMonthBlock(Document configDocument)
    {
        NodeList nodes = configDocument.getElementsByTagName(BasicModel.DAYOFMONTH_BLOCK_XML_TAG);
        if (nodes.getLength() > 0)
        {
            String dayOfMonthString = nodes.item(0).getFirstChild().getNodeValue();

            if (dayOfMonthString != null)
            {
                try
                {
                    int dayOfMonth = Integer.parseInt(dayOfMonthString);
                    BasicConfig.dayOfMonthBlock = dayOfMonth;
                }
                catch (Exception e)
                {
                    ErrorDispatcher.sendError(CommonErrors.BAD_CONFIG_FILE,
                            "DayOfMonthBlock tag from config.xml is malformed.");
                }
            }
        }

    }

    /**
     * Extract the day of week when the modification of activity tasks is forbidden by application from config.xml file.<br>
     * ex: <dayOfWeekBlock>5</dayOfWeekBlock> => after friday the week is blocked to modification.
     * 
     * @param configDocument
     *            The document to parse.
     */
    private void parseDayOfWeekBlock(Document configDocument)
    {
        NodeList nodes = configDocument.getElementsByTagName(BasicModel.DAYOFWEEK_BLOCK_XML_TAG);
        if (nodes.getLength() > 0)
        {
            String dayOfWeekString = nodes.item(0).getFirstChild().getNodeValue();

            if (dayOfWeekString != null)
            {
                try
                {
                    int dayOfWeek = Integer.parseInt(dayOfWeekString);
                    BasicConfig.dayOfWeekBlock = dayOfWeek;
                }
                catch (Exception e)
                {
                    ErrorDispatcher.sendError(CommonErrors.BAD_CONFIG_FILE,
                            "DayOfWeekBlock tag from config.xml is malformed.");
                }
            }
        }
    }

    /**
     * Extract number of days browsable by application from config.xml file.<br>
     * ex: <available-days>3</available-days>
     * 
     * @param configDocument
     *            The document to parse.
     */
    private void parseAvaiableDays(Document configDocument)
    {
        NodeList nodes = configDocument.getElementsByTagName(BasicModel.AVAILABLES_DAY_XML_TAG);
        if (nodes.getLength() > 0)
        {
            String availableDaysString = nodes.item(0).getFirstChild().getNodeValue();

            if (availableDaysString != null)
            {
                try
                {
                    int availableDays = Integer.parseInt(availableDaysString);
                    BasicConfig.availableDays = availableDays;
                }
                catch (Exception e)
                {
                    ErrorDispatcher.sendError(CommonErrors.BAD_CONFIG_FILE,
                            "Available days tag from config.xml is malformed.");
                }
            }
        }
    }

    /**
     * Extract list of portlet that will be not displayed by application from config.xml file. ex:
     * <excluded-portlets><portlet>file</portlet></excluded-portlets>
     * 
     * @param configDocument
     *            The document to parse.
     */
    private void parseExcludedPortlets(Document configDocument)
    {
        NodeList excludedNodes = configDocument.getElementsByTagName(BasicModel.EXCLUDED_PORTLETS_XML_TAG);

        for (int i = 0; i < excludedNodes.getLength(); i++)
        {
            Node node = excludedNodes.item(i);
            String portletName = node.getChildNodes().item(0).getNodeValue();
            BasicConfig.excludedPortets.add(portletName);
        }
    }

    /**
     * Extract refresh time (in seconds) by application from config.xml file.
     * 
     * @param configDocument
     *            The document to parse.
     */
    private void parseRefreshTime(Document configDocument)
    {

        NodeList nodes = configDocument.getElementsByTagName(BasicModel.REFRESH_TIME_TAG);
        if (nodes.getLength() > 0)
        {
            String refreshTimeString = nodes.item(0).getFirstChild().getNodeValue();
            if (refreshTimeString != null)
            {
                try
                {
                    Long refreshTime = Long.parseLong(refreshTimeString);
                    BasicConfig.refreshTime = refreshTime;
                }
                catch (Exception e)
                {
                    ErrorDispatcher.sendError(CommonErrors.BAD_CONFIG_FILE,
                            "Refresh time tag from config.xml is malformed. ");
                }
            }
        }
    }
}
