package fr.hd3d.basic.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'/home/loic-bodilis/donnees/eclipse/workspace/Basic/war/WEB-INF/classes/fr/hd3d/basic/ui/client/constant/BasicConstants.properties'.
 */
public interface BasicConstants extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "AddTask".
   * 
   * @return translated "AddTask"
   */
  @DefaultStringValue("AddTask")
  @Key("AddTask")
  String AddTask();

  /**
   * Translated "Basic".
   * 
   * @return translated "Basic"
   */
  @DefaultStringValue("Basic")
  @Key("Basic")
  String Basic();

  /**
   * Translated "BasicConstants".
   * 
   * @return translated "BasicConstants"
   */
  @DefaultStringValue("BasicConstants")
  @Key("ClassName")
  String ClassName();

  /**
   * Translated "<center>You didn't assigned on this task. <br>You don't modify the status of this task.</center>".
   * 
   * @return translated "<center>You didn't assigned on this task. <br>You don't modify the status of this task.</center>"
   */
  @DefaultStringValue("<center>You didn't assigned on this task. <br>You don't modify the status of this task.</center>")
  @Key("DontModifyStatus")
  String DontModifyStatus();

  /**
   * Translated "One column".
   * 
   * @return translated "One column"
   */
  @DefaultStringValue("One column")
  @Key("OneColumn")
  String OneColumn();

  /**
   * Translated "Planned Tasks".
   * 
   * @return translated "Planned Tasks"
   */
  @DefaultStringValue("Planned Tasks")
  @Key("PlannedTasks")
  String PlannedTasks();

  /**
   * Translated "Time Sheets".
   * 
   * @return translated "Time Sheets"
   */
  @DefaultStringValue("Time Sheets")
  @Key("TimeSheets")
  String TimeSheets();

  /**
   * Translated "Two columns".
   * 
   * @return translated "Two columns"
   */
  @DefaultStringValue("Two columns")
  @Key("TwoColumns")
  String TwoColumns();

  /**
   * Translated "< Type your comment ... >".
   * 
   * @return translated "< Type your comment ... >"
   */
  @DefaultStringValue("< Type your comment ... >")
  @Key("TypeYourComment")
  String TypeYourComment();

  /**
   * Translated "Unplanned Tasks".
   * 
   * @return translated "Unplanned Tasks"
   */
  @DefaultStringValue("Unplanned Tasks")
  @Key("UnplannedTasks")
  String UnplannedTasks();

  /**
   * Translated "Work comments submission.".
   * 
   * @return translated "Work comments submission."
   */
  @DefaultStringValue("Work comments submission.")
  @Key("WorkCommentsSubmission")
  String WorkCommentsSubmission();

  /**
   * Translated "Approvals and Retakes".
   * 
   * @return translated "Approvals and Retakes"
   */
  @DefaultStringValue("Approvals and Retakes")
  @Key("approvalsAndRetakes")
  String approvalsAndRetakes();

  /**
   * Translated "<center>Please set a duration before to set comment.</center>".
   * 
   * @return translated "<center>Please set a duration before to set comment.</center>"
   */
  @DefaultStringValue("<center>Please set a duration before to set comment.</center>")
  @Key("messageTimeBeforeComment")
  String messageTimeBeforeComment();

  /**
   * Translated "No comment".
   * 
   * @return translated "No comment"
   */
  @DefaultStringValue("No comment")
  @Key("noComment")
  String noComment();

  /**
   * Translated "Set comment".
   * 
   * @return translated "Set comment"
   */
  @DefaultStringValue("Set comment")
  @Key("setComment")
  String setComment();

  /**
   * Translated "Show all notes".
   * 
   * @return translated "Show all notes"
   */
  @DefaultStringValue("Show all notes")
  @Key("showAllNotes")
  String showAllNotes();

  /**
   * Translated "week-end".
   * 
   * @return translated "week-end"
   */
  @DefaultStringValue("week-end")
  @Key("weekEnd")
  String weekEnd();

  /**
   * Translated "With comment".
   * 
   * @return translated "With comment"
   */
  @DefaultStringValue("With comment")
  @Key("withComment")
  String withComment();
}
