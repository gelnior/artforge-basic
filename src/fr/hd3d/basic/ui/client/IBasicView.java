package fr.hd3d.basic.ui.client;

import java.util.Set;

import fr.hd3d.common.ui.client.widget.mainview.IMainView;


/**
 * This interface represents the main application view.
 * 
 * @author HD3D
 */
public interface IBasicView extends IMainView
{

    void showWidget();

    boolean isPortletItemChecked(String portletTitle);

    void showPortlet(String portletTitle);

    void hidePortlet(String portletTitle);

    void setPortal(Integer nbColumns);

    Set<String> getPortletTitles();

    void checkPortlet(String title);

    void addPortlet(String title, int i);

    void setTimer(Long refreshTime);

}
