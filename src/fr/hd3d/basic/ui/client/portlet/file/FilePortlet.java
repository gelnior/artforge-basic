package fr.hd3d.basic.ui.client.portlet.file;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.basic.ui.client.portlet.IPortlet;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;
import fr.hd3d.common.ui.client.widget.grid.BaseGroupingView;
import fr.hd3d.common.ui.client.widget.grid.renderer.TypeColorRenderer;


/**
 * TaskPortlet displays currently opened task assigned to the user.
 * 
 * @author HD3D
 */
public class FilePortlet extends Portlet implements IPortlet
{
    /** Model contains data. */
    protected FileModel model = new FileModel();
    /** Widget controller which handle events. */
    protected FileController controller = new FileController(this, model);

    protected final BorderedPanel mainPanel = new BorderedPanel();
    protected BaseGrid<AssetRevisionModelData> inGrid = new BaseGrid<AssetRevisionModelData>(this.model
            .getAssetInStore(), this.getAssetColumnModel());
    protected BaseGrid<AssetRevisionModelData> outGrid = new BaseGrid<AssetRevisionModelData>(this.model
            .getAssetOutStore(), this.getAssetColumnModel());

    private final BaseGrid<FileRevisionModelData> fileGrid = new BaseGrid<FileRevisionModelData>(this.model
            .getFileStore(), this.getFileColumnModel());

    /**
     * Default constructor : initialize data and widgets.
     */
    public FilePortlet()
    {
        EventDispatcher.get().addController(controller);

        this.setStyles();
        this.setFileGrid();
        this.setAssetInGrid();
        this.setAssetOutGrid();
        this.add(mainPanel);
    }

    /**
     * Set portlet styles : title, layout, width...
     */
    private void setStyles()
    {
        this.setLayout(new FitLayout());
        this.setAutoWidth(true);
        this.setHeading("Assets");
        this.setTitle("Assets");

        this.setHeight(400);
    }

    private void setFileGrid()
    {
        BaseGroupingView view = new BaseGroupingView();
        view.setShowGroupedColumn(false);

        view.setGroupRenderer(new GridGroupRenderer() {
            public String render(GroupColumnData data)
            {
                String groupTitle = data.group;

                if (data.models.size() > 0)
                {
                    FileRevisionModelData file = (FileRevisionModelData) data.models.get(0);

                    groupTitle += " (" + file.get(FileModel.ASSET_STATUS) + ")";
                    if ("VALIDATED".equals(FileModel.ASSET_STATUS))
                    {
                        return "<span style=\"color: green;\">" + groupTitle + "</span>";
                    }
                    else
                    {
                        return "<span style=\"color: red;\">" + groupTitle + "</span>";
                    }
                }
                else
                {
                    return groupTitle;
                }
            }
        });
        view.setEnableGroupingMenu(false);

        fileGrid.setView(view);
        fileGrid.setHeight(200);
        fileGrid.setBorders(true);
        fileGrid.addEventSelectionChangedListener(FileEvents.FILE_SELECTED);
        // fileGrid.setHideHeaders(true);
        ContentPanel panel = new ContentPanel();
        panel.setHeading("Files");
        panel.setLayout(new FitLayout());
        panel.add(fileGrid);
        mainPanel.addNorth(panel, 130, 0, 0, 0, 0);
    }

    private void setAssetOutGrid()
    {
        this.inGrid.setHeight(80);
        this.inGrid.setBorders(true);
        this.inGrid.setAutoExpandColumn(AssetRevisionModelData.COMMENT_FIELD);
        this.inGrid.setAutoExpandMax(3000);
        // inGrid.setHideHeaders(true);

        ContentPanel panel = new ContentPanel();
        panel.setHeading("In");
        panel.setLayout(new FitLayout());
        panel.add(this.inGrid);
        this.mainPanel.addCenter(panel, 5, 0, 5, 0);
    }

    private void setAssetInGrid()
    {
        this.outGrid.setHeight(80);
        this.outGrid.setBorders(true);
        this.outGrid.setAutoExpandColumn(AssetRevisionModelData.COMMENT_FIELD);
        this.outGrid.setAutoExpandMax(3000);
        // outGrid.setHideHeaders(true);

        ContentPanel panel = new ContentPanel();
        panel.setHeading("Out");
        panel.setLayout(new FitLayout());
        panel.add(this.outGrid);
        this.mainPanel.addSouth(panel, 110, 0, 0, 0, 0);
    }

    private ColumnModel getFileColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        // Project name column
        GridUtils.addColumnConfig(columns, FileRevisionModelData.KEY_FIELD, "Key", 120);
        GridUtils.addColumnConfig(columns, FileRevisionModelData.VARIATION_FIELD, "Variation", 120);
        GridUtils.addColumnConfig(columns, FileRevisionModelData.REVISION_FIELD, "Revision", 120);
        GridUtils.addColumnConfig(columns, FileRevisionModelData.STATUS_FIELD, "Status", 120);
        GridUtils.addColumnConfig(columns, FileModel.ASSET_VARIATION, "Variation", 120);

        return new ColumnModel(columns);
    }

    private ColumnModel getAssetColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        // Project name column
        ColumnConfig colorColumn = GridUtils.addColumnConfig(columns, AssetRevisionModelData.TASK_TYPE_COLOR_FIELD, "",
                30);
        colorColumn.setRenderer(new TypeColorRenderer<AssetRevisionModelData>());
        GridUtils.addColumnConfig(columns, AssetRevisionModelData.TASK_TYPE_NAME_FIELD, "Type", 120);
        GridUtils.addColumnConfig(columns, AssetRevisionModelData.VARIATION_FIELD, "Variation", 120);
        GridUtils.addColumnConfig(columns, AssetRevisionModelData.STATUS_FIELD, "Status", 120);
        GridUtils.addColumnConfig(columns, AssetRevisionModelData.COMMENT_FIELD, "Comment", 120);

        return new ColumnModel(columns);
    }

    public void showGridHeaders()
    {
        this.fileGrid.showHeaders();
        this.inGrid.showHeaders();
        this.outGrid.showHeaders();
    }

    public void hideGridHeaders()
    {
        this.fileGrid.hideHeaders();
        this.inGrid.hideHeaders();
        this.outGrid.hideHeaders();
    }
}
