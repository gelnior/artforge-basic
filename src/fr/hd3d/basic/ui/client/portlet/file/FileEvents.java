package fr.hd3d.basic.ui.client.portlet.file;

import com.extjs.gxt.ui.client.event.EventType;


public class FileEvents
{

    public static final EventType WORK_OBJECT_ASSETS_RETRIEVED = new EventType();
    public static final EventType FILE_SELECTED = new EventType();

}
