package fr.hd3d.basic.ui.client.portlet.file;

import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.reader.AssetRevisionReader;
import fr.hd3d.common.ui.client.modeldata.reader.FileRevisionReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


/**
 * Model containing data store.
 * 
 * @author HD3D
 */
public class FileModel
{
    protected static final String ASSET_ID = "asset-id";
    protected static final String ASSET_VARIATION = "asset-variation";
    protected static final String ASSET_REVISION = "asset-revision";
    protected static final String ASSET_STATUS = "asset-status";

    /** The store containing data to display. */
    protected ServiceStore<FileRevisionModelData> fileStore = new ServiceStore<FileRevisionModelData>(
            new FileRevisionReader());

    /** Store for incoming asset revision. */
    protected final ServiceStore<AssetRevisionModelData> assetInStore = new ServiceStore<AssetRevisionModelData>(
            new AssetRevisionReader());
    /** Store for outcoming asset revision. */
    protected final ServiceStore<AssetRevisionModelData> assetOutStore = new ServiceStore<AssetRevisionModelData>(
            new AssetRevisionReader());

    public FileModel()
    {
        this.fileStore.groupBy(ASSET_VARIATION);
        this.fileStore.sort(FileRevisionModelData.KEY_FIELD, SortDir.DESC);
    }

    /**
     * @return Store which contains task data.
     */
    public ServiceStore<FileRevisionModelData> getFileStore()
    {
        return fileStore;
    }

    /**
     * @return Store for incoming asset revision.
     */
    public ServiceStore<AssetRevisionModelData> getUpStore()
    {
        return this.assetInStore;
    }

    /**
     * @return Store for outcoming asset revision.
     */
    public ServiceStore<AssetRevisionModelData> getDownStore()
    {
        return assetOutStore;
    }

    public void loadAssets(TaskModelData task)
    {
        if (task != null)
            task.getWorkObjectAssets(FileEvents.WORK_OBJECT_ASSETS_RETRIEVED);
    }

    public void loadFiles(List<AssetRevisionModelData> assets)
    {
        fileStore.removeAll();
        for (final AssetRevisionModelData asset : assets)
        {
            final ServiceStore<FileRevisionModelData> tmpStore = new ServiceStore<FileRevisionModelData>(
                    new FileRevisionReader());
            tmpStore.setPath(asset.getDefaultPath() + "/" + ServicesPath.FILE_REVISIONS);
            tmpStore.addLoadListener(new LoadListener() {
                @Override
                public void loaderLoad(LoadEvent le)
                {
                    List<FileRevisionModelData> files = tmpStore.getModels();
                    for (FileRevisionModelData file : files)
                    {
                        file.set(ASSET_ID, asset.getId());
                        file.set(ASSET_VARIATION, asset.getVariation());
                        file.set(ASSET_REVISION, asset.getRevision());
                        file.set(ASSET_STATUS, asset.getStatus());
                    }
                    fileStore.add(files);
                }
            });
            tmpStore.reload();
        }
    }

    public BaseStore<AssetRevisionModelData> getAssetInStore()
    {
        return this.assetInStore;
    }

    public BaseStore<AssetRevisionModelData> getAssetOutStore()
    {
        return this.assetOutStore;
    }

    public void reloadInStore(Long assetId)
    {
        this.assetInStore.setPath(ServicesPath.ASSET_REVISIONS + assetId + ServicesPath.UP_STREAM);
        this.assetInStore.reload();
    }

    public void reloadOutStore(Long assetId)
    {
        this.assetOutStore.setPath(ServicesPath.ASSET_REVISIONS + assetId + ServicesPath.DOWN_STREAM);
        this.assetOutStore.reload();
    }
}
