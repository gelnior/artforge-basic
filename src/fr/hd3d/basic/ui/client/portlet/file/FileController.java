package fr.hd3d.basic.ui.client.portlet.file;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.basic.ui.client.event.BasicEvents;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;


/**
 * Handles events raised by the task view. React to some events from other portlets.
 * 
 * @author HD3D
 */
public class FileController extends MaskableController
{
    /** Activity portlet. */
    private final FilePortlet view;
    /** Model that handles activity data. */
    private final FileModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            The portlet view.
     * @param model
     *            The model that handles activity data.
     */
    public FileController(FilePortlet view, FileModel model)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == BasicEvents.TASK_SELECTED)
        {
            this.onTaskSelected(event);
        }
        else if (type == FileEvents.WORK_OBJECT_ASSETS_RETRIEVED)
        {
            this.workObjectAssetsRetrieved(event);
        }
        else if (type == FileEvents.FILE_SELECTED)
        {
            this.setFileSelected(event);
        }
        else if (type == BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED)
        {
            this.onHeadersButtonClicked(event);
        }
        this.forwardToChild(event);
    }

    private void onTaskSelected(AppEvent event)
    {
        TaskModelData task = event.getData();
        this.model.getFileStore().removeAll();
        this.model.getAssetInStore().removeAll();
        this.model.getAssetOutStore().removeAll();
        this.model.loadAssets(task);
    }

    private void workObjectAssetsRetrieved(AppEvent event)
    {
        List<AssetRevisionModelData> assets = event.getData();
        this.model.loadFiles(assets);
    }

    private void setFileSelected(AppEvent event)
    {
        FileRevisionModelData file = event.getData();
        Long assetId = file.get(FileModel.ASSET_ID);

        this.model.reloadInStore(assetId);
        this.model.reloadOutStore(assetId);
    }

    /**
     * When header button is clicked grid headers are hidden or shown depending if button is pressed or not.
     * 
     * @param event
     *            Headers button event.
     */
    private void onHeadersButtonClicked(AppEvent event)
    {
        Boolean isPressed = event.getData();
        if (isPressed)
        {
            this.view.showGridHeaders();
        }
        else
        {
            this.view.hideGridHeaders();
        }
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        this.registerEventTypes(BasicEvents.TASK_SELECTED);
        this.registerEventTypes(FileEvents.WORK_OBJECT_ASSETS_RETRIEVED);
        this.registerEventTypes(FileEvents.FILE_SELECTED);
        this.registerEventTypes(BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED);
    }
}
