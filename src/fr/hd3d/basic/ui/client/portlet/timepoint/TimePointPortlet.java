package fr.hd3d.basic.ui.client.portlet.timepoint;

import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.Style.VerticalAlignment;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.form.Time;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.RowData;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;

import fr.hd3d.basic.ui.client.portlet.activity.ActivityPortlet;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.basic.HoursEditor;


/**
 * TimepointPortlet displays currently the arrival and departure times of the user.
 * 
 * @author HD3D
 */
public class TimePointPortlet extends Portlet implements ITimePointPortlet
{

    /** Model contains data. */
    protected TimepointModel model = new TimepointModel();
    /** Widget controller which handles events. */
    protected TimepointController controller = new TimepointController(this, model);

    /** Field to set user arrival time. */
    private final HoursEditor arrivalTime = new HoursEditor(24);
    /** Field to set user departure time. */
    private final HoursEditor departureTime = new HoursEditor(24);
    /** Field to set user beginning Break time. */
    private final HoursEditor beginningBreak = new HoursEditor(24);
    /** Field to set user end break time. */
    private final HoursEditor endBreak = new HoursEditor(24);

    private final ActivityPortlet activityPortlet;

    /** Field to display total time passed at work. */
    private final Label fieldTotal = new Label();

    private final HorizontalPanel main = new HorizontalPanel();

    private static boolean updating = false;

    /**
     * Default constructor : initialize data and widgets.
     */
    public TimePointPortlet(ActivityPortlet activityPortlet)
    {
        this.activityPortlet = activityPortlet;
        EventDispatcher.get().addController(controller);

        this.controller.handleEvent(TimepointEvents.INIT);
    }

    /**
     * Initialize grids,toolbar and styles.
     */
    public void initWidgets()
    {
        this.setStyles();
        this.setForm();
    }

    @Override
    public void show()
    {
        super.show();
    }

    /**
     * Set portlet styles : title, layout, width...
     */
    private void setStyles()
    {
        this.setLayout(new FitLayout());
        this.setHeading("Times");
        this.setTitle("times");
        this.setHeight(110);
        this.setBodyBorder(true);
    }

    /**
     * Build time pointing form.
     */
    private void setForm()
    {

        main.setVerticalAlign(VerticalAlignment.BOTTOM);
        main.setHorizontalAlign(HorizontalAlignment.LEFT);

        main.setSpacing(10);

        LayoutContainer arrivalContainer = createTimeField(arrivalTime, "Arrival", 0, 24,
                TimepointEvents.ARRIVAL_CHANGED, 40);
        LayoutContainer beginningBreakContainer = createTimeField(beginningBreak, "Break Beginning", 0, 24,
                TimepointEvents.BEGINNING_BREAK_CHANGED, 52);
        LayoutContainer endBreakContainer = createTimeField(endBreak, "Break End", 0, 24,
                TimepointEvents.END_BREAK_CHANGED, 56);
        LayoutContainer departureContainer = createTimeField(departureTime, "Leaving", 0, 24,
                TimepointEvents.LEAVING_CHANGED, 76);

        main.add(arrivalContainer);
        main.add(beginningBreakContainer);
        main.add(endBreakContainer);
        main.add(departureContainer);

        LayoutContainer totalContainer = new LayoutContainer();

        fieldTotal.setEnabled(false);
        fieldTotal.setStyleAttribute("textAlign", "center");
        fieldTotal.setStyleAttribute("font-size", "20px");
        totalContainer.setWidth(80);
        totalContainer.add(fieldTotal);

        main.add(totalContainer);

        main.setBorders(true);
        this.add(main);

        main.setScrollMode(Scroll.AUTOX);
    }

    /**
     * Update displayed data with curent day data.
     */
    public void updateWidget()
    {
        PersonDayModelData personDay = this.model.getCurrentDay();

        updating = true;
        if (personDay != null && personDay.getStartDate1() != null)
        {
            arrivalTime.setValueByDate(personDay.getStartDate1());
        }
        else
        {
            arrivalTime.setValue(null);
        }

        if (personDay != null && personDay.getEndDate2() != null)
        {
            departureTime.setValueByDate(personDay.getEndDate2());
        }
        else
        {
            departureTime.setValue(null);
        }

        if (personDay != null && personDay.getEndDate1() != null)
        {
            beginningBreak.setValueByDate(personDay.getEndDate1());
        }
        else
        {
            beginningBreak.setValue(null);
        }

        if (personDay != null && personDay.getStartDate2() != null)
        {
            endBreak.setValueByDate(personDay.getStartDate2());
        }
        else
        {
            endBreak.setValue(null);
        }

        this.updateTotal();
        updating = false;
    }

    /**
     * Update the number of worked hours based on arrival and leaving time.
     */
    public void updateTotal()
    {
        PersonDayModelData personDay = this.model.getCurrentDay();

        fieldTotal.setText(personDay.getPresenceTime());
    }

    /**
     * @return The total number of hour base on time sheet.
     */
    public int getActivitiesTotal()
    {
        return this.activityPortlet.getActivitiesTotal();
    }

    /** Set specific color to total presence hours. */
    public void setTotalColor(String cssColor)
    {
        this.fieldTotal.setStyleAttribute(CSSUtils.COLOR, cssColor);
    }

    /**
     * @param field
     *            Time field to add.
     * @param title
     *            Time field title.
     * @param startHour
     *            Time field start hour.
     * @param endHour
     *            Time field end hour.
     * @param event
     *            Event to forward when time field selection changed.
     * @param defaultValueIndex
     *            Index of value to select by default (4 for 1h, 8 for 2h, 7 for 1h45...)
     * @return Create a time field allowing to select hours from start hour to end hour.
     */
    public static LayoutContainer createTimeField(final HoursEditor field, String title, int startHour, int endHour,
            final EventType event, int defaultValueIndex)
    {
        LayoutContainer container = new LayoutContainer();
        container.setLayout(new FitLayout());
        container.setLayout(new RowLayout());

        field.setFieldLabel(title);
        field.setEditable(false);
        field.setTriggerAction(TriggerAction.ALL);
        field.setTypeAhead(true);
        field.setAutoSelection(defaultValueIndex);

        field.addSelectionChangedListener(new SelectionChangedListener<FieldModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<FieldModelData> se)
            {
                if (!updating)
                {
                    FieldModelData fieldValue = se.getSelectedItem();
                    Time time = new Time();
                    time.setHour(0);
                    time.setMinutes(0);

                    Long secondValue = (Long) fieldValue.getValue();
                    float nbHours = secondValue / 3600;
                    int hours = (int) Math.floor(nbHours);
                    secondValue = secondValue % 3600;

                    if (hours > 0)
                    {
                        time.setHour(hours);
                    }
                    if (secondValue > 0)
                    {
                        time.setMinutes(Math.round(secondValue / 60));
                    }
                    EventDispatcher.forwardEvent(new AppEvent(event, time));
                }
            }
        });
        container.add(new Text(title), new RowData(Style.DEFAULT, 1, new Margins(0, 0, 2, 0)));
        container.add(field);

        return container;
    }
}
