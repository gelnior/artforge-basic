package fr.hd3d.basic.ui.client.portlet.timepoint;

import fr.hd3d.basic.ui.client.portlet.IPortlet;


public interface ITimePointPortlet extends IPortlet
{

    /**
     * Initialize grids,toolbar and styles.
     */
    public abstract void initWidgets();

    /**
     * Displays portlet
     */
    public abstract void show();

    /**
     * Update displayed data with curent day data.
     */
    public abstract void updateWidget();

    /**
     * Update the number of worked hours based on arrival and leaving time.
     */
    public abstract void updateTotal();

    /**
     * @return The total number of hour base on time sheet.
     */
    public abstract int getActivitiesTotal();

    /** Set specific color to total presence hours. */
    public abstract void setTotalColor(String cssColor);

}
