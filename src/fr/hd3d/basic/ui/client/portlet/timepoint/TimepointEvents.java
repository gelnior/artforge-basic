package fr.hd3d.basic.ui.client.portlet.timepoint;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by activity portlet.
 * 
 * @author HD3D
 */
public class TimepointEvents
{
    public static final EventType INIT = new EventType();
    public static final EventType ARRIVAL_CHANGED = new EventType();
    public static final EventType LEAVING_CHANGED = new EventType();
    public static final EventType BEGINNING_BREAK_CHANGED = new EventType();
    public static final EventType END_BREAK_CHANGED = new EventType();
    public static final EventType TIME_POINT_SAVED = new EventType();
    public static final EventType UNCONSISTENT_TIME = new EventType();
    public static final EventType CHECK_TOTALS = new EventType();
}
