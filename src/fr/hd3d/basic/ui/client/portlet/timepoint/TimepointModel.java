package fr.hd3d.basic.ui.client.portlet.timepoint;

import com.extjs.gxt.ui.client.widget.form.Time;

import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;


/**
 * Model containing time point data store.
 * 
 * @author HD3D
 */
public class TimepointModel
{
    /** Day of which arrival and leaving times are currently displayed. */
    private PersonDayModelData currentDay;

    /**
     * @return Day of which arrival and leaving times are currently displayed.
     */
    public PersonDayModelData getCurrentDay()
    {
        return this.currentDay;
    }

    /**
     * set day of which arrival and leaving times are currently displayed.
     * 
     * @param currentDay
     *            The day to set.
     */
    public void setCurrentDay(PersonDayModelData currentDay)
    {
        this.currentDay = currentDay;
        String path = ServicesPath.getOneSegmentPath(ServicesPath.MY + ServicesPath.PERSON_DAYS,
                this.currentDay.getId());
        this.currentDay.setDefaultPath(path);
    }

    /**
     * Save arrival time changes to services.
     * 
     * @param time
     *            First arrival time.
     */
    public void saveArrivalTime(Time time)
    {
        this.currentDay.setStartDate1(time.getDate());
        this.currentDay.save(TimepointEvents.TIME_POINT_SAVED);
    }

    /**
     * Save first leaving time (break beginning).
     * 
     * @param time
     *            First leaving time.
     */
    public void saveBeginningBreak(Time time)
    {
        this.currentDay.setEndDate1(time.getDate());
        this.currentDay.save(TimepointEvents.TIME_POINT_SAVED);
    }

    /**
     * Save arrival time changes to services.
     * 
     * @param time
     *            Second arrival time.
     */
    public void saveEndBreak(Time time)
    {
        this.currentDay.setStartDate2(time.getDate());
        this.currentDay.save(TimepointEvents.TIME_POINT_SAVED);
    }

    /**
     * Save leaving time changes to services.
     * 
     * @param time
     *            Second leaving time.
     */
    public void saveLeavingTime(Time time)
    {
        this.currentDay.setEndDate2(time.getDate());
        this.currentDay.save(TimepointEvents.TIME_POINT_SAVED);
    }

}
