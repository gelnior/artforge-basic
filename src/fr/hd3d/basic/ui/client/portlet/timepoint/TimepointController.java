package fr.hd3d.basic.ui.client.portlet.timepoint;

import java.util.Date;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.form.Time;

import fr.hd3d.basic.ui.client.error.BasicErrors;
import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.util.CSSUtils;


/**
 * Handles events raised by the time point view. React to some events from other portlets.
 * 
 * @author HD3D
 */
public class TimepointController extends MaskableController
{
    /** Time point portlet. */
    private final ITimePointPortlet view;
    /** Model that handles Time point data. */
    private final TimepointModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            The portlet view.
     * @param model
     *            The model that handles activity data.
     */
    public TimepointController(ITimePointPortlet view, TimepointModel model)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == TimepointEvents.INIT)
        {
            this.onInit();
        }
        else if (type == ActivityEvents.DAY_LOADED)
        {
            this.onDayLoaded(event);
        }
        else if (type == ActivityEvents.ACTIVITIES_SAVED)
        {
            this.checkTotals(event, true);
        }
        else if (type == ActivityEvents.TASK_ACTIVITIES_LOADED)
        {
            this.checkTotals(event, true);
        }
        else if (type == TimepointEvents.TIME_POINT_SAVED)
        {
            this.onTimeSaved(event);
        }
        else if (type == TimepointEvents.ARRIVAL_CHANGED)
        {
            this.saveArrivalTime(event);
            this.checkTotals(event, true);
        }
        else if (type == TimepointEvents.LEAVING_CHANGED)
        {
            this.saveLeavingTime(event);
            this.checkTotals(event, true);
        }
        else if (type == TimepointEvents.BEGINNING_BREAK_CHANGED)
        {
            this.saveBeginningBreakTime(event);
            this.checkTotals(event, false);
        }
        else if (type == TimepointEvents.END_BREAK_CHANGED)
        {
            this.saveEndBreakTime(event);
            this.checkTotals(event, false);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.checkTotals(event, false);
        }
        this.forwardToChild(event);
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        this.registerEventTypes(ActivityEvents.TASK_ACTIVITIES_LOADED);
        this.registerEventTypes(ActivityEvents.ACTIVITIES_SAVED);
        this.registerEventTypes(ActivityEvents.DAY_LOADED);

        this.registerEventTypes(TimepointEvents.INIT);
        this.registerEventTypes(TimepointEvents.TIME_POINT_SAVED);
        this.registerEventTypes(TimepointEvents.ARRIVAL_CHANGED);
        this.registerEventTypes(TimepointEvents.LEAVING_CHANGED);
        this.registerEventTypes(TimepointEvents.BEGINNING_BREAK_CHANGED);
        this.registerEventTypes(TimepointEvents.END_BREAK_CHANGED);
        this.registerEventTypes(CommonEvents.ERROR);
    }

    /**
     * Initialize widget and data.
     */
    private void onInit()
    {
        this.view.initWidgets();
    }

    /**
     * When day is loaded, fields are automatically set to arrival and leaving times of current day then total number of
     * hours is updated.
     * 
     * @param event
     *            Day loaded event.
     */
    private void onDayLoaded(AppEvent event)
    {
        PersonDayModelData personDay = event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);
        this.model.setCurrentDay(personDay);
        this.view.updateWidget();
    }

    public void saveArrivalTime(AppEvent event)
    {
        Time time = (Time) event.getData();
        Date arrivalTime = time.getDate();

        Date beginningBreakTime = this.model.getCurrentDay().getEndDate1();
        Date endBreakTime = this.model.getCurrentDay().getStartDate2();
        Date leavingTime = this.model.getCurrentDay().getEndDate2();

        if (checkTime(arrivalTime, beginningBreakTime, endBreakTime, leavingTime))
        {
            this.model.saveArrivalTime(time);
        }
    }

    public void saveLeavingTime(AppEvent event)
    {
        Time time = (Time) event.getData();
        Date arrivalTime = this.model.getCurrentDay().getStartDate1();
        Date beginningBreakTime = this.model.getCurrentDay().getEndDate1();
        Date endBreakTime = this.model.getCurrentDay().getStartDate2();
        Date leavingTime = time.getDate();

        if (checkTime(arrivalTime, beginningBreakTime, endBreakTime, leavingTime))
        {
            this.model.saveLeavingTime(time);
        }
    }

    public void saveBeginningBreakTime(AppEvent event)
    {
        Time time = (Time) event.getData();
        Date beginningBreakTime = time.getDate();

        Date arrivalTime = this.model.getCurrentDay().getStartDate1();
        Date endBreakTime = this.model.getCurrentDay().getStartDate2();
        Date leavingTime = this.model.getCurrentDay().getEndDate2();

        if (checkTime(arrivalTime, beginningBreakTime, endBreakTime, leavingTime))
        {
            this.model.saveBeginningBreak(time);
        }
    }

    public void saveEndBreakTime(AppEvent event)
    {
        Time time = (Time) event.getData();
        Date endBreakTime = time.getDate();

        Date arrivalTime = this.model.getCurrentDay().getStartDate1();
        Date beginningBreakTime = this.model.getCurrentDay().getEndDate1();
        Date leavingTime = this.model.getCurrentDay().getEndDate2();

        if (checkTime(arrivalTime, beginningBreakTime, endBreakTime, leavingTime))
        {
            this.model.saveEndBreak(time);
        }
    }

    /** When time is saved total is updated. */
    private void onTimeSaved(AppEvent event)
    {
        this.view.updateTotal();
    }

    /**
     * Check if data are coherent : arrival before leaving, break before leaving.
     * 
     * @param arrival
     *            Arrival time
     * @param beginningBreak
     *            Break leaving time.
     * @param endBreak
     *            Break come back.
     * @param leaving
     *            Leaving time
     * @return True if data are coherent
     */
    public boolean checkTime(Date arrival, Date beginningBreak, Date endBreak, Date leaving)
    {
        if (endBreak == null)
        {
            if (leaving == null)
            {
                if (arrival != null && beginningBreak != null)
                {
                    if (DatetimeUtil.isTimeBefore(beginningBreak, arrival))
                    {
                        ErrorDispatcher.sendError(BasicErrors.TIME_DEPARTURE_BEFORE_ARRIVAL);
                        return false;
                    }
                }
            }
        }
        else
        {
            if (leaving == null)
            {
                if (arrival != null && beginningBreak == null)
                {
                    if (DatetimeUtil.isTimeBefore(endBreak, arrival))
                    {
                        ErrorDispatcher.sendError(BasicErrors.TIME_DEPARTURE_BEFORE_ARRIVAL);
                        return false;
                    }
                }
                if (arrival != null && beginningBreak != null)
                {
                    if (DatetimeUtil.isTimeBefore(beginningBreak, arrival))
                    {
                        ErrorDispatcher.sendError(BasicErrors.TIME_DEPARTURE_BEFORE_ARRIVAL);
                        return false;
                    }
                }
            }

        }

        if (arrival != null && leaving != null && (endBreak == null || beginningBreak == null))
        {
            if (DatetimeUtil.isTimeBefore(leaving, arrival))
            {
                ErrorDispatcher.sendError(BasicErrors.TIME_DEPARTURE_BEFORE_ARRIVAL);
                return false;
            }
            else
            {
                if (beginningBreak != null
                        && (DatetimeUtil.isTimeBefore(beginningBreak, arrival) || DatetimeUtil.isTimeBefore(leaving,
                                beginningBreak)))
                {
                    ErrorDispatcher.sendError(BasicErrors.TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING);
                    return false;
                }

                if (endBreak != null
                        && (DatetimeUtil.isTimeBefore(endBreak, arrival) || DatetimeUtil
                                .isTimeBefore(endBreak, leaving)))
                {
                    ErrorDispatcher.sendError(BasicErrors.TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING);
                    return false;
                }
            }
        }
        if (arrival != null && leaving == null && endBreak != null && beginningBreak != null)
        {
            if (DatetimeUtil.isTimeBefore(beginningBreak, arrival)
                    || DatetimeUtil.isTimeBefore(endBreak, beginningBreak)
                    || DatetimeUtil.isTimeBefore(endBreak, arrival))
            {
                ErrorDispatcher.sendError(BasicErrors.TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING);
                return false;
            }
        }

        if (arrival != null && leaving != null && endBreak != null && beginningBreak != null)
        {
            if (DatetimeUtil.isTimeBefore(beginningBreak, arrival)
                    || DatetimeUtil.isTimeBefore(endBreak, beginningBreak)
                    || DatetimeUtil.isTimeBefore(leaving, endBreak))
            {
                ErrorDispatcher.sendError(BasicErrors.TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING);
                return false;
            }
        }

        return true;
    }

    /**
     * Check if total time > total time from time sheet. If total time is incorrect, the total time colors changes to
     * red. Else it changes to green.
     * 
     * @param event
     *            Check totals event.
     * @param dispatchError
     *            true if error should be dispatch to display that activities are bigger than presence if it is the
     *            case.
     */
    private void checkTotals(AppEvent event, boolean dispatchError)
    {
        Long presenceTime = this.model.getCurrentDay().calculPresenceTime();
        int totalActivities = this.view.getActivitiesTotal();
        boolean isPresenceTimeNok = totalActivities > presenceTime;

        if (presenceTime != 0 && totalActivities != 0 && isPresenceTimeNok)
        {
            if (dispatchError)
            {
                ErrorDispatcher.sendError(BasicErrors.ACTIVITIES_BIGGER_PRESENCE);
            }
            this.view.setTotalColor(CSSUtils.COLOR_RED);
        }
        else
        {
            this.view.setTotalColor(CSSUtils.COLOR_GREEN);
        }
    }
}
