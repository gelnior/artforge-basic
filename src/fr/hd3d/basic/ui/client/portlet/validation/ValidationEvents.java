package fr.hd3d.basic.ui.client.portlet.validation;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events type raised by task portlet.
 * 
 * @author HD3D
 */
public class ValidationEvents
{
    public static final EventType TASK_EDITED = new EventType();
    public static final EventType TASK_SAVED = new EventType();
    public static final EventType INIT = new EventType();
    public static final EventType APPROVAL_LOADED = new EventType();
    public static final EventType SHOW_ALL_APPROVAL_NOTES_CLICKED = new EventType();
}
