package fr.hd3d.basic.ui.client.portlet.validation;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;

import fr.hd3d.basic.ui.client.event.BasicEvents;
import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.basic.ui.client.portlet.activity.widget.ApprovalCommentDialog;
import fr.hd3d.basic.ui.client.portlet.validation.model.ValidationModel;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


/**
 * Handles events raised by the task view. React to some events from other portlets.
 * 
 * @author HD3D
 */
public class ValidationController extends MaskableController
{
    /** Task portlet. */
    private final IValidationPortlet view;
    /** Model that handles task data. */
    private final ValidationModel model;

    /** True if tree grid must be expanded after validation reloading. */
    private boolean expandTreeGridAfterLoading = false;

    public boolean isExpandTreeGridAfterLoading()
    {
        return this.expandTreeGridAfterLoading;
    }

    /**
     * Default constructor.
     * 
     * @param view
     *            The portlet view.
     * @param model
     *            The model that handles activity data.
     */
    public ValidationController(IValidationPortlet view, ValidationModel model)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == ValidationEvents.INIT)
        {
            this.onInit();
        }
        else if (type == BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED)
        {
            this.onHeadersButtonClicked(event);
        }
        else if (type == BasicEvents.TASK_SELECTED)
        {
            this.onTaskSelected(event);
        }
        else if (type == ActivityEvents.TASKS_LOADED)
        {
            this.onTaskLoaded(event);
        }
        else if (type == ValidationEvents.APPROVAL_LOADED)
        {
            this.onApprovalLoaded(event);
        }
        else if (type == ValidationEvents.SHOW_ALL_APPROVAL_NOTES_CLICKED)
        {
            this.onShowAllTask(event);
        }
        else if (type == ActivityEvents.EDIT_LAST_NOTE_CLICKED)
        {
            onEditLastNoteClicked(event);
        }
        else if (type == ActivityEvents.EDIT_LAST_NOTE_FINISHED)
        {
            onEditLastNoteFinished(event);
        }

        this.forwardToChild(event);
    }

    /** When basic is initialized, the task portlet widgets and data are loaded. */
    private void onInit()
    {
        this.view.initWidgets();
    }

    /**
     * When header button is clicked grid headers are hidden or shown depending if button is pressed or not.
     * 
     * @param event
     *            Headers button event.
     */
    private void onHeadersButtonClicked(AppEvent event)
    {
        Boolean isPressed = event.getData();
        if (isPressed)
        {
            this.view.showGridHeaders();
        }
        else
        {
            this.view.hideGridHeaders();
        }
    }

    /**
     * When task is selected corresponding validations are retrieved (it means validation with same task type on same
     * work object).
     * 
     * @param event
     *            Task selected event.
     */
    private void onTaskSelected(AppEvent event)
    {
        TaskModelData task = event.getData();
        if (task == null)
            task = event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);

        if (task != null)
        {
            this.expandTreeGridAfterLoading = true;
            this.model.reloadValidations(task);
        }
    }
 
    /** When Approval is loaded, it updates the treeStore */
    private void onApprovalLoaded(AppEvent event)
    {
        this.buildFilteredTreeStore();
        if (expandTreeGridAfterLoading)
        {
            this.view.expandAllTreeGrid();
        }
    }

    /**
     * Show all task in the tree grid.
     * 
     * @param event
     *            The show all task event.
     */
    private void onShowAllTask(AppEvent event)
    {
        this.expandTreeGridAfterLoading = false;
        this.model.reloadTreeStore4AllTask();
    }

    /**
     * When the task is loaded, set the store and create the tree.
     * 
     * @param event
     *            The task loaded event.
     */
    private void onTaskLoaded(AppEvent event)
    {
        ServiceStore<TaskModelData> store = event.getData();
        if (store != null)
        {
            this.expandTreeGridAfterLoading = false;
            this.model.setTaskStore(store);
            this.model.reloadTreeStore4AllTask();
        }
    }

    /** Update the treeStore with the listStore. */
    public void buildFilteredTreeStore()
    {
        this.model.getApprovalTreeStore().removeAll();
        if (this.model.getApprovalStore().getCount() != 0)
        {
            this.model.updateApprovalMapToFilter();
            TaskModelData currentTask = this.model.getCurrentTask();
            if (currentTask != null)
            {
                this.addTaskToTreeStore(
                        this.model.getApprovalListMapByWorkIdAndTaskTypeId()
                                .get(currentTask.getWorkObjectId().toString() + " "
                                        + currentTask.getTaskTypeId().toString()), currentTask);
            }
            else
            {
                this.addAllTaskToTree();
            }
        }
    }

    /**
     * Add all approvals linked to <i>task</i> inside approval tree.
     * 
     * @param filteredApprovalList
     *            The filtered list of approval (only approvals linked to current task.
     * @param task
     *            The current task.
     */
    private void addTaskToTreeStore(List<ApprovalNoteModelData> filteredApprovalList, TaskModelData task)
    {
        if (filteredApprovalList == null)
            return;

        FastMap<ArrayList<ApprovalNoteModelData>> approvalMap = new FastMap<ArrayList<ApprovalNoteModelData>>();
        FastMap<ApprovalNoteModelData> newestMap = new FastMap<ApprovalNoteModelData>();

        for (ApprovalNoteModelData approval : filteredApprovalList)
        {
            if (approval.getBoundEntityId() != null && task.getWorkObjectId() != null
                    && approval.getBoundEntityId().longValue() != task.getWorkObjectId().longValue())
                continue;

            ArrayList<ApprovalNoteModelData> approvalList = approvalMap.get(approval.getApprovalNoteType().toString());
            ApprovalNoteModelData currentNewestApproval = newestMap.get(approval.getApprovalNoteType().toString());
            if (approvalList == null)
            {
                approvalList = new ArrayList<ApprovalNoteModelData>();
                approvalMap.put(approval.getApprovalNoteType().toString(), approvalList);
                currentNewestApproval = approval;
                newestMap.put(approval.getApprovalNoteType().toString(), currentNewestApproval);
            }

            approvalList.add(approval);
            if (approval.getDate().after(currentNewestApproval.getDate()))
            {
                newestMap.put(approval.getApprovalNoteType().toString(), approval);
            }
        }

        if (approvalMap.isEmpty())
            return;

        ApprovalNoteModelData taskNameApproval = this.model.getNewTaskTypeNode(newestMap.values().iterator(), task);
        this.model.getApprovalTreeStore().add(taskNameApproval, true);

        for (String typeName : approvalMap.keySet())
        {
            ApprovalNoteModelData newest = newestMap.get(typeName);
            ApprovalNoteModelData typeNameApproval = this.model.getNewApprovalTypeNode(newest);

            this.model.getApprovalTreeStore().add(taskNameApproval, typeNameApproval, true);
            this.model.getApprovalTreeStore().add(typeNameApproval, approvalMap.get(typeName), true);
        }
    }

    /**
     * Add all task to tree. For each task, we add all approval notes regrouped by approval note type.
     */
    private void addAllTaskToTree()
    {
        if (this.model.getApprovalListMapByWorkIdAndTaskTypeId() == null)
            return;

        for (TaskModelData task : this.model.getTaskStore().getModels())
        {
            String key = task.getWorkObjectId().toString() + " " + task.getTaskTypeId().toString();
            addTaskToTreeStore(this.model.getApprovalListMapByWorkIdAndTaskTypeId().get(key), task);
        }
    }

    /**
     * When edit last note button is clicked, it displays a dialog linked to last approval note set on currently
     * selected task. This dialog allows to edit approval note comment. When approval is saved via the dialog, it
     * forwards the EDIT_LAST_NOTE_FINISHED event.
     */
    private void onEditLastNoteClicked(AppEvent event)
    {
        ApprovalNoteModelData approval = this.model.getLastApprovalNote();
        if (approval != null
                && (approval.getStatus().equals(ETaskStatus.STAND_BY.toString())
                        || approval.getStatus().equals(ETaskStatus.WORK_IN_PROGRESS.toString()) || approval.getStatus()
                        .equals(ETaskStatus.WAIT_APP.toString())))
        {
        	if (!approval.getDefaultPath().startsWith(ServicesPath.MY))
        		approval.setDefaultPath(ServicesPath.MY + approval.getDefaultPath());
            ApprovalCommentDialog.get("Edit last note comment").show(approval);
        }
        else if (approval != null && !approval.getStatus().equals(ETaskStatus.STAND_BY.toString())
                && !approval.getStatus().equals(ETaskStatus.WORK_IN_PROGRESS.toString())
                && !approval.getStatus().equals(ETaskStatus.WAIT_APP.toString()))
        {
            MessageBox.alert("Error",
                    "To edit last note, task status must be equal to Stand By, Work In Progress or Wait For Approval.",
                    null);
        }
        else
        {
            MessageBox.alert("Error", "There is no note for this task.", null);
        }
    }

    /**
     * When last note has been saved, approval comment is updated inside tree widget.
     * 
     * @param event
     *            The event contains the edited approval note.
     */
    private void onEditLastNoteFinished(AppEvent event)
    {
        ApprovalNoteModelData approval = event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);
        this.model.getApprovalTreeStore().update(approval);
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        this.registerEventTypes(BasicEvents.TASK_SELECTED);
        this.registerEventTypes(BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED);
        this.registerEventTypes(ValidationEvents.INIT);
        this.registerEventTypes(ActivityEvents.TASKS_LOADED);
        this.registerEventTypes(ActivityEvents.EDIT_LAST_NOTE_CLICKED);
        this.registerEventTypes(ActivityEvents.EDIT_LAST_NOTE_FINISHED);
        this.registerEventTypes(ValidationEvents.APPROVAL_LOADED);
        this.registerEventTypes(ValidationEvents.SHOW_ALL_APPROVAL_NOTES_CLICKED);
    }
}
