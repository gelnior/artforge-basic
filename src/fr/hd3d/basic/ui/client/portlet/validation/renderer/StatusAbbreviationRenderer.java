package fr.hd3d.basic.ui.client.portlet.validation.renderer;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;


/**
 * Renderer to display the status of a task in abbreviation format.
 * 
 * @author HD3D
 * 
 */
public class StatusAbbreviationRenderer implements GridCellRenderer<ApprovalNoteModelData>
{
    public Object render(ApprovalNoteModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<ApprovalNoteModelData> store, Grid<ApprovalNoteModelData> grid)
    {

        String status = model.get(property);
        FastMap<String> abbreviationMap = NoteStatusComboBox.getStatusAbbreviationMap();
        String statusAbb = abbreviationMap.get(status);

        String color = TaskStatusMap.getColorForStatus(status);
        if (color != null)
        {
            config.style = "background-color:" + color + ";";
        }

        return statusAbb == null ? "" : "<center>" + statusAbb + "</center>";
    }
}
