package fr.hd3d.basic.ui.client.portlet.validation;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGridView;
import com.google.gwt.core.client.GWT;

import fr.hd3d.basic.ui.client.constant.BasicConstants;
import fr.hd3d.basic.ui.client.portlet.validation.model.ValidationModel;
import fr.hd3d.basic.ui.client.portlet.validation.renderer.ApprovalStatusTreeGridRenderer;
import fr.hd3d.basic.ui.client.portlet.validation.renderer.CommentRenderer;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskStatusRenderer;
import fr.hd3d.common.ui.client.widget.treegrid.BaseTreeGrid;
import fr.hd3d.common.ui.client.widget.treegrid.BaseTreeGridView;


/**
 * ValidationPortlet displays validations linked to currently selected task (in activity portlet). If no task is
 * selected, all validations linked to current day tasks are displayed.
 * 
 * @author HD3D
 */
public class ValidationPortlet extends Portlet implements IValidationPortlet
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static BasicConstants CONSTANTS = GWT.create(BasicConstants.class);

    /** Model contains data. */
    protected ValidationModel model = new ValidationModel();
    /** Widget controller which handle events. */
    protected ValidationController controller = new ValidationController(this, model);

    /** TreeGrid displays validation data grouped by task. */
    protected BaseTreeGrid<ApprovalNoteModelData> grid = new BaseTreeGrid<ApprovalNoteModelData>(
            this.model.getApprovalTreeStore(), this.getColumnModel());

    /**
     * Default constructor : initialize data and widgets.
     */
    public ValidationPortlet()
    {
        EventDispatcher.get().addController(controller);

        this.controller.handleEvent(ValidationEvents.INIT);
    }

    /** Build validation grid. */
    public void initWidgets()
    {
        this.setStyles();
        this.setGrid();
        this.setContextMenu();
    }

    /** Set a context menu to let user display all validation after displaying validation for a specific task. */
    public void setContextMenu()
    {
        EasyMenu menu = new EasyMenu();
        menu.addItem(CONSTANTS.showAllNotes(), ValidationEvents.SHOW_ALL_APPROVAL_NOTES_CLICKED,
                Hd3dImages.getRefreshIcon());
        this.grid.setContextMenu(menu);
    }

    /**
     * Show portlet grid headers.
     */
    public void showGridHeaders()
    {
        this.grid.showHeaders();
    }

    /**
     * Hide portlet grid headers.
     */
    public void hideGridHeaders()
    {

        this.grid.hideHeaders();
    }

    /**
     * Expand the treeGrid
     */
    public void expandAllTreeGrid()
    {
        this.grid.expandAll();
    }

    /**
     * Set portlet styles : title, layout, width...
     */
    private void setStyles()
    {
        this.setHeight(300);
        this.setLayout(new FitLayout());
        this.setHeading(CONSTANTS.approvalsAndRetakes());
        // this.setTitle(CONSTANTS.approvalsAndRetakes());
    }

    /**
     * Set grid which displays tasks assigned to currently connected user.
     */
    private void setGrid()
    {
        TreeGridView view = new BaseTreeGridView();

        this.grid.setView(view);
        this.grid.setBorders(true);
        this.grid.setHeight(180);
        this.grid.getStyle().setNodeCloseIcon(null);
        this.grid.getStyle().setNodeOpenIcon(null);
        this.grid.setAutoExpandMax(3000);
        this.grid.setAutoExpandColumn(ApprovalNoteModelData.COMMENT_FIELD);
        this.grid.getTreeView().setBufferEnabled(false);
        this.add(grid);
    }

    /**
     * @return Column list for task grid.
     */
    private ColumnModel getColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        ColumnConfig workObjectColumn = GridUtils.addColumnConfig(columns, ApprovalNoteModelData.APPROVAL_TYPE_NAME,
                "Work Object", 120);
        workObjectColumn.setRenderer(new ApprovalStatusTreeGridRenderer());
        workObjectColumn.setSortable(false);

        ColumnConfig workObjectPathColumn = GridUtils.addColumnConfig(columns, ApprovalNoteModelData.WORK_OBJECT_PATH,
                "Work object path", 120);
        workObjectPathColumn.setSortable(false);

        ColumnConfig dateConfig = GridUtils.addColumnConfig(columns, ApprovalNoteModelData.DATE_FIELD,
                COMMON_CONSTANTS.Date(), 110);
        dateConfig.setDateTimeFormat(DateFormat.FRENCH_DATE_TIME);
        dateConfig.setSortable(false);

        ColumnConfig approverColumn = GridUtils.addColumnConfig(columns, ApprovalNoteModelData.APPROVER_NAME_FIELD,
                "Approver", 90);
        approverColumn.setSortable(false);

        ColumnConfig statusColumn = GridUtils.addColumnConfig(columns, ApprovalNoteModelData.STATUS_FIELD,
                COMMON_CONSTANTS.Status(), 80);
        statusColumn.setSortable(false);
        // statusColumn.setRenderer(new StatusAbbreviationRenderer());
        statusColumn.setRenderer(new TaskStatusRenderer<Hd3dModelData>());

        ColumnConfig commentColumn = GridUtils.addColumnConfig(columns, ApprovalNoteModelData.COMMENT_FIELD,
                COMMON_CONSTANTS.Comment());
        commentColumn.setSortable(false);
        commentColumn.setRenderer(new CommentRenderer());

        return new ColumnModel(columns);
    }
}
