package fr.hd3d.basic.ui.client.portlet.validation.renderer;

import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.tips.QuickTip;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.util.HtmlUtils;


/**
 * Class used to render note comment. It changes http:// strings into hyperlinks and transforms carriage returns in <br />
 * . Moreover it allows carriages when end of the line is reached.
 * 
 * @author HD3D
 */
public class CommentRenderer implements GridCellRenderer<ApprovalNoteModelData>
{
    private QuickTip quickTip;

    public Object render(final ApprovalNoteModelData model, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<ApprovalNoteModelData> store, Grid<ApprovalNoteModelData> grid)
    {
        if (quickTip == null)
        {
            this.createQuickType(grid);
        }
        FlowPanel panel = new FlowPanel();
        if (model.getComment() != null)
        {
            panel.add(new Text(getHtmlRepresentation(model.getComment())));
        }
        panel.addStyleName(" cell-long-text");
        if (model.getFileRevision() != null && !model.getFileRevision().isEmpty())
        {
            Image image = Hd3dImages.getFileIcon().createImage();
            image.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event)
                {
                    List<Long> fileRevisionsId = model.getFileRevision();
                    // ProxyPlayerDialog.get(fileRevisionsId).show();
                }
            });
            DOM.setStyleAttribute(image.getElement(), "cursor", "pointer");

            panel.add(image);
        }
        return panel;
    }

    /**
     * @param b
     * @return Given text as a tooltiped and styled span.
     */
    private String getHtmlRepresentation(String text)
    {
        String html = "";
        if (text != null)
            text = text.replace("\n", "<br />");

        if (text != null)
            html += "<span qtip='" + text.replace("'", "").replaceAll("\n", "<br />")
                    + "'  style=\"padding-left: 10px\">";

        html += HtmlUtils.changeToHyperText(text);

        if (text != null)
            html += "</span>";

        return html;
    }

    /**
     * Create tooltip that displays commen as tooltip when mouse is over comment span.
     * 
     * @param grid
     *            The grid on which comment will appear.
     */
    private void createQuickType(Grid<? extends Hd3dModelData> grid)
    {
        quickTip = new QuickTip(grid);
        quickTip.initTarget(grid);
        quickTip.getToolTipConfig().setShowDelay(200);
        quickTip.setInterceptTitles(false);
        quickTip.setShadow(false);
        quickTip.setBodyBorder(false);
        quickTip.setBorders(false);
    }
}
