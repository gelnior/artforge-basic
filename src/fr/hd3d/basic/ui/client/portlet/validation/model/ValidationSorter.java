package fr.hd3d.basic.ui.client.portlet.validation.model;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreSorter;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


/**
 * ValidationSorter sorts approvals by their status (NOK -> Done -> OK) then by their date.
 * 
 * @author HD3D
 */
public class ValidationSorter extends StoreSorter<ApprovalNoteModelData>
{
    /**
     * ValidationSorter sorts approvals by their status (NOK -> Done -> OK) then by their date.
     * 
     * @param store
     *            The store to sort.
     * @param m1
     *            First approval to compare.
     * @param m2
     *            Second approval to compare.
     * @param property
     *            Field not used by sorter.
     */
    @Override
    public int compare(Store<ApprovalNoteModelData> store, ApprovalNoteModelData m1, ApprovalNoteModelData m2,
            String property)
    {
        int compare = 0;

        if (m1.get(ApprovalNoteModelData.WORK_OBJECT_PATH) != null
                && m2.get(ApprovalNoteModelData.WORK_OBJECT_PATH) != null)
        {
            if (m1.getStatus() == null || m2.getStatus() == null)
            {
                compare = 0;
            }
            else
            {
                compare = getOrder(m2).compareTo(getOrder(m1));
            }

            if (compare == 0)
            {
                if (m1.getDate() == null || m2.getDate() == null)
                {
                    compare = 0;
                }
                else
                {
                    compare = m2.getDate().compareTo(m1.getDate());
                }
            }
        }
        else
        {
            if (m1.getDate() == null || m2.getDate() == null)
            {
                compare = 0;
            }
            else
            {
                compare = m2.getDate().compareTo(m1.getDate());
            }
        }
        return compare;
    }

    /**
     * This method specifies a specific ordering for approval status.
     * 
     * @param approval
     *            The approval of which status need an order.
     * @return 1 for OK, 2 for Done and 3 for NOK.
     */
    static public Integer getOrder(ApprovalNoteModelData approval)
    {
        if (approval.isStatusWIP())
            return 1;
        else if (approval.isStatusStandby())
            return 2;
        else if (approval.isStatusTodo())
            return 3;
        else if (approval.isStatusOk())
            return 4;
        else if (approval.isStatusDone() || approval.isStatusWaitApp())
            return 5;
        else if (approval.isStatusNOk() || approval.isStatusRetake())
            return 6;
        else
            return 0;
    }
};
