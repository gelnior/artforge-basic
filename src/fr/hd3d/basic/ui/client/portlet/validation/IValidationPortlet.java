package fr.hd3d.basic.ui.client.portlet.validation;

import fr.hd3d.basic.ui.client.portlet.IPortlet;


public interface IValidationPortlet extends IPortlet
{

    /** Build task grid. */
    public abstract void initWidgets();

    /** Set context menu on validation portlet. */
    public abstract void setContextMenu();

    /** Show portlet grid headers. */
    public abstract void showGridHeaders();

    /** Hide portlet grid headers. */
    public abstract void hideGridHeaders();

    /** Expand the fully the tree grid. */
    public abstract void expandAllTreeGrid();

}
