package fr.hd3d.basic.ui.client.portlet.validation.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;


/**
 * Display cell data over a background color depending on approval status.
 * 
 * TODO : Rewrite it to not modified current model during rendering.
 * 
 * @author HD3D
 */
public class ApprovalStatusTreeGridRenderer extends TreeGridCellRenderer<ApprovalNoteModelData>
{
    @Override
    public Object render(ApprovalNoteModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<ApprovalNoteModelData> store, Grid<ApprovalNoteModelData> grid)
    {

        String status = model.get(ApprovalNoteModelData.DISPLAY_STATUS_FIELD);
        if (status == null)
        {
            model.setApprovalTypeName("");
        }
        else
        {
            String color = TaskStatusMap.getColorForStatus(status);
            if (color != null)
            {
                config.style = "background-color:" + color + ";";
            }
        }

        return super.render(model, property, config, rowIndex, colIndex, store, grid);
    }
}
