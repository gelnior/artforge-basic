package fr.hd3d.basic.ui.client.portlet.validation.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.basic.ui.client.portlet.validation.ValidationEvents;
import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.NoteTaskIds;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.HtmlUtils;


/**
 * 
 * @author HD3D
 */
public class ValidationModel
{

    /** This store containing the approval notes. */
    protected ServiceStore<ApprovalNoteModelData> approvalStore = new ServiceStore<ApprovalNoteModelData>(
            ReaderFactory.getApprovalNoteReader());
    /** This treeStore containing the approval notes to print in the portlet. */
    protected TreeStore<ApprovalNoteModelData> approvalTreeStore = new TreeStore<ApprovalNoteModelData>();
    /** This store containing the list of tasks assigned to user. */
    protected ServiceStore<TaskModelData> taskStore = null;

    /** Constraint on the workObject for the approval note store. */
    protected Constraint workObjectConstraint = new Constraint(EConstraintOperator.in,
            ApprovalNoteModelData.BOUND_ENTITY_ID);
    /** Constraint on the task type for the approval note store. */
    protected Constraint taskTypeConstraint = new Constraint(EConstraintOperator.in, "approvalNoteType.taskType.id");
    /** Constraints list to set on the approval note store. */
    private final Constraints constraints = new Constraints(workObjectConstraint, taskTypeConstraint);

    /** List containing the work Object ID needed by work object constraint. */
    private ArrayList<Long> workObjectIdList = new ArrayList<Long>();
    /** List containing the task type ID needed by task type constraint. */
    private HashSet<Long> taskTypeIdList = new HashSet<Long>();
    /** Current task of which approval notes are displayed (the tree store is filter with its ID). */
    private TaskModelData currentTask = null;
    /** Map containing all approvals for constraints sorted by work object ID and task type ID. */
    private FastMap<ArrayList<ApprovalNoteModelData>> approvalListMapByWorkIdAndTaskTypeId = new FastMap<ArrayList<ApprovalNoteModelData>>();

    /** Sorter to sort the treeStore. */
    private final ValidationSorter sorter = new ValidationSorter();

    /** Constraint on task type for the approval note type Store */
    protected Constraint taskTypeNoteConstraint = new Constraint(EConstraintOperator.eq, "taskType.id");
    /** Constraint on project id for the approval note type store. */
    protected Constraint projectConstraint = new Constraint(EConstraintOperator.eq, "project.id");

    /**
     * Controller : configure stores.
     */
    public ValidationModel()
    {
        this.approvalStore.setPath(ServicesPath.MY + ServicesPath.APPROVAL_NOTES);
        this.approvalStore.addEventLoadListener(ValidationEvents.APPROVAL_LOADED);
        this.approvalTreeStore.setStoreSorter(sorter);
        // this.approvalTypeStore.addParameter(new Constraints(taskTypeNoteConstraint, projectConstraint));
    }

    /**
     * Return the store containing the approval notes.
     * 
     * @return The approval store.
     */
    public ServiceStore<ApprovalNoteModelData> getApprovalStore()
    {
        return approvalStore;
    }

    /**
     * Return the treeStore containing the approval notes.
     * 
     * @return The approval treeStore.
     */
    public TreeStore<ApprovalNoteModelData> getApprovalTreeStore()
    {
        return this.approvalTreeStore;
    }

    /**
     * @return The current task of which approval notes should be displayed.
     */
    public TaskModelData getCurrentTask()
    {
        return currentTask;
    }

    /**
     * @return The store containing the tasks.
     */
    public ServiceStore<TaskModelData> getTaskStore()
    {
        return taskStore;
    }

    /**
     * @return The fastMap containing the list of approval note ordered by work object id and task type id.
     */
    public FastMap<ArrayList<ApprovalNoteModelData>> getApprovalListMapByWorkIdAndTaskTypeId()
    {
        return approvalListMapByWorkIdAndTaskTypeId;
    }

    /**
     * Set the fastMap containing the list of approval note ordered by work object id and task type id.
     * 
     * @param map
     *            The map to set.
     */
    public void setApprovalListMapByWorkIdAndTaskTypeId(FastMap<ArrayList<ApprovalNoteModelData>> map)
    {
        approvalListMapByWorkIdAndTaskTypeId = map;
    }

    /**
     * Reloads approval and filter the tree store for the given task. If task is null, the tree store will be empty.
     * 
     * @param task
     *            The given task.
     */
    public void reloadValidations(TaskModelData task)
    {
        currentTask = task;

        approvalStore.clearParameters();
        if (task.getId() != null)
            approvalStore.addParameter(new NoteTaskIds(Arrays.asList(task.getId())));
        approvalStore.reload();
    }

    /**
     * Set the store containing the list of task and create the treeStore.
     * 
     * @param _taskStore
     *            The store
     */
    public void setTaskStore(ServiceStore<TaskModelData> _taskStore)
    {
        if (this.taskStore == null)
            this.taskStore = _taskStore;
    }

    /**
     * Update the constraints to the approval note store.
     */
    public void updateConstraints()
    {
        this.workObjectIdList = new ArrayList<Long>();
        this.taskTypeIdList = new HashSet<Long>();

        for (TaskModelData task : this.taskStore.getModels())
        {
            this.workObjectIdList.add(task.getWorkObjectId());

            this.taskTypeIdList.add(task.getTaskTypeId());
        }
    }

    /**
     * Reload the data and add them to tree store.
     */
    public void reloadTreeStore4AllTask()
    {
        this.currentTask = null;
        this.updateConstraints();
        this.approvalStore.removeParameter(constraints);
        this.approvalStore.clearParameters();
        if (!workObjectIdList.isEmpty() && !taskTypeIdList.isEmpty())
        {
            this.approvalStore.addParameter(new NoteTaskIds(CollectionUtils.getIds(taskStore.getModels())));
            this.approvalStore.reload();
        }
    }

    /** Updates the map containing all approvalNotes with as key workObjectId and tasktypeId. */
    public void updateApprovalMapToFilter()
    {
        this.approvalListMapByWorkIdAndTaskTypeId.clear();

        for (ApprovalNoteModelData approval : this.getApprovalStore().getModels())
        {
            if (approval.getBoundEntityId() != null && approval.getTaskTypeId() != null)
            {
                String mapKey = approval.getBoundEntityId().toString() + " " + approval.getTaskTypeId().toString();
                ArrayList<ApprovalNoteModelData> approvalList = approvalListMapByWorkIdAndTaskTypeId.get(mapKey);
                if (approvalList == null)
                {
                    approvalList = new ArrayList<ApprovalNoteModelData>();
                    approvalListMapByWorkIdAndTaskTypeId.put(mapKey, approvalList);
                }
                approvalList.add(approval);
            }
            else
            {
                Logger.log("Invalid approval to display.");
            }
        }
    }

    /**
     * @param approval
     *            The approval needed to know the status to set on approval type node.
     * @return New approval type needed to display approval type as a node inside approval tree and to group approvals.
     */
    public ApprovalNoteModelData getNewApprovalTypeNode(ApprovalNoteModelData approval)
    {
        ApprovalNoteModelData typeNameApproval = new ApprovalNoteModelData();
        typeNameApproval.set(ApprovalNoteModelData.DISPLAY_STATUS_FIELD, approval.getStatus());
        typeNameApproval.setApprovalTypeName(HtmlUtils.BLANK_CHAR + approval.getApprovalTypeName()
                + HtmlUtils.BLANK_CHAR);
        return typeNameApproval;
    }

    /**
     * @param approvalIterator
     *            Iterator on list of approval note type nodes set under the task type node. It deduces task type status
     *            from approval type node status.
     * @param task
     * @return New approval to display it as a task type node inside approval tree. It is useful for approval grouping.
     */
    public ApprovalNoteModelData getNewTaskTypeNode(Iterator<ApprovalNoteModelData> approvalIterator, TaskModelData task)
    {
        ApprovalNoteModelData taskNode = new ApprovalNoteModelData();
        String status = "";
        Date mostRecent = null;
        while (approvalIterator.hasNext())
        {
            ApprovalNoteModelData approval = approvalIterator.next();
            boolean statusOk = approval.isStatusOk();
            boolean statusRTK = approval.isStatusRetake();
            boolean statusWAP = approval.isStatusWaitApp();
            if (statusOk)
            {
                if (!status.equals(EApprovalNoteStatus.RETAKE.name())
                        && !status.equals(EApprovalNoteStatus.WAIT_APP.name()))
                    status = EApprovalNoteStatus.OK.name();
            }
            else if (statusWAP)
            {
                if (!status.equals(EApprovalNoteStatus.RETAKE.name()))
                {
                    status = EApprovalNoteStatus.WAIT_APP.name();
                }
            }
            else if (statusRTK)
            {
                status = EApprovalNoteStatus.RETAKE.name();
            }
            else
            {
                if (!status.equals(EApprovalNoteStatus.RETAKE.name())
                        && !status.equals(EApprovalNoteStatus.WAIT_APP.name())
                        && !status.equals(EApprovalNoteStatus.RETAKE.name()))

                    status = approval.getStatus();
            }

            if (mostRecent == null)
            {
                mostRecent = approval.getDate();
            }
            else
            {
                if (mostRecent.before(approval.getDate()))
                {
                    mostRecent = approval.getDate();
                }
            }
        }

        taskNode.set(ApprovalNoteModelData.DISPLAY_STATUS_FIELD, status);
        taskNode.setStatus(status);
        taskNode.setApprovalTypeName(HtmlUtils.BLANK_CHAR + task.getWorkObjectName() + " " + HtmlUtils.BLANK_CHAR
                + HtmlUtils.BLANK_CHAR + " (" + task.getTaskTypeName() + ") " + HtmlUtils.BLANK_CHAR);
        taskNode.setDate(mostRecent);
        taskNode.set(
                ApprovalNoteModelData.WORK_OBJECT_PATH,
                task.getProjectName().toUpperCase() + " > " + task.getWorkObjectParentsName() + " "
                        + task.getWorkObjectName());

        return taskNode;
    }

    public ApprovalNoteModelData getLastApprovalNote()
    {
        if (this.getApprovalStore().getCount() > 0)
            return this.getApprovalStore().getAt(this.getApprovalStore().getCount() - 1);
        else
            return null;
    }
}
