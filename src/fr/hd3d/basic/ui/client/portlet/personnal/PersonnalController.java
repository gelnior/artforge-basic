package fr.hd3d.basic.ui.client.portlet.personnal;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.basic.ui.client.event.BasicEvents;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;


/**
 * Handles events raised by the task view. React to some events from other portlets.
 * 
 * @author HD3D
 */
public class PersonnalController extends MaskableController
{

    @Override
    public void handleEvent(AppEvent event)
    {
        // EventType type = event.getType();
        // if (type == BasicEvents.PERSON_INITIALIZED)
        // {
        // this.onPersonInitialized();
        // }
        this.forwardToChild(event);
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        this.registerEventTypes(BasicEvents.PERSON_INITIALIZED);
    }
}
