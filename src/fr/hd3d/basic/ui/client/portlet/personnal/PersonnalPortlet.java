package fr.hd3d.basic.ui.client.portlet.personnal;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.basic.ui.client.portlet.IPortlet;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.GridUtils;


/**
 * TaskPortlet displays currently opened task assigned to the user.
 * 
 * @author HD3D
 */
public class PersonnalPortlet extends Portlet implements IPortlet
{
    /** Widget controller which handle events. */
    protected PersonnalController controller = new PersonnalController();
    /** Model contains data. */
    protected PersonnalModel model = new PersonnalModel();
    /** The task grid. */
    protected Grid<TaskModelData> taskGrid;

    /**
     * Default constructor : initialize data and widgets.
     */
    public PersonnalPortlet()
    {
        this.model.initTaskStore();

        this.setStyles();
        this.setGrid();
    }

    /**
     * Set portlet styles : title, layout, width...
     */
    private void setStyles()
    {
        this.setLayout(new FitLayout());
        this.setAutoWidth(true);
        this.setHeading("My informations");
        this.setTitle("My informations");
    }

    /**
     * Set grid which displays tasks assigned to currently connected user.
     */
    private void setGrid()
    {
        this.taskGrid = new Grid<TaskModelData>(this.model.getTaskStore(), this.getColumnModel());

        this.taskGrid.setHeight(200);
        this.add(taskGrid);
    }

    /**
     * @return Column list for task grid.
     */
    private ColumnModel getColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        GridUtils.addColumnConfig(columns, TaskModelData.NAME_FIELD, "Name");
        GridUtils.addColumnConfig(columns, TaskModelData.STATUS_FIELD, "Status");
        GridUtils.addColumnConfig(columns, TaskModelData.ACTUAL_START_DATE_FIELD, "Start date");
        GridUtils.addColumnConfig(columns, TaskModelData.ACTUAL_END_DATE_FIELD, "End date");

        return new ColumnModel(columns);
    }
}
