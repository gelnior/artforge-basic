package fr.hd3d.basic.ui.client.portlet.personnal;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Model containing data store.
 * 
 * @author HD3D
 */
public class PersonnalModel
{
    /** The store containing data to display. */
    protected ServiceStore<TaskModelData> taskStore = new ServiceStore<TaskModelData>(new TaskReader());

    /**
     * @return Store which contains task data.
     */
    public ServiceStore<TaskModelData> getTaskStore()
    {
        return taskStore;
    }

    /**
     * Sets right URL for services proxy for store and load data inside it.
     */
    public void initTaskStore()
    {
        this.taskStore.addParameter(new Constraint(EConstraintOperator.eq, TaskModelData.CREATOR_ID_FIELD,
                MainModel.currentUser.getId()));
        this.taskStore.reload();
    }
}
