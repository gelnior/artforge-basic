package fr.hd3d.basic.ui.client.portlet.activity.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.ButtonScale;
import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.AutoTaskTypeComboBox;
import fr.hd3d.common.ui.client.widget.EditableNameGrid;
import fr.hd3d.common.ui.client.widget.ProjectCombobox;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskStatusRenderer;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;


/**
 * Task simple explorer displays available tasks depending on filter set by user. Filter are : project, task type and
 * work object name.
 * 
 * @author HD3D
 */
public class TaskSimpleExplorer extends SimpleExplorerPanel<TaskModelData> implements ITaskSimpleExplorer
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Project combo box used to filter task on project. */
    private final ProjectCombobox projectCombo = new ProjectCombobox();
    /** TaskType combo box used to filter task on task type. */
    private final AutoTaskTypeComboBox typeCombo = new AutoTaskTypeComboBox();
    /** Text field used to filter on work Object name. */
    private final TextField<String> workObjectFilterField = new TextField<String>();

    TaskSimpleExplorerModel taskModel = new TaskSimpleExplorerModel();

    /** @return column model for task selection. */
    private static ColumnModel getColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        GridUtils.addColumnConfig(columns, TaskModelData.WORK_OBJECT_PARENTS_NAME_FIELD, "Path", 180);
        GridUtils.addColumnConfig(columns, TaskModelData.WORK_OBJECT_NAME_FIELD, COMMON_CONSTANTS.WorkObject(), 180);
        GridUtils.addColumnConfig(columns, TaskModelData.WORKER_NAME_FIELD, COMMON_CONSTANTS.Worker(), 160);
        ColumnConfig statusConfig = GridUtils.addColumnConfig(columns, TaskModelData.STATUS_FIELD,
                COMMON_CONSTANTS.Status(), 120);
        statusConfig.setRenderer(new TaskStatusRenderer<TaskModelData>());

        return new ColumnModel(columns);
    }

    /** Default constructor. */
    public TaskSimpleExplorer()
    {
        super(new TaskReader(), getColumnModel());
        this.nameGrid.reconfigure(this.taskModel.getModelStore(), getColumnModel());
        this.model = taskModel;
        this.controller = new TaskSimpleExplorerController(taskModel, this);

        this.setProjectCombo();
        this.setTypeCombo();
        this.setWorkObjectText();

        this.cleanToolbar();
        this.setGrid();
    }

    /** Set work object field value to null. Set Task type combo box value to null. */
    public void clearNonProjectFields()
    {
        this.workObjectFilterField.clear();
        this.typeCombo.clear();
    }

    /** Enable work object field. Enable task type combo box. */
    public void enableNonProjectFields()
    {
        this.workObjectFilterField.enable();
        this.typeCombo.enable();
    }

    /** Disable work object field. Disable task type combo box. */
    public void disableNonProjectFields()
    {
        this.workObjectFilterField.disable();
        this.typeCombo.disable();
    }

    @Override
    protected void setGrid(ColumnModel cm)
    {
        this.nameGrid = new EditableNameGrid<TaskModelData>(this.model.getModelStore());

        if (cm != null)
        {
            nameGrid.reconfigure(this.nameGrid.getStore(), cm);
        }

        BorderLayoutData data = new BorderLayoutData(LayoutRegion.CENTER);
        data.setMargins(new Margins(0, 0, 0, 5));
        this.add(nameGrid, data);
    }

    @Override
    protected void setStyle()
    {
        this.setHeaderVisible(false);
        this.setLayout(new BorderLayout());
        this.setBorders(false);

        this.setSize(500, 307);
    }

    private void setWorkObjectText()
    {
        workObjectFilterField.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                super.componentKeyUp(event);
                EventDispatcher.forwardEvent(ActivityEvents.WORKOBJECT_FILTER_KEY_UP,
                        workObjectFilterField.getRawValue());
            }
        });

        this.workObjectFilterField.setEmptyText("Work object...");
        this.workObjectFilterField.setWidth(100);
        this.workObjectFilterField.disable();
        this.toolBar.add(workObjectFilterField);
    }

    /** Add project combo close to name filter. */
    private void setProjectCombo()
    {
        this.toolBar.add(projectCombo);
        this.projectCombo.setWidth(140);
        this.projectCombo.setEmptyText(COMMON_CONSTANTS.Project() + "...");
        this.projectCombo.addSelectionChangedListener(new EventSelectionChangedListener<ProjectModelData>(
                ActivityEvents.SELECTOR_PROJECT_CHANGED));
    }

    /** Add task type combo close to name filter */
    private void setTypeCombo()
    {
        this.toolBar.add(typeCombo);

        this.model.setTaskTypeStore(typeCombo.getServiceStore());

        this.typeCombo.setWidth(200);
        this.typeCombo.setEmptyText("Search Task Type...");

        this.typeCombo.addSelectionChangedListener(new EventSelectionChangedListener<TaskTypeModelData>(
                ActivityEvents.SELECTOR_TASK_TYPE_CHANGED));

        this.typeCombo.disable();
    }

    /** Remove edition tools from toolbar. */
    private void cleanToolbar()
    {
        this.addToolItem.disable();
        this.addToolItem.hide();
        this.saveToolItem.hide();
        this.saveToolItem.disable();
    }

    /** Hide grid headers and add double click listener on grid. */
    private void setGrid()
    {
        this.nameGrid.setHideHeaders(false);
        this.nameGrid.addListener(Events.OnDoubleClick, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                onGridDoubleClicked();
            }
        });
    }

    /**
     * TODO : set it inside controller. When explorer grid is double clicked, the SELECTOR_TASK_DOUBLE_CLICKED event is
     * raised and double-clicked task is attached to the event.
     */
    private void onGridDoubleClicked()
    {
        TaskModelData task = nameGrid.getSelectionModel().getSelectedItem();
        task.setProjectTypeName(projectCombo.getValue().getProjectTypeName());

        AppEvent event = new AppEvent(ActivityEvents.SELECTOR_TASK_DOUBLE_CLICKED);
        event.setData(task);

        EventDispatcher.forwardEvent(event);
    }

    /**
     * Set tool bar tool items : add, delete, refresh, save and saving status.
     */
    @Override
    protected void setToolbar()
    {
        this.setTopComponent(toolBar);

        this.addToolItem.setScale(ButtonScale.MEDIUM);
        this.saveToolItem.setScale(ButtonScale.MEDIUM);

        this.toolBar.add(addToolItem);
        this.toolBar.add(deleteToolItem);
        this.toolBar.add(refreshToolItem);
        this.toolBar.add(saveToolItem);
        this.toolBar.add(savingToolItem);

        this.toolBar.setStyleAttribute("padding-right", "5px");
        this.savingToolItem.setBusy(CONSTANTS.Saving());
        this.savingToolItem.hide();
    }

}
