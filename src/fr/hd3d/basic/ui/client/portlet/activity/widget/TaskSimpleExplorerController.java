package fr.hd3d.basic.ui.client.portlet.activity.widget;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerController;


/**
 * Task simple explorer controller handle events task selector dialog explorer.
 * 
 * @author HD3D
 */
public class TaskSimpleExplorerController extends SimpleExplorerController<TaskModelData>
{
    /** Simple explorer panel displaying task list. */
    private final ITaskSimpleExplorer panel;
    /** Model handles simple explorer data. */
    private final TaskSimpleExplorerModel taskModel;

    /**
     * Constructor.
     * 
     * @param model
     *            Model handles simple explorer data.
     * @param view
     *            Simple explorer panel displaying task list.
     */
    public TaskSimpleExplorerController(TaskSimpleExplorerModel model, ITaskSimpleExplorer view)
    {
        super(model, view);

        this.panel = view;
        this.taskModel = model;
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(ActivityEvents.SELECTOR_PROJECT_CHANGED);
        this.registerEventTypes(ActivityEvents.SELECTOR_TASK_TYPE_CHANGED);
        this.registerEventTypes(ActivityEvents.WORKOBJECT_FILTER_KEY_UP);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (this.isMasked)
        {
            this.forwardToChild(event);
        }
        else if (type == ActivityEvents.SELECTOR_PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == ActivityEvents.SELECTOR_TASK_TYPE_CHANGED)
        {
            this.onTaskTypeChanged(event);
        }
        else if (type == ActivityEvents.WORKOBJECT_FILTER_KEY_UP)
        {
            this.onWorkObjectKeyUp(event);
        }
        else
        {
            super.handleEvent(event);
        }
    }

    /**
     * When selected project changed, explorer store project path and task type combo store path are updated with
     * project ID. Then, task type field and work object fields are cleared and enabled.
     * 
     * @param event
     *            Project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();

        if (project != null)
        {
            this.taskModel.setProjectConstraintValue(project.getId());
            this.model.getTaskTypeStore().setPath(
                    ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.TASKTYPES);

            this.taskModel.setProxyPath(ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.TASKS);
            this.panel.clearNonProjectFields();
            this.panel.enableNonProjectFields();
            this.model.getTaskTypeStore().reload();
        }
        else
        {
            this.panel.clearNonProjectFields();
            this.panel.disableNonProjectFields();
        }

    }

    /**
     * When selected task type changed, explorer store project constraint is updated with project ID then data are
     * refreshed if a project is selected.
     * 
     * @param event
     *            task type changed event.
     */
    private void onTaskTypeChanged(AppEvent event)
    {
        TaskTypeModelData taskType = event.getData();

        if (taskType != null)
            this.taskModel.setTaskTypeConstraintValue(taskType.getId());

        if (this.taskModel.getProjectId() != null)
        {
            this.model.getModelStore().reload();
            this.panel.enableButtons();
        }
    }

    /**
     * When something is type inside work object filter field, explorer store work object lucene constraint is updated
     * with field value then data are refreshed if a project and task type are selected.
     * 
     * @param event
     *            Key up on work object field event.
     */
    private void onWorkObjectKeyUp(AppEvent event)
    {
        String workObjectFilterValue = event.getData();

        if (workObjectFilterValue != null && this.taskModel.getProjectId() != null
                && this.taskModel.getTaskTypeId() != null)
        {
            this.taskModel.setWorkObjectConstraintValue(workObjectFilterValue);
            this.model.getModelStore().reload();
            this.panel.enableButtons();
        }
    }
}
