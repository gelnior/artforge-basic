package fr.hd3d.basic.ui.client.portlet.activity.widget;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerModel;


/**
 * TaskSimpleExplorerModel handles data for task simple explorer : it manages constraint set on explorer store. It does
 * not retrieve CANCELLED, CLOSE, OK tasks and tasks assigned to current worker. Then data are filtered on set project
 * ID, task type ID and work object name.
 * 
 * @author HD3D
 */
public class TaskSimpleExplorerModel extends SimpleExplorerModel<TaskModelData>
{
    /** Constraint used to filter data on project ID. */
    private final Constraint projectConstraint = new Constraint(EConstraintOperator.eq, "project.id", null);
    /** Constraint used to filter data on task type ID. */
    private final Constraint taskTypeConstraint = new Constraint(EConstraintOperator.eq, "taskType.id", null);
    /** Constraint used to no retrieve tasks from current user. */
    private final LogicConstraint workerConstraint = new LogicConstraint(EConstraintLogicalOperator.OR, new Constraint(
            EConstraintOperator.neq, "worker.id", MainModel.currentUser.getId()), new Constraint(
            EConstraintOperator.isnull, "worker.id"));

    /** Lucene cConstraint used to filter data on work object name. */
    private final LuceneConstraint workObjectConstraint = new LuceneConstraint(EConstraintOperator.prefix, "wo_name",
            "", null);

    /**
     * Constructor : sets constraints on work
     */
    public TaskSimpleExplorerModel()
    {
        super(ReaderFactory.getTaskReader());

        List<String> banStatus = new ArrayList<String>();
        banStatus.add(ETaskStatus.CANCELLED.toString());
        banStatus.add(ETaskStatus.CLOSE.toString());
        banStatus.add(ETaskStatus.OK.toString());
        banStatus.add(ETaskStatus.NEW.toString());
        Constraint statusConstraint = new Constraint(EConstraintOperator.notin, TaskModelData.STATUS_FIELD, banStatus);

        LogicConstraint and = new LogicConstraint(EConstraintLogicalOperator.AND, projectConstraint, taskTypeConstraint);
        LogicConstraint and_bis = new LogicConstraint(EConstraintLogicalOperator.AND, and, statusConstraint);
        LogicConstraint and_ter = new LogicConstraint(EConstraintLogicalOperator.AND, and_bis, workerConstraint);

        this.getModelStore().addParameter(and_ter);
        this.getModelStore().addParameter(workObjectConstraint);
    }

    /**
     * @return Current project ID set in project filter.
     */
    public Long getProjectId()
    {
        return (Long) this.projectConstraint.getLeftMember();
    }

    /**
     * @return Current task type ID set in task type filter.
     */
    public Long getTaskTypeId()
    {
        return (Long) this.taskTypeConstraint.getLeftMember();
    }

    /**
     * Set project ID inside project filter.
     * 
     * @param id
     *            The ID to set.
     */
    public void setProjectConstraintValue(Long id)
    {
        this.projectConstraint.setLeftMember(id);
    }

    /**
     * Set task type ID inside task type filter.
     * 
     * @param id
     *            The ID to set.
     */
    public void setTaskTypeConstraintValue(Long id)
    {
        this.taskTypeConstraint.setLeftMember(id);
    }

    /**
     * Set work object name filter value.
     * 
     * @param workObjectFilterValue
     *            The value to set.
     */
    public void setWorkObjectConstraintValue(String workObjectFilterValue)
    {
        workObjectConstraint.setLeftMember(workObjectFilterValue);
    }
}
