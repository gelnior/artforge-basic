package fr.hd3d.basic.ui.client.portlet.activity.editor;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;


/**
 * Editor that lets user to select a task type model data via its name and that returns as value the task type name.
 * 
 * @author HD3D
 */
public class TaskTypeEditor extends CellEditor
{
    /** The field combo box which act as the editor. */
    private final ModelDataComboBox<TaskTypeModelData> combo;

    /** Default Constructor. */
    public TaskTypeEditor(ModelDataComboBox<TaskTypeModelData> combo)
    {
        super(combo);

        this.combo = combo;
    }

    /**
     * Set task type combo value by selecting a task type of name equals to <i>value</i>.
     * 
     * @param value
     *            Name of the task type to set.
     */
    @Override
    public Object preProcessValue(Object value)
    {
        TaskTypeModelData object;

        if (value == null)
        {
            return null;
        }
        else
        {
            String name = (String) value;

            object = combo.getValueByName(name);

            return object;
        }
    }

    /**
     * @return Selected project name.
     */
    @Override
    public Object postProcessValue(Object value)
    {
        TaskTypeModelData type = (TaskTypeModelData) value;

        if (value == null)// || (name != null && name.equals(type.getName())))
        {
            return null;
        }
        else
        {
            return type.getName();
        }
    }
}
