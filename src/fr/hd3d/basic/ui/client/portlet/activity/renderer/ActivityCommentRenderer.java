package fr.hd3d.basic.ui.client.portlet.activity.renderer;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.core.client.GWT;

import fr.hd3d.basic.ui.client.constant.BasicConstants;
import fr.hd3d.basic.ui.client.portlet.activity.widget.CommentDialog;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;


/**
 * This class displays a button for each row. When button is clicked it pops a dialog that allows to set a comment for
 * corresponding task activity. This dialog is displayed only if activity duration is set else it shows an error message
 * box.
 * 
 * @author hd3d
 * 
 */
public class ActivityCommentRenderer implements GridCellRenderer<TaskActivityModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static BasicConstants CONSTANTS = GWT.create(BasicConstants.class);

    private final String textWarning = CONSTANTS.messageTimeBeforeComment();
    private final String titleWarning = COMMON_CONSTANTS.warning();
    private final String buttonToolTip = CONSTANTS.setComment();

    /** Set to true if comment is editable. */
    private boolean editable = true;

    /**
     * Set to true if comment is editable.
     * 
     * @param editable
     *            The boolean to set.
     */
    public void setEditable(boolean editable)
    {
        this.editable = editable;
    }

    public Object render(final TaskActivityModelData model, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<TaskActivityModelData> store, Grid<TaskActivityModelData> grid)
    {
        final CommentDialog commentDialog = CommentDialog.get(buttonToolTip);
        config.css = "basic-editable";

        Button modifyCommentButton = new Button((model.getComment() != null) ? "modify" : "edit",
                new SelectionListener<ButtonEvent>() {
                    @Override
                    public void componentSelected(ButtonEvent ce)
                    {
                        if (!editable)
                        {
                            return;
                        }

                        if (model.getDuration() == null || model.getDuration().longValue() == 0)
                        {
                            MessageBox.alert(titleWarning, textWarning, null);
                        }
                        else
                        {
                            commentDialog.show(model);
                        }
                    }
                });

        int width = grid.getColumnModel().getColumnWidth(colIndex) - 10;
        modifyCommentButton.setWidth(width);
        modifyCommentButton.setMinWidth(width);
        modifyCommentButton.setToolTip(buttonToolTip);

        return modifyCommentButton;
    }
}
