package fr.hd3d.basic.ui.client.portlet.activity.model.reader;

import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class WeekTotalReader implements IWeekTotalReader
{
    public Long getTotal(String json)
    {
        JSONValue object = JSONParser.parse(json);
        JSONValue resultMap = object.isObject().get(Hd3dModelData.ROOT);

        String userId = MainModel.currentUser.getId().toString();
        Double duration = resultMap.isArray().get(0).isObject().get(userId).isNumber().doubleValue();
        Long total = duration.longValue();

        if (total == null)
        {
            total = 0L;
        }

        return total;
    }
}
