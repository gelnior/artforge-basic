package fr.hd3d.basic.ui.client.portlet.activity.widget;

import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;


/**
 * Comment dialog dialog contains a text area that lets user edit task activity comment. This class also acts as a
 * singleton to not recreate a new comment dialog each time it is displayed.
 * 
 * @author HD3D
 */
public class CommentDialog extends OkDialog
{
    /** Dialog to display for comment dialog. */
    private static CommentDialog dialog;

    /** Text area used to edit activity comment. */
    protected final TextArea commentTextArea = new TextArea();
    /** Activity of which comment is edited. */
    protected TaskActivityModelData model;

    /**
     * @param title
     *            The dialog title.
     * @return Comment dialog instance.
     */
    public static CommentDialog get(String title)
    {
        if (dialog == null)
            dialog = new CommentDialog(title);
        dialog.setHeading(title);

        return dialog;
    }

    /**
     * Default constructor : set styles and layout.
     * 
     * @param heading
     *            The dialog title.
     */
    public CommentDialog(String heading)
    {
        this.setLayout(new BorderLayout());
        this.setBodyBorder(false);
        this.setButtons(Dialog.OKCANCEL);
        this.setHeading(heading);
        this.setWidth(400);
        this.setHeight(225);
        this.setHideOnButtonClick(true);
        this.setModal(true);
        this.add(commentTextArea, new BorderLayoutData(LayoutRegion.CENTER));
    }

    /**
     * Do not use this method to show comment dialog.
     */
    @Override
    public void show()
    {
        GWT.log("Don't user this method, comment dialog must be show for a specific task activity.");
    }

    /***
     * When dialog is displayed, it registers model as activity to edit when comment is modified. It initializes text
     * area with current activity comment.
     * 
     * @param model
     *            The model of which comment is edited.
     */
    public void show(TaskActivityModelData model)
    {
        this.focusComment();
        this.model = model;
        this.commentTextArea.clear();
        this.commentTextArea.setValue(model.getComment());

        super.show();
    }

    /**
     * Set the focus on comment text area.
     */
    public void focusComment()
    {
        this.setFocusWidget(commentTextArea);
    }

    /**
     * When OK button is pressed, activity comment is updated and Task Activity Edited event is dispachted to
     * controllers.
     */
    @Override
    protected void onOkPressed()
    {
        model.setComment(commentTextArea.getValue());
        EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITY_EDITED, model);
    }

}
