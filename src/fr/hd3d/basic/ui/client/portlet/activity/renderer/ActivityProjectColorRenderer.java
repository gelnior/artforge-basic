package fr.hd3d.basic.ui.client.portlet.activity.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.task.ActivityModelData;


/**
 * Fill cell background with the color of the project linked to activity.
 * 
 * @author HD3D
 */
public class ActivityProjectColorRenderer implements GridCellRenderer<ActivityModelData>
{
    /**
     * Fill cell background with the color of the project linked to activity.
     */
    public Object render(ActivityModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<ActivityModelData> store, Grid<ActivityModelData> grid)
    {
        String color = model.get(property);

        return getTypeRendered(color);
    }

    /**
     * @param color
     *            Background color.
     * @return HTML code representing a div with background color equal to <i>color</i>.
     */
    private String getTypeRendered(String color)
    {
        String cellCode = "&nbsp;";
        if (!Util.isEmptyString(color))
        {
            cellCode = "<div style='padding: 3px; text-align:center; background-color:" + color + ";'>" + "&nbsp;"
                    + "</div>";
        }
        return cellCode;
    }

}
