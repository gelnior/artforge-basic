package fr.hd3d.basic.ui.client.portlet.activity.model.reader;

public class WeekTotalReaderSingleton
{
    private static IWeekTotalReader reader;

    public static IWeekTotalReader get()
    {
        if (reader == null)
        {
            reader = new WeekTotalReader();
        }
        return reader;
    }

    public static void setInstance(IWeekTotalReader weekTotalReaderMock)
    {
        reader = weekTotalReaderMock;
    }
}
