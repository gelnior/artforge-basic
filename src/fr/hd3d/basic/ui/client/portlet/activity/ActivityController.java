package fr.hd3d.basic.ui.client.portlet.activity;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;

import fr.hd3d.basic.ui.client.config.BasicConfig;
import fr.hd3d.basic.ui.client.event.BasicEvents;
import fr.hd3d.basic.ui.client.portlet.activity.model.ActivityModel;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseBulkCallback;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.dialog.CommentTaskDialog;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Handles events raised by the task view. React to some events from other portlets.
 * 
 * @author HD3D
 */
public class ActivityController extends MaskableController
{
    /** Activity portlet. */
    private final IActivityPortlet view;
    /** Model that handles activity data. */
    private final ActivityModel model;

    /** Save last edited project to adapt correctly othe fields of simple activity grid. */
    private ProjectModelData lastEditedProject;

    /**
     * Default constructor.
     * 
     * @param view
     *            The portlet view.
     * @param model
     *            The model that handles activity data.
     */
    public ActivityController(IActivityPortlet view, ActivityModel model)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == ActivityEvents.INIT)
        {
            this.onInit();
        }
        else if (type == ActivityEvents.NEXT_CLICKED)
        {
            this.onNextClicked(event);
        }
        else if (type == ActivityEvents.PREVIOUS_CLICKED)
        {
            this.onPreviousClicked(event);
        }
        else if (type == ActivityEvents.REFRESH_ACTIVITIES)
        {
            this.onRefreshActivities();
        }
        else if (type == ActivityEvents.DAY_LOADED)
        {
            this.onDayLoaded();
        }
        else if (type == ActivityEvents.ACTIVITY_WEEK_TOTAL_RETRIEVED)
        {
            this.onWeekTotalRetrieved(event);
        }
        else if (type == ActivityEvents.ACTIVITY_MONTH_TOTAL_RETRIEVED)
        {
            this.onMonthTotalRetrieved(event);
        }
        else if (type == ActivityEvents.TASKS_LOADED)
        {
            this.onTasksLoaded();
        }
        else if (type == ActivityEvents.TASK_ACTIVITIES_LOADED)
        {
            this.onTaskActivitiesLoaded();
        }
        else if (type == ActivityEvents.SIMPLE_ACTIVITIES_LOADED)
        {
            this.onSimpleActivitiesLoaded();
        }
        else if (type == ActivityEvents.TASK_ACTIVITY_EDITED)
        {
            this.onTaskActivityEdited(event);
        }
        else if (type == ActivityEvents.SIMPLE_ACTIVITY_EDITED)
        {
            this.onSimpleActivityEdited(event);
        }
        else if (type == ActivityEvents.SIMPLE_PROJECT_CHANGED)
        {
            this.onSimpleProjectChanged(event);
        }
        else if (type == ActivityEvents.SIMPLE_ACTIVITY_SAVED)
        {
            this.onSimpleActivitiesSaved(event);
        }
        else if (type == ActivityEvents.TASK_ACTIVITY_SAVED)
        {
            this.onTaskActivitiesSaved();
        }
        else if (type == ActivityEvents.ADD_TASK_CLICKED)
        {
            this.onAddTaskClicked();
        }
        else if (type == ActivityEvents.SELECTOR_TASK_DOUBLE_CLICKED)
        {
            this.onSelectorTaskDoubleClicked(event);
        }
        else if (type == ActivityEvents.ALL_OR_PLANNED_CLICKED)
        {
            this.onAllOrPlannedClicked(event);
        }
        else if (type == ActivityEvents.ACTIVITY_DOUBLE_CLICKED)
        {
            this.onActivityDoubleClicked(event);
        }
        else if (type == BasicEvents.REFRESH_TIME_REACHED)
        {
            this.onRefreshRequested();
        }
        else if (type == BasicEvents.REFRESH_BUTTON_CLICKED)
        {
            this.onRefreshRequested();
        }
        else if (type == BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED)
        {
            this.onHeadersButtonClicked(event);
        }
        else if (type == ActivityEvents.TASK_ACTIVITY_SELECTED)
        {
            onTaskActivitySelected(event);
        }
        else if (type == ActivityEvents.STATUS_CHANGED)
        {
            onStatusChanged(event);
        }
        else if (type == ActivityEvents.TASK_EDITED)
        {
            onTaskEdited(event);
        }
        else if (type == ActivityEvents.EDIT_COMMENT_CLICKED)
        {
            onActivityCommentClicked(event);
        }

        this.forwardToChild(event);
    }

    private void onTaskEdited(AppEvent event)
    {
        final List<Hd3dModelData> tasks = event.getData(CommentTaskDialog.TASKS_DATA);

        BulkRequests.bulkPut(tasks, new BaseBulkCallback(tasks) {
            @Override
            protected void onIdsUpdated()
            {
                EventDispatcher.forwardEvent(BasicEvents.TASK_SELECTED, tasks.get(0));
            }
        });
    }

    /**
     * When total activity for current week are loaded, task and simple activity store are reloaded.
     */
    private void onMonthTotalRetrieved(AppEvent event)
    {
        Long total = event.getData();
        this.model.setMonthTotal(total);
        this.view.updateDayTitle();
    }

    private void onStatusChanged(AppEvent event)
    {
        TaskModelData task = event.getData();
        if (task == null)
            return;
        this.view.openTaskComment(task);
    }

    private void onTaskActivitySelected(AppEvent event)
    {
        TaskActivityModelData activity = event.getData();
        if (activity == null)
            return;

        TaskModelData task = this.model.getTaskForActivity(activity);

        if (task == null)
        {
            task = new TaskModelData();
            task.setWorkObjectId(activity.getWorkObjectId());
            task.setWorkObjectName(activity.getWorkObjectName());
            task.setProjectName(activity.getProjectName());
            task.setWorkObjectParentsName(activity.getWorkObjectParentsName());
            task.setTaskTypeName(activity.getTaskTypeName());
            task.setTaskTypeId(activity.getTaskTypeId());
        }
        EventDispatcher.forwardEvent(BasicEvents.TASK_SELECTED, task);
    }

    /**
     * Initialize widget and data.
     */
    private void onInit()
    {
        this.model.initStoreParameters();
        this.view.initWidgets();

        this.handleEvent(ActivityEvents.REFRESH_ACTIVITIES);
    }

    /**
     * When previous button is clicked, the current day is set to the day before. All the data are refreshed depending
     * on the newly set day. Moreover next button is enabled if it was disabled.
     * 
     * @param event
     *            The previous clicked event.
     */
    private void onPreviousClicked(AppEvent event)
    {
        PersonDayModelData day = new PersonDayModelData();
        DateWrapper currentDate = new DateWrapper(this.model.getCurrentDay().getDate());
        day.setDate(currentDate.addDays(-1).asDate());
        day.setPersonId(MainModel.currentUser.getId());
        this.model.setCurrentDay(day);
        this.view.enableNextButton();
        day.refreshOrCreate(ActivityEvents.DAY_LOADED);
    }

    private boolean isAvailableDays(DateWrapper currentDate)
    {
        // test available days
        DateWrapper today = new DateWrapper(this.model.getToday());
        // DateWrapper limit = today.addDays(-1 * (BasicConfig.availableDays - 1));
        // System.out.println(BasicConfig.dayOfWeekBlock);
        // if (currentDate.before(limit))
        // {
        // return false;
        // }

        // test block day month
        int todayMonth = today.getMonth();
        int todayYear = today.getFullYear();
        int currentMonth = currentDate.getMonth();
        int currentYear = currentDate.getFullYear();
        if (todayMonth != currentMonth || (todayMonth == currentMonth && todayYear != currentYear))
        {
            int dayInMonth = today.getDate();
            if (dayInMonth >= BasicConfig.dayOfMonthBlock)
            {
                return false;
            }
        }

        // test block day week
        DateWrapper date = today.addDays(-(today.getDayInWeek() == 0 ? 7 : today.getDayInWeek()));
        if (date.before(currentDate))
        {
            int dayInWeek = today.getDayInWeek();
            if (dayInWeek > BasicConfig.dayOfWeekBlock)
            {
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    /**
     * When next button is clicked, the current day is set to the day after. All the data are refreshed depending on the
     * newly set day. Moreover next button is disabled if newly set day is equal at today.
     * 
     * @param event
     *            The next clicked event.
     */
    private void onNextClicked(AppEvent event)
    {
        if (!this.model.getCurrentDay().getDate().equals(DatetimeUtil.today()))
        {
            PersonDayModelData day = new PersonDayModelData();
            DateWrapper wrapper = new DateWrapper(this.model.getCurrentDay().getDate());
            day.setDate(wrapper.addDays(1).asDate());
            day.setPersonId(MainModel.currentUser.getId());
            this.model.setCurrentDay(day);
            this.view.enablePreviousButton();

            if (this.model.getCurrentDay().getDate().equals(DatetimeUtil.today()))
            {
                this.view.disableNextButton();
            }

            day.refreshOrCreate(ActivityEvents.DAY_LOADED);
        }
    }

    /**
     * When activities should be refreshed, the selected day is refreshed, then activities are loaded.
     */
    private void onRefreshActivities()
    {
        PersonDayModelData day = new PersonDayModelData();
        this.view.maskTaskActivityGrid();
        day.setDate(DatetimeUtil.today());
        this.onRefreshActivities(day);
    }

    /**
     * When activities should be refreshed, for given day, activities are loaded.
     * 
     * @param day
     *            The day for which activities will be displayed.
     */
    private void onRefreshActivities(PersonDayModelData day)
    {
        this.view.maskTaskActivityGrid();
        day.setPersonId(MainModel.currentUser.getId());
        this.model.setCurrentDay(day);
        this.view.resetSelection();
        day.refreshOrCreate(ActivityEvents.DAY_LOADED);
        this.model.refreshServertime();
    }

    /**
     * When selected day is loaded, total activity for current week is refreshed.
     */
    private void onDayLoaded()
    {
        this.model.updateDayConstraints();

        this.model.getTaskActivityStore().reload();
        this.model.getSimpleActivityStore().reload();
        this.reloadWeektotal();
        this.reloadMonthTotal();
        if (!isAvailableDays(new DateWrapper(this.model.getCurrentDay().getDate())))
        {
            this.view.setEditable(false);
        }
        else
        {
            this.view.setEditable(true);
        }
    }

    /**
     * Get from services total number of hours worked from the beginning of current week to current day.
     */
    public void reloadWeektotal()
    {
        Date currentDate = this.model.getCurrentDay().getDate();
        DateWrapper endDate = new DateWrapper(currentDate);
        endDate = endDate.clearTime();

        DateWrapper startDate = new DateWrapper(endDate.asDate());
        int nbDaysToSubstract = endDate.getDayInWeek() - 1;
        if (nbDaysToSubstract == -1)
        {
            nbDaysToSubstract = 6;
        }
        startDate = endDate.addDays(-1 * nbDaysToSubstract);

        this.model.reloadActivityWeekTotal(startDate.asDate(), endDate.asDate());
    }

    /**
     * Get from services total number of hours worked from the beginning of current week to current day.
     */
    public void reloadMonthTotal()
    {
        Date currentDate = this.model.getCurrentDay().getDate();
        DateWrapper endDate = new DateWrapper(currentDate);
        endDate = endDate.clearTime();

        DateWrapper startDate = new DateWrapper(endDate.getFirstDayOfMonth().asDate());

        this.model.reloadActivityMonthTotal(startDate.asDate(), endDate.asDate());
    }

    /**
     * When total activity for current week loaded, task and simple activity store are reloaded.
     */
    private void onWeekTotalRetrieved(AppEvent event)
    {
        Long total = event.getData();
        this.model.setWeekTotal(total);
        this.view.updateDayTitle();
    }

    /**
     * When activities are loaded, corresponding tasks are loaded.
     */
    private void onTaskActivitiesLoaded()
    {
        this.model.updateTaskConstraint();
        this.model.getTaskStore().reload();
    }

    /**
     * When activities are loaded, new activities are generated for task that don't have corresponding activities yet.
     */
    private void onTasksLoaded()
    {
        this.model.updateTaskIds();

        FastMap<TaskActivityModelData> activityExistMap = new FastMap<TaskActivityModelData>();

        // Remove filter used for display purpose to work on all activities.
        if (this.model.isFilterOn())
        {
            this.model.removeTodayFilter();
            this.model.setFilterOn(true);
        }

        for (TaskActivityModelData activity : this.model.getTaskActivityStore().getModels())
        {
            activityExistMap.put(activity.getTaskId().toString(), activity);
        }

        for (TaskModelData task : this.model.getTaskStore().getModels())
        {
            TaskActivityModelData activity = activityExistMap.get(task.getId().toString());
            if (activity == null)
            {
                activity = this.addTaskActivityToTaskActivityGrid(task);
            }
            else
            {
                activity.setTaskFields(task);
                this.model.getTaskActivityStore().update(activity);
            }
        }

        // Read filter to display only today activities.
        if (this.model.isFilterOn())
            this.model.addTodayFilter();

        this.view.updateDayTitle();
    }

    /**
     * Add a task activity to the current task activity grid based on task given in parameter.Add to the last position.
     * 
     * @param task
     *            The task on which new activity is built.
     * */
    private TaskActivityModelData addTaskActivityToTaskActivityGrid(TaskModelData task)
    {
        return addTaskActivityToTaskActivityGrid(task, this.model.getTaskActivityStore().getCount());
    }

    /**
     * Add a task activity to the current task activity grid based on task given in parameter to given position.
     * 
     * @param task
     *            The task on which new activity is built.
     * @param position
     *            The position to add the given task.
     */
    private TaskActivityModelData addTaskActivityToTaskActivityGrid(TaskModelData task, int position)
    {
        TaskActivityModelData activity = new TaskActivityModelData();
        activity.setDayId(this.model.getCurrentDay().getId());
        activity.setTaskFields(task);
        activity.setFilledDate(DatetimeUtil.today());
        activity.setFilledById(MainModel.currentUser.getId());
        activity.setWorkerId(MainModel.currentUser.getId());
        activity.setTaskTypeId(task.getTaskTypeId());
        activity.setWorkObjectId(task.getWorkObjectId());
        activity.setTaskStatus(task.getStatus());
        activity.setWorkObjectEntity(task.getBoundEntityName());
        this.model.getTaskActivityStore().insert(activity, position);

        return activity;
    }

    /**
     * When a task activity is edited, it is automatically saved. If duration is equal to zero and activity is not
     * linked to a task assigned to current worker, activity is deleted instead of saved.
     * 
     * @param event
     *            The grid cell edit event.
     */
    private void onTaskActivityEdited(AppEvent event)
    {
        TaskActivityModelData activity = event.getData();
        if (activity == null)
            return;

        String path = ServicesPath.getOneSegmentPath(ServicesPath.MY + ServicesPath.TASK_ACTIVITIES, activity.getId());
        activity.setDefaultPath(path);

        Long duration = activity.getDuration();
        if (duration != null)
        {
            if (duration == 0)
            {
                boolean isAssigned = null == this.model.getTaskMapById().get(activity.getTaskId().toString());

                if (isAssigned || activity.getWorkerId() == null)
                {
                    activity.delete(ActivityEvents.TASK_ACTIVITY_SAVED);
                    this.model.getTaskActivityStore().remove(activity);
                }
                else
                {
                    activity.delete(ActivityEvents.TASK_ACTIVITY_SAVED);
                    activity.setId(null);
                    activity.setDefaultPath(null);
                }
            }
            else
            {
                activity.save(ActivityEvents.TASK_ACTIVITY_SAVED);
            }
        }
        this.updateStatusTaskForActivity(activity);
    }

    /**
     * When a task activity is saved, dirty markers are removed and title is updated (for number of hours rendering).
     */
    private void onTaskActivitiesSaved()
    {
        this.view.removeTaskDirty();
        this.view.updateDayTitle();
        this.reloadWeektotal();
        this.reloadMonthTotal();
        EventDispatcher.forwardEvent(ActivityEvents.ACTIVITIES_SAVED, this.model.getSecondsTotal());
    }

    /**
     * When activity is edited, if lastEditedProject is registered, all informations linked to project are updated. If
     * all required fields are set, data are saved.
     * 
     * @param event
     *            The simple activity edited event.
     */
    private void onSimpleActivityEdited(AppEvent event)
    {
        SimpleActivityModelData activity = event.getData();
        if (activity == null)
            return;

        String path = ServicesPath
                .getOneSegmentPath(ServicesPath.MY + ServicesPath.SIMPLE_ACTIVITIES, activity.getId());
        activity.setDefaultPath(path);

        if (lastEditedProject != null)
        {
            activity.setProject(lastEditedProject);
            this.model.getSimpleActivityStore().update(activity);

            lastEditedProject = null;
        }

        if (activity.getProjectId() != null && activity.getDuration() != null && activity.getDuration() > 0)
        {
            if (activity.getId() == null)
            {
                this.model.addEmptySimpleActivity();
            }

            activity.save(ActivityEvents.SIMPLE_ACTIVITY_SAVED);
        }
        else if (activity.getId() != null && activity.getDuration() == 0)
        {
            activity.delete(ActivityEvents.SIMPLE_ACTIVITY_SAVED);
        }
    }

    /**
     * When simple activities are loaded, an empty line is added to let user add easily simple activities.
     */
    private void onSimpleActivitiesLoaded()
    {
        this.model.addEmptySimpleActivity();
        this.view.updateDayTitle();
    }

    /**
     * When a project is changed on a simple activity. The set value is registered, to be se after on the corresponding
     * model data.
     * 
     * @param event
     *            Project changed event.
     */
    private void onSimpleProjectChanged(AppEvent event)
    {
        lastEditedProject = event.getData();
    }

    /**
     * When simple activity is saved, hour number is updated and dirty markers are removed.
     */
    private void onSimpleActivitiesSaved(AppEvent event)
    {
        this.view.removeSimpleDirty();
        this.view.updateDayTitle();
        this.reloadWeektotal();
        this.reloadMonthTotal();

        SimpleActivityModelData activity = event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);
        if (activity != null && activity.getDuration() == 0L)
            this.model.getSimpleActivityStore().remove(activity);

        EventDispatcher.forwardEvent(ActivityEvents.ACTIVITIES_SAVED, this.model.getSecondsTotal());
    }

    /**
     * When "Add task" item is clicked, the task selector is displayed to let user select another on which save time.
     */
    private void onAddTaskClicked()
    {
        this.view.displayTaskSelector();
    }

    /**
     * When task selector grid is double clicked the selected task is added to the current day task list (if not
     * already). User is now allowed to add time on this task.<br>
     * <br>
     * A confirmation message box is displayed if task is not assigned. Because services automatically assign task to
     * current user if task is unassigned.
     * 
     * @param event
     *            The selector task double clicked event.
     */
    private void onSelectorTaskDoubleClicked(AppEvent event)
    {
        final TaskModelData task = event.getData();
        if (task.getWorkerID() == null)
        {
            Listener<MessageBoxEvent> handler = new Listener<MessageBoxEvent>() {
                public void handleEvent(MessageBoxEvent be)
                {
                    Button btn = be.getButtonClicked();
                    if (btn.getItemId().equals(Dialog.YES))
                        addTaskActivityForTask(task);
                }
            };
            MessageBox
                    .confirm(
                            "Warning",
                            "This task is unassigned. If you set activity time on it, it will be assigned to you. Do you still want to set activity time on it ?",
                            handler);
        }
        else
        {
            addTaskActivityForTask(task);
        }
    }

    /**
     * Add a new task activity to the task grid and initializes its data with given task data.
     * 
     * @param task
     *            The task for which an activity is needed (to let user set time on it).
     */
    private void addTaskActivityForTask(TaskModelData task)
    {
        if (task == null)
            return;
        this.view.hideTaskSelector();

        FastMap<Boolean> activityExistMap = new FastMap<Boolean>();
        for (TaskActivityModelData activity : this.model.getTaskActivityStore().getModels())
        {
            activityExistMap.put(activity.getTaskId().toString(), true);
        }
        if (activityExistMap.get(task.getId().toString()) == null)
        {
            this.addTaskActivityToTaskActivityGrid(task, 0);
            if (task.getWorkerID() == null || task.getWorkerID().longValue() == MainModel.currentUser.getId())
            {
                this.model.getTaskStore().add(task);
                this.model.updateTaskIds();
            }
        }

        this.view.resetSelection();
    }

    /**
     * When current day toggle button is clicked a filter is applied to task activity store to display only tasks
     * planned for current day. If toggle button is off, the filter is removed.
     */
    private void onAllOrPlannedClicked(AppEvent event)
    {
        Boolean filter = event.getData();
        if (filter)
        {
            this.model.addTodayFilter();
        }
        else
        {
            this.model.removeTodayFilter();
        }
    }

    /**
     * When activity is double clicked, it displays its identity panel.
     * 
     * @param event
     *            Activity double clicked event.
     */
    private void onActivityDoubleClicked(AppEvent event)
    {
        TaskActivityModelData activity = this.view.getSelectedActivity();
        Hd3dModelData workObject = new Hd3dModelData();
        workObject.setId(activity.getWorkObjectId());

        if (FieldUtils.isShot(activity.getWorkObjectEntity()))
        {
            workObject.setSimpleClassName(ShotModelData.SIMPLE_CLASS_NAME);
            workObject.setDefaultPath(ServicesPath.PROJECTS + activity.getProjectId() + "/" + ServicesPath.SHOTS
                    + workObject.getId() + "/");
        }
        else if (FieldUtils.isConstituent(activity.getWorkObjectEntity()))
        {
            workObject.setSimpleClassName(ConstituentModelData.SIMPLE_CLASS_NAME);
            workObject.setDefaultPath(ServicesPath.PROJECTS + activity.getProjectId() + "/" + ServicesPath.CONSTITUENTS
                    + workObject.getId() + "/");
        }
        else
        {
            Logger.log("Info Dialog Display : wrong entity for object to display", new Exception());
        }

        ProjectModelData project = new ProjectModelData();
        project.setId(activity.getProjectId());

        this.view.displayIdentityDialog(workObject, project);
    }

    /** When refresh time is reached, activity grids are reloaded. */
    private void onRefreshRequested()
    {
        PersonDayModelData day = this.model.getCurrentDay();

        this.onRefreshActivities(day);
    }

    /**
     * When header button is clicked grid headers are hidden or shown depending if button is pressed or not.
     * 
     * @param event
     *            Headers button event.
     */
    private void onHeadersButtonClicked(AppEvent event)
    {
        Boolean isPressed = event.getData();
        if (isPressed)
        {
            this.view.showGridHeaders();
        }
        else
        {
            this.view.hideGridHeaders();
        }
    }

    private void onActivityCommentClicked(AppEvent event)
    {
        this.view.displayActivityCommentDialog();
    }

    /**
     * Save the status of the activity if it changed. It forward the event Status_changed_waitapp, if the status is
     * wait_4_approval.
     * 
     * @param activity
     *            activity whose the status changed.
     */
    public void updateStatusTaskForActivity(TaskActivityModelData activity)
    {

        TaskModelData task = this.model.getTaskMapById().get(activity.getTaskId().toString());
        if (task == null)
            return;

        Boolean right = task.getUserCanUpdate();
        if (right)
        {
            if (!task.getStatus().equals(activity.getTaskStatus()))
            {
                task.setDefaultPath(ServicesPath.getOneSegmentPath(ServicesPath.MY + ServicesPath.TASKS, task.getId()));
                task.setStatus(activity.getTaskStatus());

                EventDispatcher.forwardEvent(ActivityEvents.STATUS_CHANGED, task);
                EventDispatcher.forwardEvent(ActivityEvents.TASK_ACTIVITY_SAVED);
            }
        }
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        this.registerEventTypes(ActivityEvents.INIT);

        this.registerEventTypes(ActivityEvents.DAY_LOADED);
        this.registerEventTypes(ActivityEvents.ACTIVITY_WEEK_TOTAL_RETRIEVED);
        this.registerEventTypes(ActivityEvents.REFRESH_ACTIVITIES);
        this.registerEventTypes(ActivityEvents.TASKS_LOADED);
        this.registerEventTypes(ActivityEvents.TASK_ACTIVITIES_LOADED);
        this.registerEventTypes(ActivityEvents.SIMPLE_ACTIVITIES_LOADED);
        this.registerEventTypes(ActivityEvents.TASK_ACTIVITY_EDITED);
        this.registerEventTypes(ActivityEvents.SIMPLE_ACTIVITY_EDITED);
        this.registerEventTypes(ActivityEvents.SIMPLE_PROJECT_CHANGED);
        this.registerEventTypes(ActivityEvents.SIMPLE_ACTIVITY_SAVED);
        this.registerEventTypes(ActivityEvents.TASK_ACTIVITY_SAVED);
        this.registerEventTypes(ActivityEvents.NEXT_CLICKED);
        this.registerEventTypes(ActivityEvents.PREVIOUS_CLICKED);
        this.registerEventTypes(ActivityEvents.ADD_TASK_CLICKED);
        this.registerEventTypes(ActivityEvents.SELECTOR_TASK_DOUBLE_CLICKED);
        this.registerEventTypes(ActivityEvents.ALL_OR_PLANNED_CLICKED);
        this.registerEventTypes(ActivityEvents.ACTIVITY_DOUBLE_CLICKED);

        this.registerEventTypes(BasicEvents.REFRESH_TIME_REACHED);
        this.registerEventTypes(BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED);
        this.registerEventTypes(BasicEvents.REFRESH_BUTTON_CLICKED);
        this.registerEventTypes(ActivityEvents.TASK_ACTIVITY_SELECTED);
        this.registerEventTypes(ActivityEvents.STATUS_CHANGED);
        this.registerEventTypes(ActivityEvents.ACTIVITY_MONTH_TOTAL_RETRIEVED);
        this.registerEventTypes(ActivityEvents.TASK_EDITED);
        this.registerEventTypes(ActivityEvents.EDIT_COMMENT_CLICKED);
    }
}
