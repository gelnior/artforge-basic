package fr.hd3d.basic.ui.client.portlet.activity.widget;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.simpleexplorer.ISimpleExplorerPanel;


/**
 * Interface needed to mock task simple explorer.
 * 
 * @author HD3D
 */
public interface ITaskSimpleExplorer extends ISimpleExplorerPanel<TaskModelData>
{

    /** Set work object field value to null. Set Task type combo box value to null. */
    public void clearNonProjectFields();

    /** Enable work object field. Enable task type combo box. */
    public void enableNonProjectFields();

    /** Disable work object field. Disable task type combo box. */
    public void disableNonProjectFields();
}
