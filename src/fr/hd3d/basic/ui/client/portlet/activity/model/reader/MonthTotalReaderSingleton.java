package fr.hd3d.basic.ui.client.portlet.activity.model.reader;

public class MonthTotalReaderSingleton
{
    private static IMonthTotalReader reader;

    public static IMonthTotalReader get()
    {
        if (reader == null)
        {
            reader = new MonthTotalReader();
        }
        return reader;
    }

    public static void setInstance(IMonthTotalReader monthTotalReaderMock)
    {
        reader = monthTotalReaderMock;
    }
}
