package fr.hd3d.basic.ui.client.portlet.activity.editor;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


/**
 * Editor that lets user to select a project model data via its name and that returns as value the project name.
 * 
 * @author HD3D
 */
public class ProjectEditor extends CellEditor
{
    /** The field combo box which act as the editor. */
    private final ProjectActivityCombobox combo;

    // /** */
    // private final String name = "";

    /** Default Constructor. */
    public ProjectEditor(ProjectActivityCombobox combo)
    {
        super(combo);

        this.combo = combo;
    }

    /**
     * Set project combo value by selecting a project of name equals to <i>value</i>.
     * 
     * @param value
     *            Name of the project to set.
     */
    @Override
    public Object preProcessValue(Object value)
    {
        ProjectModelData object;

        if (value == null)
        {
            return null;
        }
        else
        {
            String name = (String) value;
            object = combo.getValueByName(name);

            return object;
        }
    }

    /**
     * When project data is edited, it dispatches an event to controllers telling that a simple project has changed.
     * 
     * @return Selected project name.
     */
    @Override
    public Object postProcessValue(Object value)
    {
        ProjectModelData project = (ProjectModelData) value;

        if (value == null)// || (name != null && name.equals(project.getName())))
        {
            return null;
        }
        else
        {
            AppEvent event = new AppEvent(ActivityEvents.SIMPLE_PROJECT_CHANGED);
            event.setData(project);
            EventDispatcher.forwardEvent(event);

            return project.getName();
        }
    }
}
