package fr.hd3d.basic.ui.client.portlet.activity.model.reader;

public interface IWeekTotalReader
{
    public Long getTotal(String json);
}
