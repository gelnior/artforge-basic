package fr.hd3d.basic.ui.client.portlet.activity.widget;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;


/**
 * OK dialog has OK/Cancel buttons set by default and provides an easy method to override when OK button is pressed.
 * 
 * @author HD3D
 */
public abstract class OkDialog extends Dialog
{
    /**
     * Constructor :
     */
    public OkDialog()
    {
        this.setButtons(Dialog.OKCANCEL);
    }

    /**
     * When button is pressed, it checks it is OK button then it calls <i>onOkPressed</i> method.
     */
    @Override
    protected void onButtonPressed(Button button)
    {
        super.onButtonPressed(button);
        if (button == getButtonById(OK))
        {
            this.onOkPressed();
        }
    }

    /**
     * Method called when OK button is pressed.
     */
    protected abstract void onOkPressed();
}
