package fr.hd3d.basic.ui.client.portlet.activity.model.reader;

public interface IMonthTotalReader
{
    public Long getTotal(String json);
}
