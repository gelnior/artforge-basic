package fr.hd3d.basic.ui.client.portlet.activity;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


public interface IActivityPortlet
{
    void initWidgets();

    void enableNextButton();

    void disableNextButton();

    void updateDayTitle();

    void removeTaskDirty();

    void removeSimpleDirty();

    void displayTaskSelector();

    void disablePreviousButton();

    void enablePreviousButton();

    void showGridHeaders();

    void hideGridHeaders();

    void openTaskComment(TaskModelData task);

    void setEditable(boolean b);

    void resetSelection();

    void hideTaskSelector();

    TaskActivityModelData getSelectedActivity();

    void displayIdentityDialog(Hd3dModelData workObject, ProjectModelData project);

    void displayActivityCommentDialog();

    void maskTaskActivityGrid();
}
