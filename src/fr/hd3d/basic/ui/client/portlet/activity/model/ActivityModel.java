package fr.hd3d.basic.ui.client.portlet.activity.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.basic.ui.client.config.BasicConfig;
import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.basic.ui.client.portlet.activity.model.callback.MonthTotalCallback;
import fr.hd3d.basic.ui.client.portlet.activity.model.callback.WeekTotalCallback;
import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.ServerTimeCallback;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EndDateParameter;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.ExtraFields;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;
import fr.hd3d.common.ui.client.service.parameter.StartDateParameter;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Model containing data store.
 * 
 * @author HD3D
 */
public class ActivityModel
{
    private final Date today = new Date();

    /** The store containing tasks associated to activities. */
    protected ServiceStore<TaskModelData> taskStore = new ServiceStore<TaskModelData>(ReaderFactory.getTaskReader());

    /** The store containing task activities. */
    protected ServiceStore<TaskActivityModelData> taskActivityStore = new ServiceStore<TaskActivityModelData>(
            ReaderFactory.getTaskActivityReader());
    /** The store containing simple activities. */
    protected ServiceStore<SimpleActivityModelData> simpleActivityStore = new ServiceStore<SimpleActivityModelData>(
            ReaderFactory.getSimpleActivityReader());

    /** Start date constraint for task store. */
    protected Constraint taskStartConstraint = new Constraint(EConstraintOperator.leq, TaskModelData.START_DATE_FIELD,
            null);
    /** End date constraint for task store. */
    protected Constraint taskEndConstraint = new Constraint(EConstraintOperator.geq, TaskModelData.END_DATE_FIELD, null);

    /** The constraint to retrieve only activities for the current day. */
    private final Constraint dayConstraint = new Constraint(EConstraintOperator.eq, "day.id", null);

    private FastMap<TaskModelData> taskMapById = new FastMap<TaskModelData>();

    /** True if today filter is on. */
    private boolean isFilterOn;
    /** Filter to display only activity planned for the current day. */
    private final ActivityStoreFilter todayFilter = new ActivityStoreFilter();

    /**
     * Every activities are linked to a day. Current day represents the day on which displayed activities are
     * associated.
     */
    private PersonDayModelData currentDay;

    /** Total activity time for current week. */
    private Long weekTotal = 0L;
    /** Total activity time for current month. */
    private Long monthTotal = 0L;

    private final InConstraint taskIdsConstraint = new InConstraint(TaskModelData.ID_FIELD);
    private final OrConstraint rootTaskCosntraint = new OrConstraint();

    /**
     * Default constructor. Set grouping on task activity grid.
     */
    public ActivityModel()
    {
        this.taskActivityStore.groupBy(TaskActivityModelData.WORK_OBJECT_PARENTS_NAME_FIELD);
    }

    /**
     * @return Store which contains task activity data.
     */
    public ServiceStore<TaskActivityModelData> getTaskActivityStore()
    {
        return taskActivityStore;
    }

    /**
     * @return Store which contains simple activity data.
     */
    public ServiceStore<SimpleActivityModelData> getSimpleActivityStore()
    {
        return simpleActivityStore;
    }

    /**
     * @return Store which contains task activity data.
     */
    public ServiceStore<TaskModelData> getTaskStore()
    {
        return taskStore;
    }

    /**
     * @return Current day of displayed activities.
     */
    public PersonDayModelData getCurrentDay()
    {
        return this.currentDay;
    }

    public Long getDayConstraintValue()
    {
        return (Long) this.dayConstraint.getLeftMember();
    }

    public Date getTaskStartDateConstraintValue()
    {
        return (Date) this.taskStartConstraint.getLeftMember();
    }

    public Date getTaskEndDateConstraintValue()
    {
        return (Date) this.taskEndConstraint.getLeftMember();
    }

    /**
     * @return Date corresponding to today.
     */
    public Date getToday()
    {
        return today;
    }

    /**
     * Set the total of work week time.
     * 
     * @param total
     */
    public void setWeekTotal(Long total)
    {
        this.weekTotal = total;
    }

    /**
     * Get the total of work week time.
     * 
     * @return the total of work week time.
     */
    public Long getWeekTotal()
    {
        return this.weekTotal;
    }

    public FastMap<TaskModelData> getTaskMapById()
    {
        return taskMapById;
    }

    /**
     * Return the task concerning to the given activity.
     * 
     * @param activity
     *            the given activity.
     * @return the task
     */
    public TaskModelData getTaskForActivity(TaskActivityModelData activity)
    {
        if (activity == null)
            return null;
        return taskMapById.get(activity.getTaskId().toString());
    }

    /**
     * Return if the activity owner is the associated task owner.
     * 
     * @param activity
     *            the given activity
     * @return if it is the associated task owner.
     */
    public boolean isOwnTask(TaskActivityModelData activity)
    {

        return (taskMapById.get(activity.getTaskId().toString()) == null);
    }

    /**
     * Set the total of work month time.
     * 
     * @param total
     *            the total of work month time.
     */

    public void setMonthTotal(Long total)
    {
        this.monthTotal = total;

    }

    /**
     * Get the total of work month time.
     * 
     * @return the total of work month time.
     */
    public Long getMonthTotal()
    {
        return monthTotal;
    }

    /**
     * Set the current day for selecting which activities should be displayed.
     * 
     * @param day
     *            Day to set as current day.
     */
    public void setCurrentDay(PersonDayModelData day)
    {
        this.currentDay = day;
        String path = ServicesPath.getOneSegmentPath(ServicesPath.MY + ServicesPath.PERSON_DAYS,
                this.currentDay.getId());
        this.currentDay.setDefaultPath(path);
    }

    /**
     * @return True if current day filter is on.
     */
    public boolean isFilterOn()
    {
        return isFilterOn;
    }

    /**
     * @param isFilterOn
     *            the isFilterOn to set
     */
    public void setFilterOn(boolean isFilterOn)
    {
        this.isFilterOn = isFilterOn;
    }

    /**
     * Sets right URL for services proxy : path and parameters. Task that are retrieved are all work in progress task
     * (i.e. task status not equal to OK or closed) and assigned to currently connected user.
     */
    public void initStoreParameters()
    {
        List<String> banStatus = new ArrayList<String>();
        banStatus.add(ETaskStatus.CANCELLED.toString());
        banStatus.add(ETaskStatus.CLOSE.toString());
        banStatus.add(ETaskStatus.OK.toString());
        banStatus.add(ETaskStatus.NEW.toString());

        Constraint workerConstraint = new EqConstraint(TaskModelData.WORKER_ID, MainModel.currentUser.getId());
        Constraint statusConstraint = new Constraint(EConstraintOperator.notin, TaskModelData.STATUS_FIELD, banStatus);
        AndConstraint andWorkerStatut = new AndConstraint(workerConstraint, statusConstraint);

        Date today = new Date();
        long milliSecondPerHour = DatetimeUtil.DAY_MILLISECONDS;
        int nbJour = BasicConfig.availableDays;
        long actualTime = today.getTime();
        long passedTime = actualTime - (nbJour * milliSecondPerHour);
        Date today_sub_availableDays = new Date(passedTime);

        ArrayList<String> statusTime = new ArrayList<String>();
        statusTime.add(ETaskStatus.CLOSE.toString());
        statusTime.add(ETaskStatus.OK.toString());
        AndConstraint andStatusOk_today_sub_availableDays = new AndConstraint(new Constraint(EConstraintOperator.in,
                TaskModelData.STATUS_FIELD, statusTime), new AndConstraint(workerConstraint, new Constraint(
                EConstraintOperator.gt, TaskModelData.VERSION_FIELD, today_sub_availableDays)));

        OrConstraint or = new OrConstraint(andWorkerStatut, andStatusOk_today_sub_availableDays);
        rootTaskCosntraint.setLeftMember(or);
        this.taskStore.addParameter(rootTaskCosntraint);
        this.taskStore.addParameter(new ExtraFields(Const.TOTAL_ACTIVITIES_DURATION));
        this.taskStore.setPath(ServicesPath.MY + ServicesPath.TASKS);
        this.taskStore.addEventLoadListener(ActivityEvents.TASKS_LOADED);

        this.taskActivityStore.setPath(ServicesPath.MY + ServicesPath.TASK_ACTIVITIES);
        this.taskActivityStore.addEventLoadListener(ActivityEvents.TASK_ACTIVITIES_LOADED);
        this.taskActivityStore.addParameter(dayConstraint);

        this.simpleActivityStore.setPath(ServicesPath.MY + ServicesPath.SIMPLE_ACTIVITIES);
        this.simpleActivityStore.addParameter(dayConstraint);
        this.simpleActivityStore.addEventLoadListener(ActivityEvents.SIMPLE_ACTIVITIES_LOADED);
    }

    /**
     * Update task IDs list used for retrieving data.
     */

    public void updateTaskIds()
    {
        taskMapById = new FastMap<TaskModelData>();
        for (TaskModelData task : taskStore.getModels())
        {
            taskMapById.put(task.getId().toString(), task);
        }
    }

    /**
     * Update current day constraint by setting day ID as a parameter. Update task store constraints with day date.
     */
    public void updateDayConstraints()
    {
        this.dayConstraint.setLeftMember(currentDay.getId());

        DateWrapper date = new DateWrapper(currentDay.getDate());
        this.taskEndConstraint.setLeftMember(date.clearTime().asDate());
        this.taskStartConstraint.setLeftMember(date.clearTime().addDays(1).asDate());
    }

    /**
     * @return Total number of hours registered for displayed activities.
     */
    public int getHoursTotal()
    {
        Double hours = 0D;

        // Total number of task activity hours.
        for (TaskActivityModelData activity : this.taskActivityStore.getModels())
        {
            if (activity.getId() != null && activity.getDuration() != null)
                hours += new Double(activity.getDuration());
        }

        // Total number of simple activity hours.
        for (SimpleActivityModelData activity : this.simpleActivityStore.getModels())
        {
            if (activity.getId() != null && activity.getDuration() != null)
                hours += new Double(activity.getDuration());
        }

        return new Double(hours / (3600)).intValue();
    }

    /**
     * @return Total number of hours registered for displayed activities.
     */
    public int getSecondsTotal()
    {
        Long seconds = 0L;

        // Total number of task activity hours.
        for (TaskActivityModelData activity : this.taskActivityStore.getModels())
        {
            if (activity.getId() != null && activity.getDuration() != null)
                seconds += activity.getDuration();
        }

        // Total number of simple activity hours.
        for (SimpleActivityModelData activity : this.simpleActivityStore.getModels())
        {
            if (activity.getId() != null && activity.getDuration() != null)
                seconds += activity.getDuration();
        }

        return seconds.intValue();
    }

    /**
     * Get task IDs for task that are planned for today then apply store filter on task activity store that will remove
     * all activity linked to tasked that are not planned today.
     */
    public void addTodayFilter()
    {
        this.setFilterOn(true);
        this.todayFilter.getIds().clear();
        for (TaskModelData task : taskStore.getModels())
        {
            Date startDate = task.getStartDate();
            Date endDate = task.getEndDate();
            Date currentDate = this.currentDay.getDate();
            if (startDate != null && endDate != null && (currentDate.after(startDate) || currentDate.equals(startDate))
                    && (currentDate.before(endDate) || currentDate.equals(endDate)))
            {
                this.todayFilter.getIds().add(task.getId());
            }
        }

        this.taskActivityStore.addFilter(this.todayFilter);
        this.taskActivityStore.applyFilters(TaskActivityModelData.TASK_ID_FIELD);
    }

    /**
     * Remove current day task IDs filter on task activity store.
     */
    public void removeTodayFilter()
    {
        this.setFilterOn(false);
        this.taskActivityStore.removeFilter(this.todayFilter);
        this.taskActivityStore.applyFilters(TaskActivityModelData.TASK_ID_FIELD);
    }

    /**
     * Create a new simple activity inside simple activity store. This activity is already configured for currently
     * selected day and connected user. Project, duration and type are not registered.
     */
    public void addEmptySimpleActivity()
    {
        SimpleActivityModelData activity = new SimpleActivityModelData();
        activity.setDayId(this.getCurrentDay().getId());
        activity.setFilledDate(DatetimeUtil.today());
        activity.setFilledById(MainModel.currentUser.getId());
        activity.setWorkerId(MainModel.currentUser.getId());

        this.getSimpleActivityStore().add(activity);
    }

    /**
     * Reload total of seconds spent during activities between <i>startDate</i> and <i>endDate</i> for current user.
     * 
     * @param startDate
     *            Start date of time area on which total will be calculated.
     * @param endDate
     *            End date of time area on which total will be calculated.
     */
    public void reloadActivityWeekTotal(Date startDate, Date endDate)
    {
        WeekTotalCallback callback = new WeekTotalCallback();
        String path = ServicesPath.MY + ServicesPath.DURATION;

        StartDateParameter startDateParameter = new StartDateParameter(startDate);
        EndDateParameter endDateParameter = new EndDateParameter(endDate);
        path += ParameterBuilder.parametersToString(Arrays.asList(startDateParameter, endDateParameter));
        RestRequestHandlerSingleton.getInstance().getRequest(path, callback);
    }

    /**
     * Reload total of seconds spent during activities between <i>startDate</i> and <i>endDate</i> for current user.
     * 
     * @param startDate
     *            Start date of time area on which total will be calculated.
     * @param endDate
     *            End date of time area on which total will be calculated.
     */
    public void reloadActivityMonthTotal(Date startDate, Date endDate)
    {
        MonthTotalCallback callback = new MonthTotalCallback();
        String path = ServicesPath.MY + ServicesPath.DURATION;

        StartDateParameter startDateParameter = new StartDateParameter(startDate);
        EndDateParameter endDateParameter = new EndDateParameter(endDate);
        path += ParameterBuilder.parametersToString(Arrays.asList(startDateParameter, endDateParameter));
        RestRequestHandlerSingleton.getInstance().getRequest(path, callback);
    }

    public void refreshServertime()
    {
        String path = ServicesURI.SERVERTIME;
        RestRequestHandlerSingleton.getInstance().getRequest(path, new ServerTimeCallback(today));
    }

    /**
     * Update tasks store constraint by adding a parameter to retrieve tasks corresponding to current activities.
     */
    public void updateTaskConstraint()
    {
        if (taskActivityStore.getCount() > 0)
        {
            List<Long> ids = new ArrayList<Long>();
            for (TaskActivityModelData activity : this.taskActivityStore.getModels())
            {
                ids.add(activity.getTaskId());
            }
            this.taskIdsConstraint.setLeftMember(ids);
            this.rootTaskCosntraint.setRightMember(this.taskIdsConstraint);
        }
        else
        {
            this.rootTaskCosntraint.setRightMember(null);
        }
    }

}
