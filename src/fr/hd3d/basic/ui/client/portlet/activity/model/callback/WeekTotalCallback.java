package fr.hd3d.basic.ui.client.portlet.activity.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.basic.ui.client.portlet.activity.ActivityEvents;
import fr.hd3d.basic.ui.client.portlet.activity.model.reader.WeekTotalReaderSingleton;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * Value returned by activities duration service is not an object list. So the data have to be parsed in a specific way.
 * This callback extracts the duration from returned data and forwards it to event dispatcher with an
 * ACTIVITY_WEEK_TOTAL_RETRIEVED event.
 * 
 * @author HD3D
 */
public class WeekTotalCallback extends BaseCallback
{

    @Override
    protected void onSuccess(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();
            Long total = WeekTotalReaderSingleton.get().getTotal(json);
            if (total == null)
                total = 0L;

            EventDispatcher.forwardEvent(ActivityEvents.ACTIVITY_WEEK_TOTAL_RETRIEVED, total);
        }
        catch (Exception e)
        {
            Logger.log("Error occurs while retrieving total week activity time.", e);
        }
    }
}
