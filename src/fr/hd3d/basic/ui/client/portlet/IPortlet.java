package fr.hd3d.basic.ui.client.portlet;

public interface IPortlet
{
    public void show();

    public void hide();

    public String getTitle();
}
