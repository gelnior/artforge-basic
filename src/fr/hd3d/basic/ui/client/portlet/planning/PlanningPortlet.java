package fr.hd3d.basic.ui.client.portlet.planning;

import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.basic.ui.client.constant.BasicConstants;
import fr.hd3d.basic.ui.client.portlet.IPortlet;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


/**
 * TaskPortlet displays currently opened task assigned to the user.
 * 
 * @author HD3D
 */
public class PlanningPortlet extends Portlet implements IPortlet
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static BasicConstants CONSTANTS = GWT.create(BasicConstants.class);

    /** Widget controller which handle events. */
    protected PlanningController controller = new PlanningController();
    /** Model contains data. */
    protected PlanningModel model = new PlanningModel();
    /** The task grid. */
    protected Grid<TaskModelData> taskGrid;

    /**
     * Default constructor : initialize data and widgets.
     */
    public PlanningPortlet()
    {
        this.setStyles();

        this.setPlanning();
    }

    /**
     * Build planning displayed inside portlet.
     */
    private void setPlanning()
    {
        // UserPlanning planning = new UserPlanning(true);
        // planning.setBodyBorder(false);
        // planning.setBorders(true);
        // planning.setHeaderVisible(false);
        // planning.hideToolbar();
        //
        // planning.addUser(MainModel.currentUser);

        // this.add(planning.getContentPanel());
    }

    /**
     * Set portlet styles : title, layout, width...
     */
    private void setStyles()
    {
        this.setLayout(new FitLayout());
        this.setAutoWidth(true);
        this.setHeading(COMMON_CONSTANTS.Planning());
        this.setTitle("planning");
        this.setHeight(200);
    }
}
