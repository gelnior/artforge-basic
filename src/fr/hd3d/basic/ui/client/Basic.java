package fr.hd3d.basic.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Entry point classes for Logs application. It sets up the main view, and launch START event.
 */
public class Basic implements EntryPoint
{
    /**
     * This is the entry point method.
     */
    public void onModuleLoad()
    {
        BasicView mainView = new BasicView();
        mainView.init();

        EventDispatcher.forwardEvent(CommonEvents.START);

    }
}
