package fr.hd3d.basic.ui.client.util;

import com.extjs.gxt.ui.client.util.IconHelper;
import com.google.gwt.user.client.ui.AbstractImagePrototype;


/**
 * Basic Images furnishes basic icon.
 * 
 * @author HD3D
 */
public class BasicImages
{
    /**
     * @return Portlet selection icon.
     */
    public static AbstractImagePrototype getPortletsIcon()
    {
        return IconHelper.create("images/portlets.png");
    }

    /**
     * @return Portlet number of columns selection.
     */
    public static AbstractImagePrototype getColumnsIcon()
    {
        return IconHelper.create("images/columns.png");
    }
}
