package fr.hd3d.basic.ui.client.config;

import java.util.ArrayList;


/**
 * Contains constant values used at different places in application. Holds also static variable share between portlets.
 * 
 * @author HD3D
 */
public class BasicConfig
{
    public static final String TITLE_EVENT_VAR_NAME = "portlet-title";
    public static final String ROW_EVENT_VAR_NAME = "portlet-row";
    public static final String START_ROW_EVENT_VAR_NAME = "portlet-start-row";
    public static final String COLUMN_EVENT_VAR_NAME = "portlet-column";
    public static final String START_COLUMN_EVENT_VAR_NAME = "portlet-start-column";

    public static final String PORTLET_SUFFIX_SETTING = "basic-portlet-";
    public static final String COLUMN_PORTAL_SETTING = "basic-columns";
    public static final String POSITION_PORTLET_SETTING = "portlet-position-";
    public static final String PORTLET_MATRIX_COLUMN_SEPARATOR = ";";
    public static final String PORTLET_MATRIX_ROW_SEPARATOR = "-";

    public static final String LAYOUT_MENU_ITEM_GROUP = "layout";

    public static int availableDays = 7;
    public static ArrayList<String> excludedPortets = new ArrayList<String>();
    public static Long refreshTime = 0L;
    public static int dayOfWeekBlock = 7;
    public static int dayOfMonthBlock = 1;
}
