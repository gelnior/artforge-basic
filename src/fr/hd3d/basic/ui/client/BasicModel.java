package fr.hd3d.basic.ui.client;

import java.util.List;

import fr.hd3d.basic.ui.client.callback.InitBasicConfigurationCallback;
import fr.hd3d.basic.ui.client.config.BasicConfig;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Handles Basic main data.
 * 
 * @author HD3D
 */
public class BasicModel extends MainModel
{
    /** Maximum number of columns supported by basic application. */
    public static final int MAX_COLUMNS = 2;
    /** Name of the node from XML configuration file that contains number of browsable days. */
    public static final String AVAILABLES_DAY_XML_TAG = "available-days";

    /**
     * Name of the node from XML configuration file that contains the day of the week to block the edition of
     * activities.
     */
    public static final String DAYOFWEEK_BLOCK_XML_TAG = "dayOfWeek-block";

    /**
     * Name of the node from XML configuration file that contains the day of the month to block the edition of
     * activities.
     */
    public static final String DAYOFMONTH_BLOCK_XML_TAG = "dayOfMonth-block";

    /** Name of the node from XML configuration file that contains refresh time. */
    public static final String REFRESH_TIME_TAG = "refresh-time";
    /** Tag describing giving list of portlet to exclude. */
    public static final String EXCLUDED_PORTLETS_XML_TAG = "portlet";

    /** Default column number for widget layout. */
    public static final Integer DEFAULT_COLUMN_NUMBER = 2;

    /** Portlet position matrix where portlet are described by their lower case names. */
    private List<List<String>> portletsMatrix;

    /** The portal columns number. */
    private int nbColumns = DEFAULT_COLUMN_NUMBER;

    /**
     * @return The portal columns number.
     */
    public int getNbColumns()
    {
        return nbColumns;
    }

    /**
     * Set portal columns number.
     * 
     * @param nbColumns
     *            The value to set.
     */
    public void setNbColumns(int nbColumns)
    {
        this.nbColumns = nbColumns;
    }

    /**
     * @return Portlet position matrix where portlet are described by their lower case names.
     */
    public List<List<String>> getPortletMatrix()
    {
        return this.portletsMatrix;
    }

    /**
     * Set portlet position matrix where portlet are described by their lower case names.
     * 
     * @param matrix
     *            The matrix to set.
     */
    public void setPortletMatrix(List<List<String>> matrix)
    {
        this.portletsMatrix = matrix;
    }

    /**
     * Save portlet matrix to services. The key of the setting is composed of portlet-position- and the number of
     * columns actually set.The value is built with '-' and ';'. '-' separates rows and ';' separates column.<br>
     * Ex: key=portlet-position-2 value=task-activity;files-planning
     */
    public void savePortletMatrix()
    {
        String setting = "";
        for (List<String> column : portletsMatrix)
        {
            if (setting.length() > 0)
            {
                setting += BasicConfig.PORTLET_MATRIX_COLUMN_SEPARATOR;
            }

            String columnString = "";
            for (String title : column)
            {
                if (columnString.length() > 0)
                {
                    columnString += BasicConfig.PORTLET_MATRIX_ROW_SEPARATOR;
                }
                columnString += title;
            }

            setting += columnString;
        }

        UserSettings.setSetting(BasicConfig.POSITION_PORTLET_SETTING + this.nbColumns, setting);
    }

    /**
     * Extract maximum available days for activity portlet, excluded portlet list and refresh time from configuration
     * file.
     */
    @Override
    public void initServicesConfig()
    {
        try
        {
            requestHandler.getRequest(CommonConfig.CONFIG_FILE_PATH, new InitBasicConfigurationCallback(this));
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(CommonErrors.NO_CONFIG_FILE);
        }
    }
}
