package fr.hd3d.basic.ui.client;

import java.util.Set;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.PortalEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.custom.Portal;
import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.menu.CheckMenuItem;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;

import fr.hd3d.basic.ui.client.config.BasicConfig;
import fr.hd3d.basic.ui.client.constant.BasicConstants;
import fr.hd3d.basic.ui.client.error.BasicErrors;
import fr.hd3d.basic.ui.client.event.BasicEvents;
import fr.hd3d.basic.ui.client.portlet.activity.ActivityPortlet;
import fr.hd3d.basic.ui.client.portlet.timepoint.TimePointPortlet;
import fr.hd3d.basic.ui.client.portlet.validation.ValidationPortlet;
import fr.hd3d.basic.ui.client.util.BasicImages;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.MenuButtonClickListener;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.ToolBarToggleButton;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.mainview.MainView;


/**
 * Main Logs View shows a grid displaying log data and a toolbar for filtering the grid data.
 * 
 * @author HD3D
 */
public class BasicView extends MainView implements IBasicView
{
    /** Constant strings to display : dialog messages, button label... */
    public static BasicConstants CONSTANTS = GWT.create(BasicConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Model handling grid data. */
    private final BasicModel model = new BasicModel();
    /** Portal that contains widget portlets. */
    private Portal portal;
    /** Portlets referenced by their title. */
    private final FastMap<Portlet> portlets = new FastMap<Portlet>();
    /** Portlets menu items referenced by their title. */
    private final FastMap<CheckMenuItem> portletsItems = new FastMap<CheckMenuItem>();

    /** Menu for portlets menu items (to select which portlets are visible or not. */
    private final Menu portletsMenu = new Menu();

    /** Main Panel that contains all widgets. */
    private final BorderedPanel mainPanel = new BorderedPanel();

    protected final ToolBarToggleButton headerButton = new ToolBarToggleButton(Hd3dImages.getListDetail(),
            "Show/hide headers", BasicEvents.HEADERS_TOGGLE_BUTTON_CLICKED);

    private final Button refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), "Refresh all",
            BasicEvents.REFRESH_BUTTON_CLICKED);

    /**
     * Default constructor : set application name.
     */
    public BasicView()
    {
        super(CONSTANTS.Basic());
    }

    /**
     * Initializes controller and model.
     */
    @Override
    public void init()
    {
        BasicController mainController = new BasicController(model, this);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(mainController);
    }

    /**
     * Show widget build portal then add available portlets.
     */
    public void showWidget()
    {
        this.startViewport.removeAll();

        this.setPortlets();
        this.setMenuBar();
        this.setPortal(this.model.getNbColumns());

        this.addToViewport(mainPanel);

        this.headerButton.toggle();
    }

    /**
     * Hide portlet of which title corresponds to <i>title</i>.
     */
    public void hidePortlet(String title)
    {
        Portlet portlet = portlets.get(title.toLowerCase());
        CheckMenuItem item = portletsItems.get(title.toLowerCase());
        if (portlet != null)
        {
            item.setChecked(false);
            portlet.hide();
        }
    }

    /**
     * Show portlet of which title corresponds to <i>title</i>.
     */
    public void showPortlet(String title)
    {
        Portlet portlet = portlets.get(title.toLowerCase());

        if (portlet != null)
        {
            portlet.show();
        }
    }

    /**
     * @return True if item corresponding to portlet title is checked.
     */
    public boolean isPortletItemChecked(String title)
    {
        CheckMenuItem item = portletsItems.get(title.toLowerCase());
        if (item != null)
        {
            return item.isChecked();
        }

        return false;
    }

    /**
     * @return all available portlet titles.
     */
    public Set<String> getPortletTitles()
    {
        return portlets.keySet();
    }

    /**
     * Check portlet in the portlet menu and display portlet.
     */
    public void checkPortlet(String title)
    {
        CheckMenuItem item = this.portletsItems.get(title.toLowerCase());
        if (item != null)
        {
            item.setChecked(true);
        }
        this.showPortlet(title);
    }

    /**
     * Add portlet to the portlet portal inside column i.
     * 
     * @param title
     *            The title of the portlet to add.
     * @param i
     *            The portal column on which column should be added.
     */
    public void addPortlet(String title, int i)
    {
        Portlet portlet = portlets.get(title.toLowerCase());
        if (portlet != null)
            this.portal.add(portlet, i);
    }

    /**
     * Build portal with the number of columns given in parameter. Portal has a portlet drop listener.
     */
    public void setPortal(Integer nbColumns)
    {
        // Remove all portlets
        if (portal != null)
        {
            for (Portlet portlet : portlets.values())
            {
                if (portal.getPortletColumn(portlet) > 0)
                    portal.remove(portlet, portal.getPortletColumn(portlet));
            }
            mainPanel.remove(portal);
        }

        // Set new portal
        portal = new Portal(nbColumns);
        for (int i = 0; i < nbColumns; i++)
        {
            portal.setColumnWidth(i, 1 / new Float(nbColumns));
        }

        // Add drop listener.
        portal.addListener(Events.Drop, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                PortalEvent event = (PortalEvent) be;

                AppEvent appEvent = new AppEvent(BasicEvents.PORTLET_POSITION_CHANGED);
                appEvent.setData(BasicConfig.TITLE_EVENT_VAR_NAME, event.getPortlet().getTitle());
                appEvent.setData(BasicConfig.ROW_EVENT_VAR_NAME, event.getRow());
                appEvent.setData(BasicConfig.COLUMN_EVENT_VAR_NAME, event.getColumn());
                appEvent.setData(BasicConfig.START_ROW_EVENT_VAR_NAME, event.getStartRow());
                appEvent.setData(BasicConfig.START_COLUMN_EVENT_VAR_NAME, event.getStartColumn());

                EventDispatcher.forwardEvent(appEvent);
            }
        });

        if (this.mainPanel.getItemByItemId(portal.getId()) == null)
            mainPanel.addCenter(portal);
        mainPanel.layout();
    }

    /**
     * Display error message depending on error code passed in parameter.
     * 
     * @param error
     *            The code of the error to display.
     */
    @Override
    public void displayError(final Integer error, String userMsg, String stack)
    {

        if (!errorsDisplayed.contains(error))
        {
            Listener<MessageBoxEvent> callback = new Listener<MessageBoxEvent>() {
                public void handleEvent(MessageBoxEvent be)
                {
                    errorsDisplayed.remove(new Integer(error));
                }
            };

            super.displayError(error, userMsg, stack);

            switch (error)
            {
                case BasicErrors.TIME_DEPARTURE_BEFORE_ARRIVAL:
                    MessageBox.alert(COMMON_CONSTANTS.Error(), "Departure or break must be after arrival", callback);
                    break;
                case BasicErrors.TIME_BREAK_BIGGER_PRESENCE:
                    MessageBox.alert(COMMON_CONSTANTS.Error(),
                            "Break is bigger than difference between arrival and leaving.", callback);
                    break;
                case BasicErrors.ACTIVITIES_BIGGER_PRESENCE:
                    MessageBox.alert(COMMON_CONSTANTS.Error(),
                            "Activities are bigger than presence. Correct time presence, please.", callback);
                    break;
                case BasicErrors.TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING:
                    MessageBox.alert(COMMON_CONSTANTS.Error(),
                            "Break must be between arrival and leaving, and beginning before end.", callback);
                    break;
                default:
                    ;
            }
        }
    }

    /**
     * A new timer is set. It will be set repeatedly until basic is closed. <br>
     * When timer reaches the end of a cycle, REFRESH_TIME_REACHED event is raised.
     * 
     * @param refreshTime
     *            Time in second that will make timer to end.
     */
    public void setTimer(Long refreshTime)
    {
        Timer timer = new Timer() {
            @Override
            public void run()
            {
                EventDispatcher.forwardEvent(BasicEvents.REFRESH_TIME_REACHED);
            }
        };

        timer.scheduleRepeating(refreshTime.intValue() * 1000);
    }

    /**
     * Set menu bar : user name, portlets menu and layout menu.
     */
    private void setMenuBar()
    {
        this.mainPanel.setToolBar();

        LabelToolItem userLabel = new LabelToolItem(MainModel.getCurrentUser().getName());
        userLabel.setStyleAttribute("font-weight", "bold");
        userLabel.setStyleAttribute("font-size", "18px");
        userLabel.setStyleAttribute("padding-left", "15px");
        userLabel.setStyleAttribute("padding-right", "10px");
        this.mainPanel.addToToolBar(userLabel);
        this.mainPanel.addToToolBar(refreshButton);

        this.mainPanel.addToToolBar(new FillToolItem());

        this.mainPanel.addToToolBar(headerButton);

        Button portletButton = new Button();
        portletButton.setIcon(BasicImages.getPortletsIcon());
        portletButton.setMenu(portletsMenu);
        this.mainPanel.addToToolBar(portletButton);

        Button layoutButton = new Button();
        layoutButton.setIcon(BasicImages.getColumnsIcon());
        layoutButton.setMenu(this.getLayoutMenu());
        layoutButton.setStyleAttribute("padding-right", "18px");
        this.mainPanel.addToToolBar(layoutButton);
    }

    /**
     * @return Menu to tell if portal layout should be rendered as two or one column.
     */
    private Menu getLayoutMenu()
    {
        Menu layoutMenu = new Menu();

        AppEvent oneEvent = new AppEvent(BasicEvents.COLUMNS_CHANGED);
        oneEvent.setData(new Integer(1));
        AppEvent twoEvent = new AppEvent(BasicEvents.COLUMNS_CHANGED);
        twoEvent.setData(new Integer(2));

        CheckMenuItem oneColItem = new CheckMenuItem(CONSTANTS.OneColumn());
        oneColItem.setGroup(BasicConfig.LAYOUT_MENU_ITEM_GROUP);
        CheckMenuItem twoColItem = new CheckMenuItem(CONSTANTS.TwoColumns());
        twoColItem.setGroup(BasicConfig.LAYOUT_MENU_ITEM_GROUP);

        if (this.model.getNbColumns() == 1)
            oneColItem.setChecked(true);
        else
            twoColItem.setChecked(true);

        oneColItem.addSelectionListener(new MenuButtonClickListener(oneEvent));
        twoColItem.addSelectionListener(new MenuButtonClickListener(twoEvent));

        layoutMenu.add(oneColItem);
        layoutMenu.add(twoColItem);

        return layoutMenu;
    }

    /**
     * Add all portlet supported by the application.
     */
    private void setPortlets()
    {
        ActivityPortlet activityPortlet = new ActivityPortlet();
        this.addNewPortlet(activityPortlet);
        this.addNewPortlet(new TimePointPortlet(activityPortlet));
        // this.addNewPortlet(new TaskPortlet());
        this.addNewPortlet(new ValidationPortlet());
        // this.addNewPortlet(new FilePortlet());
        // this.addNewPortlet(new PersonnalPortlet());
        // this.addNewPortlet(new PlanningPortlet());
    }

    /**
     * Add a new portlet to the portal and add a menu entry to the portlet menu. Then checks the user settings to hide
     * it if the user hided it the last time he used the application.
     * 
     * @param portlet
     *            The portlet to add.
     */
    private void addNewPortlet(Portlet portlet)
    {
        if (!BasicConfig.excludedPortets.contains(portlet.getTitle().toLowerCase()))
        {
            String heading = portlet.getHeading().toLowerCase();

            // Register portlet.
            portlets.put(heading.toLowerCase(), portlet);

            CheckMenuItem menuItem = new CheckMenuItem(portlet.getHeading());
            // menuItem.setChecked(true);

            portlet.hide();

            // Set menu item.
            AppEvent event = new AppEvent(BasicEvents.PORTLET_CHECKED);
            event.setData(heading);
            menuItem.addSelectionListener(new MenuButtonClickListener(event));

            portletsMenu.add(menuItem);
            portletsItems.put(heading, menuItem);
        }
    }

}
