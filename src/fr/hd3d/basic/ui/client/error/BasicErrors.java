package fr.hd3d.basic.ui.client.error;

/**
 * Error displayed by basic.
 * 
 * @author HD3D
 */
public class BasicErrors
{
    public static final int TIME_DEPARTURE_BEFORE_ARRIVAL = 100;
    public static final int TIME_BREAK_BIGGER_PRESENCE = 101;
    public static final int ACTIVITIES_BIGGER_PRESENCE = 102;
    public static final int TIME_BEGINNING_BREAK_BEFORE_ARRIVAL_AFTER_LEAVING = 103;
}
