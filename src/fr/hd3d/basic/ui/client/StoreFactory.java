package fr.hd3d.basic.ui.client;

import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


/**
 * Factory for easy service store generating.
 * 
 * @author HD3D
 */
public class StoreFactory
{

    public static ServiceStore<TaskModelData> newTaskStore()
    {
        return new ServiceStore<TaskModelData>(ReaderFactory.getTaskReader());
    }
}
